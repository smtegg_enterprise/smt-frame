import { defineConfig } from 'vite'
import * as path from 'path'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': path.join(__dirname, './src'),
      'smt-frame': path.join(__dirname, './components/index.js')
    }
  },
  plugins: [
    vue(),
    // Components({
    //   resolvers: [
    //     AntDesignVueResolver({
    //       importStyle: false,
    //       exclude: ['Modal']
    //     }),
    //   ],
    // })
  ],
  define: {
    'process.env': {}
  },
  css: {

  },
  server: {    
    proxy: {
      '/api': {
        target: 'http://localhost:5000',
        changeOrigin: true
      }
    }
  },
  build: {
    lib: {
      entry: path.resolve(__dirname, './components/index.js'),
      name: 'SmtFrame',
      fileName: 'smt-frame'
    },
    outDir: 'lib',
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['vue', 'vue-router', 'ant-design-vue', 'vxe-table'],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue'
        },
        exports: 'named'
      }
    }
  }
})
