import { default as _colors } from 'tailwindcss/colors'
import { formatColor, parseColor } from 'tailwindcss/lib/util/color'

const gray = _colors.slate

export function parseAlpha(val, alpha = 1) {
  const color = parseColor(val)
  color.alpha = alpha
  return formatColor(color)
}

const primary = _colors.sky[500]

export const colors = {
  'dark': gray[900],
  'light': gray[100],
  'base': gray[600],
  'primary': primary,
  'success': _colors.green[500],
  'warning': _colors.orange[500],
  'error': _colors.rose[500],
  'info': gray[500],
  'background': gray[50],
  'border': gray[300],
  'border-dark': gray[600],
  'split': parseAlpha(gray[300], 0.6),
  'fill': parseAlpha(gray[300], 0.2),
  'selected': parseAlpha(primary, 0.1),
  'shadow': parseAlpha(_colors.black, 0.02),
  'gray': gray
}

export const spacing = {
  'xss': '2px',
  'xs': '4px',
  'sm': '8px',
  'md': '12px',
  'lg': '16px',
  'xl': '32px',
  '2xl': '48px'
}

export const font = {
  base: '12px',
  family: "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol',  'Noto Color Emoji'"
}