import * as components from './components'
import { utils } from '../utils'
import ColumnAction from './ColumnAction.vue'

const defaultConfig = {
  table: {
    size: 'mini',
    border: 'inner',
    stripe: false,
    sortConfig: { trigger: 'cell' },
    showOverflow: 'title',
    rowConfig: {
      isCurrent: true,
      isHover: true
    },
    columnConfig: {
      resizable: true,
      minWidth: '80'
    }
  }
}


function install(app, config) {
  components.VXETable.setConfig(utils.merge(defaultConfig, config))
  Object.keys(components).forEach(key => {
    if (components[key].install) {
      app.use(components[key])
    }
  })
  app.component('vxe-column-action', ColumnAction)
}

export default {
  install
}