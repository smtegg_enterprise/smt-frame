export {
  // 全局对象
  VXETable,

  // 表格功能
  Filter,
  Edit,
  //Menu,
  //Export,
  Keyboard,
  Validator,

  // 可选组件
  //Icon,
  Column,
  Colgroup,
  Grid,
  //Tooltip,
  // Toolbar,
  // Pager,
  // Form,
  // FormItem,
  // FormGather,
  // Checkbox,
  // CheckboxGroup,
  // Radio,
  // RadioGroup,
  // RadioButton,
  // Switch,
  // Input,
  // Select,
  // Optgroup,
  // Option,
  // Textarea,
  // Button,
  // Modal,
  List,
  Pulldown,

  // 表格
  Table
} from 'vxe-table'