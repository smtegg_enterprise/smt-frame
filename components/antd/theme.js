import { reactive, watch } from 'vue'
import { colors, font, spacing, parseAlpha } from '../theme'
import { theme as _theme } from 'ant-design-vue'
import { storage } from '../utils'

const token = {
  colorText: colors.base,
  colorPrimary: colors.primary,
  colorError: colors.error,
  colorInfo: colors.info,
  colorSuccess: colors.success,
  colorWarning: colors.warning,
  colorBorder: colors.border,
  colorBorderSecondary: colors.split,
  colorSplit: colors.split,
  colorLink: colors.primary,
  colorFillAlter: colors.fill,
  colorLinkHover: parseAlpha(colors.primary, 0.6),
  borderRadius: 2,
  fontSizeSM: Number(font.base.replace('px', '')),
  fontFamily: 'inherit'
}

const components = {
  Tooltip: {
    paddingXS: Number(spacing.sm.replace('px', ''))
  }
}

export const theme = reactive({
  mode: 'light',
  token: token,
  default: token,
  components: components,
  algorithm: [_theme.compactAlgorithm]
})

function setTheme(val) {
  if (val == 'dark') {
    theme.mode = 'dark'
    theme.token = Object.assign({}, token, {
      colorText: colors.light,
      colorBgBase: colors.dark,
      colorBorder: colors['border-dark'],
      colorBorderSecondary: colors['border-dark'],
      colorSplit: colors['border-dark'],
      colorFillAlter: parseAlpha(colors.light, 0.05)
    })

    theme.algorithm.push(_theme.darkAlgorithm)

    storage.set('theme', 'dark')
    document.documentElement.classList.add('dark')
  }

  if (val == 'light') {
    theme.mode = 'light'
    theme.token = token

    theme.algorithm.splice(-1, 1)

    storage.remove('theme')
    document.documentElement.classList.remove('dark')
  }
}

watch(() => theme.mode, val => {
  setTheme(val)
})

theme.mode = storage.get('theme') || 'light'