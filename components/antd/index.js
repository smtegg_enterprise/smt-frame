import { Modal, message, notification, DatePicker, RangePicker, TimePicker, InputSearch, Pagination, Form } from 'ant-design-vue'
import DragModal from './Modal.vue'

export * from './theme'

Modal.props.maskClosable.default = false
InputSearch.props.allowClear.default = true

const valueFormat = {
  type: String,
  default: (props) => {
    if (props.picker == 'year') {
      return 'YYYY'
    }
    if (props.picker == 'month') {
      return 'YYYY-MM'
    }
    if (props.showTime) {
      return 'YYYY-MM-DD HH:mm:ss'
    }
    return 'YYYY-MM-DD'
  }
}

DatePicker.props.valueFormat = valueFormat
RangePicker.props.valueFormat = valueFormat
TimePicker.props.valueFormat = {
  type: String,
  default: 'HH:mm:ss'
}

Pagination.props.defaultPageSize.default = 50
Pagination.props.pageSizeOptions = {
  type: Array,
  default: () => ['10', '30', '50', '100']
}
Pagination.props.showSizeChanger.default = true
Pagination.props.showTotal = {
  type: Function,
  default: (total) => `共 ${total} 条记录`
}

Form.props.labelCol.default = () => { return { style: { width: '100px' } } }

function install(app) {
  app._context.components['AModal'] = DragModal

  app.config.globalProperties.$message = message
  app.config.globalProperties.$notification = notification
  app.config.globalProperties.$modal = Modal
}

export default {
  install
}