function getData(value) {
  let data = null
  if (value && ((value.startsWith('{') && value.endsWith('}')) || (value.startsWith('[') && value.endsWith(']')))) {
    try {
      data = JSON.parse(value)
    } catch { }
  }
  return data ? data : value
}

export const session = {
  get(key) {
    const value = sessionStorage.getItem(key)
    return getData(value)
  },
  set(key, value) {
    if (typeof (value) === 'object') {
      value = JSON.stringify(value)
    }
    sessionStorage[key] = value
  },
  remove(key) {
    sessionStorage.removeItem(key)
  },
  clear() {
    sessionStorage.clear()
  }
}

export const storage = {
  get(key) {
    const value = localStorage.getItem(key)
    return getData(value)
  },
  set(key, value) {
    if (typeof value === 'object') {
      value = JSON.stringify(value)
    }
    localStorage.setItem(key, value)
  },
  remove(key) {
    localStorage.removeItem(key)
  },
  clear() {
    localStorage.clear()
  }
}