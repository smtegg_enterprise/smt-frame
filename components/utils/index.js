import utils from 'xe-utils'
import { http } from './http'
import { session, storage } from './storage'

export {
  utils,
  http,
  session,
  storage
}

const install = (app) => {
  app.config.globalProperties.$http = http
  app.config.globalProperties.$session = session
  app.config.globalProperties.$storage = storage
  app.config.globalProperties.$utils = utils
}

export default {
  install
}