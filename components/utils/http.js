import axios from 'axios'

let errorCount = 0
const handleError = (err) => {
  if (errorCount > 0) return
  console.error(err)
  handleNotify('error', err.message)

  //1秒内只提示一次错误
  errorCount++
  setTimeout(() => {
    errorCount = 0
  }, 1000)
}

const handleNotify = (type, message) => {
  if (http.notifications && http.notifications[type]) {
    http.notifications[type](message)
  }
}

const ax = axios.create()

export const http = {
  host: {},
  axios: ax,
  token: '',
  notifications: null,
  setup(options = {}) {
    if (options.host) {
      this.host = options.host
    }
    if (options.notifications) {
      this.notifications = options.notifications
    }
    if (options.token) {
      this.token = options.token
    }
    if (options.headers) {
      Object.keys(options.headers).forEach(key => {
        ax.defaults.headers[key] = options.headers[key]
      })
    }
  },
  buildUrl(url) {
    const ss = url.split(':')
    if (ss.length == 2 && this.host.hasOwnProperty([ss[0]])) {
      return this.host[ss[0]] + ss[1]
    }

    return url
  },
  request(options = {}) {
    options.url = this.buildUrl(options.url)

    return new Promise((resolve, reject) => {
      return this.axios(options)
        .then((response) => {
          const data = response.data
          if (data.status !== undefined) {
            if (data.status <= 200) {
              resolve(data)
            } else {
              reject(data)
            }
            if (data.message) {
              handleNotify(data.status <= 100 ? 'success' : 'alert', data.message)
            }
          } else {
            resolve(data)
          }
        })
        .catch((error) => {
          reject(error)
          handleError(error)
        })
    })
  },

  get(url, options = {}) {
    options.method = 'get'
    options.url = url
    return this.request(options)
  },

  delete(url, options = {}) {
    options.method = 'delete'
    options.url = url
    return this.request(options)
  },

  post(url, options = {}) {
    options.method = 'post'
    options.url = url
    options.data = options.data || {}
    return this.request(options)
  },

  put(url, options = {}) {
    options.method = 'put'
    options.url = url
    options.data = options.data || {}
    return this.request(options)
  },

  upload(url, options = {}) {
    options.method = 'post'
    options.url = url
    options.headers = {
      'Content-Type': 'multipart/form-data',
    }
    return this.request(options)
  },

  open(url, name, specs) {
    if (/\?/.test(url)) {
      url += `&token=${this.token}`
    } else {
      url += `?token=${this.token}`
    }
    window.open(url, name, specs)
  },

  download(url, options = {}, fileName) {
    options.url = url
    options.method = options.method || 'get'
    options.responseType = 'blob'
    return this.request(options).then((res) => {
      if (res.type == 'application/json') {
        res.text().then(t => {
          const data = JSON.parse(t)
          if (data.message) {
            handleNotify('alert', data.message)
          }
        })
      } else {
        let href = window.URL.createObjectURL(res)
        let link = document.createElement('a')
        link.href = href
        link.download = fileName || options.fileName || 'file'
        link.target = '_blank'
        document.body.appendChild(link)
        link.click()
        link.remove()
        window.URL.revokeObjectURL(href)
      }
    })
  }
}

export default http