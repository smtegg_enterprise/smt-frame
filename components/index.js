import './style.scss'
import BsUI from './bsui'
import Antd from './antd'
import Vxe from './vxe'
import Utils from './utils'
import { http } from './utils'
import mitt from 'mitt'

export * from './bsui'
export * from './antd'
export * from './vxe'
export * from './utils'

function install(app, config) {
  app.use(Antd)
  app.use(Vxe, config?.vxe)
  app.use(BsUI, config)
  app.use(Utils)

  const props = app.config.globalProperties

  http.setup({
    notifications: {
      alert: (message) => {
        props.$modal.warning({
          content: message
        })
      },
      error: (message) => {
        props.$message.error(message, 10)
      },
      success: (message) => {
        props.$message.success(message)
      }
    }
  })

  props.$events = mitt()

  const CONFIG = window['CONFIG']
  if (CONFIG) {
    props.$config = CONFIG
    http.setup({
      host: CONFIG.host
    })
  }
}

export default {
  install
}