import Icon from './icon'
import AppHeader from './app/Header.vue'
import AppContainer from './app/Container.vue'
import AppContent from './app/Content.vue'
import AppSider from './app/Sider.vue'
import AppMenu from './app/Menu.vue'
import AppNavbar from './app/Navbar.vue'

import BsLayout from './layout/Layout.vue'
import BsContent from './layout/Content.vue'
import BsToolbar from './layout/Toolbar.vue'
import BsTitle from './layout/Title.vue'

export { Icon } from './icon'

export { default as AppHeader } from './app/Header.vue'
export { default as AppContainer } from './app/Container.vue'
export { default as AppContent } from './app/Content.vue'
export { default as AppSider } from './app/Sider.vue'
export { default as AppMenu } from './app/Menu.vue'
export { default as AppNavbar } from './app/Navbar.vue'

export { default as BsLayout } from './layout/Layout.vue'
export { default as BsContent } from './layout/Content.vue'
export { default as BsToolbar } from './layout/Toolbar.vue'
export { default as BsTitle } from './layout/Title.vue'

const components = {
  AppHeader,
  AppSider,
  AppMenu,
  AppContainer,
  AppContent,
  AppNavbar,
  BsLayout,
  BsContent,
  BsToolbar,
  BsTitle
}

const install = function (app, config = {}) {
  app.use(Icon, config?.icon)
  Object.keys(components).forEach(name => {
    app.component(name, components[name])
  })
}

export default {
  install
}