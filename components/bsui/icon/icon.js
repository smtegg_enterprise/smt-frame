import { defineComponent, h } from 'vue'

export default defineComponent({
	name: 'Icon',
	props: {
		type: {
			type: String,
			required: true
		},
		size: {
			type: [Number, String],
			default: '1.25em'
		},
		fill: {
			type: String,
			default: 'currentColor'
		},
		prefix: {
			type: String,
			default: 'ri'
		},
		className: {
			type: String,
			default: 'icon'
		},
		src: {
			type: String,
			default: '/icon/remixicon.symbol.svg'
		}
	},
	setup(props) {
		let children = []
		const ss = props.type.split(':')
		if (ss.length == 2 && ss[0] == 'text') {
			const size = isNaN(Number(props.size)) ? props.size : props.size + 'px'
			children.push(h('span', {
				style: {
					fontSize: size
				}
			}, ss[1]))
		} else {
			let name = props.type
			if (props.prefix) {
				name = props.prefix + '-' + name
			}
			children.push(h('svg', {
				width: props.size,
				height: props.size,
				fill: props.fill
			}, [h('use', {
				'xlink:href': `${props.src}#${name}`
			})
			]))
		}

		return () => h('i', {
			'class': props.className
		}, children)
	}
})