import Icon from './icon'

export { default as Icon } from './icon'

export default {
	install(app, config = {}) {
		if (config.src) {
			Icon.props.src.default = config.src
		}
		if (config.size) {
			Icon.props.size.default = config.size
		}
		app.component(Icon.name, Icon)
	}
}