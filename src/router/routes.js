const routes = [{
  path: '/',
  component: () => import('../pages/home/Index.vue')
}, {
  path: '/layout',
  component: () => import('../pages/layout/Index.vue')
}, {
  path: '/calendar',
  component: () => import('../pages/calendar/Index.vue')
}, {
  path: '/table',
  component: () => import('../pages/table/Index.vue')
}]

export default routes