import { createRouter, createWebHistory } from 'vue-router'
import Layout from '../pages/layout/Layout.vue'
import NotFound from '../pages/layout/NotFound.vue'
import routeData from './routes'

const layout = {
  name: 'layout',
  path: '/_layout',
  component: Layout,
  children: []
}

export const routes = [layout]

routeData.filter(t => t.meta?.anonymous == true).forEach(route => {
  if (route.layout === false) {
    routes.push(route)
  } else {
    layout.children.push(route)
  }
})

function initRoute(menu, parent) {
  const reg = new RegExp(menu.path, 'i')
  const route = routeData.find(t => reg.test(t.path))
  if (route) {
    const options = {
      name: menu.id,
      path: menu.path,
      meta: Object.assign({
        title: menu.title,
        elements: menu.elements,
        anonymous: menu.anonymous,
        parent: parent,
        keep: route.keep
      }, route.meta),
      component: route.component
    }

    if (route.layout === false) {
      routes.push(options)
    } else {
      layout.children.push(options)
    }
  }
}

function buildChildren(menus, parent) {
  menus.forEach(menu => {
    if (menu.type == 'route' && menu.path) {
      initRoute(menu, parent)
    }
    if (menu.children && menu.children.length > 0) {
      buildChildren(menu.children, menu.id)
    }
  })
}

function buildRoutes(menus, homePath) {
  buildChildren(menus, null)

  layout.children.push({
    name: 'notFound',
    path: '/:pathMatch(.*)*',
    component: NotFound
  })

  if (homePath && homePath != '/') {
    routes.push({
      path: '/',
      redirect: homePath
    })
  }
}

export default (options) => {
  buildRoutes(options?.menus || [], options?.app?.appPath)

  const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
  })

  router.beforeEach((to) => {
    // if (!to.meta.anonymous &&
    //   window.sessionStorage.getItem('ACCESS_TOKEN') == null) {
    //   return { path: '/login', query: { redirect: to.fullPath } }
    // }
    return true
  })

  return router
}