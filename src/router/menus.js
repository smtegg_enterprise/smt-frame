export default [{
  id: 'home',
  title: '首页',
  icon: 'home-line',
  type: 'route',
  path: '/'
}, {
  id: 'layout',
  title: '页面布局',
  icon: 'layout-line',
  type: 'route',
  path: '/layout'
}, {
  id: 'demo',
  title: '开发组件',
  icon: 'file-edit-line',
  type: 'submenu',
  children: [{
    id: 'calendar',
    title: '资源甘特图',
    type: 'route',
    path: '/calendar'
  }, {
    id: 'table',
    title: '表格插件',
    type: 'route',
    path: '/table'
  }]
}, {
  id: 'texticon',
  title: '文字图标',
  icon: 'text:金',
  type: 'submenu'
}]