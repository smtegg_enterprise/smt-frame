import { createPinia, defineStore } from 'pinia'

export const useStore = defineStore('app', {
  state: () => {
    return {
      user: null,
      menus: []
    }
  }
})

export default (data = null) => {
  return {
    install(app) {
      app.use(createPinia())
      const store = useStore()

      if (data != null) {
        store.user = data.user
        store.menus = data.menus
      }

      app.config.globalProperties.$store = store
    }
  }
}