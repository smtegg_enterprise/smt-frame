import './style.css'
//import 'ant-design-vue/dist/reset.css'
//import '../lib/style.css'
import { createApp } from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue'
import SmtFrame from 'smt-frame'
import createRouter from './router'
import createStore from './store'
import menus from './router/menus'
const router = createRouter({ menus })
const store = createStore({ menus })

import vxeEnUS from 'vxe-table/lib/locale/lang/en-US'
vxeEnUS.vxe.actions = {
  edit: 'Edit',
  delete: 'Remove'
}
vxeEnUS.vxe.confirm = {
  delete: 'Are you sure to delete?'
}

import { createI18n } from 'vue-i18n'
import VXETable from 'vxe-table'
const i18n = createI18n({
  locale: 'en_US',
  messages: {
    en_US: {
      ...vxeEnUS
    }
  }
})
VXETable.setConfig({
  i18n: (key, args) => i18n.global.te(key) ? i18n.global.t(key, args) : key
})

createApp(App)
  .use(Antd)
  .use(SmtFrame)
  .use(router)
  .use(store)
  .mount('#app')
