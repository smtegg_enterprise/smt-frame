import { defineConfig } from 'vite'
import * as path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {

    }
  },
  define: {
    'process.env': {}
  },
  build: {
    lib: {
      entry: path.resolve(__dirname, './components/theme/index.js'),
      name: 'SmtFrame.Theme',
      fileName: 'index'
    },
    outDir: 'lib/theme',
    copyPublicDir: false
  }
})
