/** @type {import('tailwindcss').Config} */
import { colors, spacing, font } from './lib/theme/index.js'

colors.primary = '#00b96b'

export default {
  darkMode: 'class',
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./components/**/*.{vue,js,ts,jsx,tsx}"
  ],
  theme: {
    font,
    fontFamily: {
      sans: font.family.split(',')
    },
    extend: {
      colors: colors,
      borderColor: {
        DEFAULT: colors.border
      },
      spacing: spacing
    },
  },
  plugins: [],
}

