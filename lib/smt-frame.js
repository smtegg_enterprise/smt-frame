import { defineComponent as ys, h as ir, openBlock as S, createElementBlock as D, createElementVNode as Z, renderSlot as N, provide as Ja, computed as gr, resolveComponent as mt, normalizeClass as Ge, normalizeStyle as Rt, withDirectives as _s, vShow as bs, createCommentVNode as ve, createVNode as $r, inject as Rr, createBlock as I, unref as P, withCtx as A, Fragment as de, renderList as Re, createSlots as Ka, mergeProps as Oe, createTextVNode as ce, toDisplayString as fe, ref as Ar, reactive as qe, watch as yr, onMounted as At, onUnmounted as Ss, normalizeProps as _r, guardReactiveProps as br, useAttrs as ws, useSlots as Es, nextTick as Os, onBeforeUnmount as Ns, withModifiers as tn } from "vue";
import { useRoute as Xa, useRouter as Ds } from "vue-router";
import { Menu as or, Tabs as nn, Modal as kt, theme as Za, InputSearch as Ts, DatePicker as Cs, RangePicker as Rs, TimePicker as As, Pagination as kr, Form as ks, message as Ps, notification as xs, Popconfirm as Fs, Button as an } from "ant-design-vue";
import { Colgroup as Ms, Column as Qa, Edit as Is, Filter as Ls, Grid as Us, Keyboard as Bs, List as Ws, Pulldown as zs, Table as qs, VXETable as ja, Validator as Hs, t as Kr } from "vxe-table";
const sr = ys({
  name: "Icon",
  props: {
    type: {
      type: String,
      required: !0
    },
    size: {
      type: [Number, String],
      default: "1.25em"
    },
    fill: {
      type: String,
      default: "currentColor"
    },
    prefix: {
      type: String,
      default: "ri"
    },
    className: {
      type: String,
      default: "icon"
    },
    src: {
      type: String,
      default: "/icon/remixicon.symbol.svg"
    }
  },
  setup(e) {
    let r = [];
    const t = e.type.split(":");
    if (t.length == 2 && t[0] == "text") {
      const n = isNaN(Number(e.size)) ? e.size : e.size + "px";
      r.push(ir("span", {
        style: {
          fontSize: n
        }
      }, t[1]));
    } else {
      let n = e.type;
      e.prefix && (n = e.prefix + "-" + n), r.push(ir("svg", {
        width: e.size,
        height: e.size,
        fill: e.fill
      }, [
        ir("use", {
          "xlink:href": `${e.src}#${n}`
        })
      ]));
    }
    return () => ir("i", {
      class: e.className
    }, r);
  }
}), Gs = {
  install(e, r = {}) {
    r.src && (sr.props.src.default = r.src), r.size && (sr.props.size.default = r.size), e.component(sr.name, sr);
  }
}, Ye = (e, r) => {
  const t = e.__vccOpts || e;
  for (const [n, a] of r)
    t[n] = a;
  return t;
}, Ys = {}, Vs = { class: "app-header dark:text-white" }, Js = { class: "app-header-logo" }, Ks = { class: "app-header-content" }, Xs = { class: "app-header-tool" };
function Zs(e, r) {
  return S(), D("header", Vs, [
    Z("div", Js, [
      N(e.$slots, "logo")
    ]),
    Z("div", Ks, [
      N(e.$slots, "default")
    ]),
    Z("div", Xs, [
      N(e.$slots, "tool")
    ])
  ]);
}
const Qs = /* @__PURE__ */ Ye(Ys, [["render", Zs]]), js = {}, eu = { class: "app-container" };
function ru(e, r) {
  return S(), D("section", eu, [
    N(e.$slots, "default")
  ]);
}
const tu = /* @__PURE__ */ Ye(js, [["render", ru]]), nu = {}, au = { class: "app-content" };
function iu(e, r) {
  return S(), D("main", au, [
    N(e.$slots, "default")
  ]);
}
const ou = /* @__PURE__ */ Ye(nu, [["render", iu]]), su = { class: "app-sider-menu" }, uu = { class: "app-sider-footer" }, lu = {
  __name: "Sider",
  props: {
    title: String,
    fixed: {
      type: Boolean,
      default: !0
    },
    theme: {
      type: String,
      default: "dark"
    },
    collapsed: {
      type: Boolean,
      default: !0
    },
    width: {
      type: [String, Number],
      default: 200
    }
  },
  emits: ["update:collapsed"],
  setup(e, { emit: r }) {
    const t = e;
    Ja("theme", t.theme);
    const n = gr(() => [
      "app-sider",
      t.fixed ? "is-fixed" : "",
      t.collapsed ? "" : "is-opened",
      t.theme == "dark" ? "is-dark" : "is-light"
    ]), a = gr(() => t.collapsed ? null : {
      width: isNaN(Number(t.width)) ? t.width : t.width + "px"
    }), i = r, o = (s) => {
      i("update:collapsed", s);
    };
    return (s, c) => {
      const u = mt("icon");
      return S(), D("aside", {
        class: Ge(n.value),
        style: Rt(a.value)
      }, [
        e.fixed ? _s((S(), D("div", {
          key: 0,
          class: "app-sider-mask",
          onClick: c[0] || (c[0] = (l) => o(!0))
        }, null, 512)), [
          [bs, !e.collapsed]
        ]) : ve("", !0),
        Z("a", {
          class: "app-sider-hamburger hover:bg-opacity-80",
          href: "javascript:;",
          onClick: c[1] || (c[1] = (l) => o(!e.collapsed))
        }, [
          $r(u, {
            type: "menu-line",
            size: "16"
          }),
          N(s.$slots, "title")
        ]),
        Z("div", su, [
          N(s.$slots, "default")
        ]),
        Z("div", uu, [
          N(s.$slots, "footer")
        ])
      ], 6);
    };
  }
}, cu = ["href", "target"], fu = ["href", "target"], du = {
  __name: "Menu",
  props: {
    collapsed: Boolean,
    data: {
      type: Array,
      default: () => []
    }
  },
  setup(e) {
    const r = Rr("theme", "dark"), t = e, n = Xa(), a = gr(() => [n == null ? void 0 : n.path]), i = gr(() => t.collapsed ? [] : [n == null ? void 0 : n.meta.parent]);
    return (o, s) => {
      const c = mt("icon"), u = mt("router-link");
      return S(), I(P(or), {
        mode: "inline",
        theme: P(r),
        inlineCollapsed: e.collapsed,
        selectedKeys: a.value,
        openKeys: i.value
      }, {
        default: A(() => [
          (S(!0), D(de, null, Re(e.data, (l) => (S(), D(de, null, [
            l.type != "submenu" && !l.hidden ? (S(), I(P(or).Item, {
              key: l.path
            }, Ka({
              default: A(() => [
                l.type == "route" && l.target != "_blank" ? (S(), I(u, {
                  key: 0,
                  to: l.path
                }, {
                  default: A(() => [
                    N(o.$slots, "title", Oe({ ref_for: !0 }, l), () => [
                      ce(fe(l.title), 1)
                    ])
                  ]),
                  _: 2
                }, 1032, ["to"])) : (S(), D("a", {
                  key: 1,
                  href: l.path,
                  target: l.target
                }, [
                  N(o.$slots, "title", Oe({ ref_for: !0 }, l), () => [
                    ce(fe(l.title), 1)
                  ])
                ], 8, cu))
              ]),
              _: 2
            }, [
              l.icon ? {
                name: "icon",
                fn: A(() => [
                  $r(c, {
                    type: l.icon
                  }, null, 8, ["type"])
                ]),
                key: "0"
              } : void 0
            ]), 1024)) : ve("", !0),
            l.type == "submenu" ? (S(), I(P(or).SubMenu, {
              key: l.id
            }, {
              icon: A(() => [
                $r(c, {
                  type: l.icon
                }, null, 8, ["type"])
              ]),
              title: A(() => [
                N(o.$slots, "title", Oe({ ref_for: !0 }, l), () => [
                  ce(fe(l.title), 1)
                ])
              ]),
              default: A(() => [
                (S(!0), D(de, null, Re(l.children, (f) => (S(), D(de, null, [
                  f.hidden ? ve("", !0) : (S(), I(P(or).Item, {
                    key: f.path
                  }, {
                    default: A(() => [
                      f.type == "route" && f.target != "_blank" ? (S(), I(u, {
                        key: 0,
                        to: f.path
                      }, {
                        default: A(() => [
                          N(o.$slots, "title", Oe({ ref_for: !0 }, f), () => [
                            ce(fe(f.title), 1)
                          ])
                        ]),
                        _: 2
                      }, 1032, ["to"])) : (S(), D("a", {
                        key: 1,
                        href: f.path,
                        target: f.target
                      }, [
                        N(o.$slots, "title", Oe({ ref_for: !0 }, f), () => [
                          ce(fe(f.title), 1)
                        ])
                      ], 8, fu))
                    ]),
                    _: 2
                  }, 1024))
                ], 64))), 256))
              ]),
              _: 2
            }, 1024)) : ve("", !0)
          ], 64))), 256))
        ]),
        _: 3
      }, 8, ["theme", "inlineCollapsed", "selectedKeys", "openKeys"]);
    };
  }
};
function ei(e) {
  let r = null;
  if (e && (e.startsWith("{") && e.endsWith("}") || e.startsWith("[") && e.endsWith("]")))
    try {
      r = JSON.parse(e);
    } catch {
    }
  return r || e;
}
const gt = {
  get(e) {
    const r = sessionStorage.getItem(e);
    return ei(r);
  },
  set(e, r) {
    typeof r == "object" && (r = JSON.stringify(r)), sessionStorage[e] = r;
  },
  remove(e) {
    sessionStorage.removeItem(e);
  },
  clear() {
    sessionStorage.clear();
  }
}, Sr = {
  get(e) {
    const r = localStorage.getItem(e);
    return ei(r);
  },
  set(e, r) {
    typeof r == "object" && (r = JSON.stringify(r)), localStorage.setItem(e, r);
  },
  remove(e) {
    localStorage.removeItem(e);
  },
  clear() {
    localStorage.clear();
  }
}, vu = {
  __name: "Navbar",
  props: {
    options: {
      type: Array,
      default: () => []
    },
    fullPath: Boolean
  },
  emits: ["change", "remove"],
  setup(e, { expose: r, emit: t }) {
    const n = e, a = Xa(), i = Ds(), o = Ar(null), s = qe(gt.get("APP_NAVS") || n.options || []);
    yr(a, (v) => {
      const h = (n.fullPath ? v.fullPath : v.path).toLocaleLowerCase();
      o.value = s.find((m) => m.key == h), o.value ? o.value.path = v.fullPath : (o.value = {
        key: h,
        path: v.fullPath,
        title: v.meta.title,
        closable: v.meta.closable ?? s.length > 0
      }, s.push(o.value));
    }, {
      immediate: !0
    }), yr(s, (v) => {
      gt.set("APP_NAVS", v);
    }, {
      deep: !0
    });
    const c = t, u = (v) => {
      o.value = s.find((p) => p.key == v), i.push(o.value.path), c("change", o.value, s);
    }, l = (v, p) => {
      var h;
      if (p == "remove") {
        const m = s.findIndex(($) => $.key == v);
        if (m > -1 && (s.splice(m, 1), ((h = o.value) == null ? void 0 : h.key) == v && s.length > 0)) {
          const $ = s[s.length - 1].path;
          i.push($);
        }
        c("remove", v, s);
      }
    };
    return r({
      close: (v) => {
        var p;
        v || (v = (p = o.value) == null ? void 0 : p.key), v && l(v, "remove");
      }
    }), (v, p) => {
      var h;
      return S(), I(P(nn), {
        type: "editable-card",
        class: "app-nav",
        size: "small",
        hideAdd: "",
        animated: !1,
        "active-key": (h = o.value) == null ? void 0 : h.key,
        onChange: u,
        onEdit: l
      }, {
        default: A(() => [
          (S(!0), D(de, null, Re(s, (m) => (S(), I(P(nn).TabPane, {
            key: m.key,
            closable: m.closable
          }, {
            tab: A(() => [
              N(v.$slots, "tab", Oe({ ref_for: !0 }, m), () => [
                ce(fe(m.title), 1)
              ])
            ]),
            _: 2
          }, 1032, ["closable"]))), 128))
        ]),
        _: 3
      }, 8, ["active-key"]);
    };
  }
};
function ri(e) {
  return e && e.__esModule && Object.prototype.hasOwnProperty.call(e, "default") ? e.default : e;
}
var hu = {
  cookies: {
    path: "/"
  },
  treeOptions: {
    parentKey: "parentId",
    key: "id",
    children: "children"
  },
  parseDateFormat: "yyyy-MM-dd HH:mm:ss",
  firstDayOfWeek: 1
}, re = hu;
function pu(e, r, t) {
  if (e)
    if (e.forEach)
      e.forEach(r, t);
    else
      for (var n = 0, a = e.length; n < a; n++)
        r.call(t, e[n], n, e);
}
var x = pu, mu = Object.prototype.toString, ti = mu, gu = ti;
function $u(e) {
  return function(r) {
    return "[object " + e + "]" === gu.call(r);
  };
}
var Ve = $u, yu = Ve, _u = Array.isArray || yu("Array"), w = _u;
function bu(e, r) {
  return e && e.hasOwnProperty ? e.hasOwnProperty(r) : !1;
}
var G = bu, Su = G;
function wu(e, r, t) {
  if (e)
    for (var n in e)
      Su(e, n) && r.call(t, e[n], n, e);
}
var Je = wu, Eu = w, Ou = x, Nu = Je;
function Du(e, r, t) {
  return e && (Eu(e) ? Ou : Nu)(e, r, t);
}
var C = Du;
function Tu(e) {
  return function(r) {
    return typeof r === e;
  };
}
var Ae = Tu, Cu = Ae, Ru = Cu("function"), L = Ru, Au = C;
function ku(e, r) {
  var t = Object[e];
  return function(n) {
    var a = [];
    if (n) {
      if (t)
        return t(n);
      Au(n, r > 1 ? function(i) {
        a.push(["" + i, n[i]]);
      } : function() {
        a.push(arguments[r]);
      });
    }
    return a;
  };
}
var Pt = ku, Pu = Pt, xu = Pu("keys", 1), te = xu, Fu = ti, Mu = Je, Iu = x;
function Xr(e, r) {
  var t = e.__proto__.constructor;
  return r ? new t(r) : new t();
}
function ur(e, r) {
  return r ? ni(e, r) : e;
}
function ni(e, r) {
  if (e)
    switch (Fu.call(e)) {
      case "[object Object]": {
        var t = Object.create(Object.getPrototypeOf(e));
        return Mu(e, function(o, s) {
          t[s] = ur(o, r);
        }), t;
      }
      case "[object Date]":
      case "[object RegExp]":
        return Xr(e, e.valueOf());
      case "[object Array]":
      case "[object Arguments]": {
        var n = [];
        return Iu(e, function(o) {
          n.push(ur(o, r));
        }), n;
      }
      case "[object Set]": {
        var a = Xr(e);
        return a.forEach(function(o) {
          a.add(ur(o, r));
        }), a;
      }
      case "[object Map]": {
        var i = Xr(e);
        return i.forEach(function(o, s) {
          i.set(s, ur(o, r));
        }), i;
      }
    }
  return e;
}
function Lu(e, r) {
  return e && ni(e, r);
}
var xt = Lu, Uu = x, Bu = te, Wu = w, zu = xt, on = Object.assign;
function sn(e, r, t) {
  for (var n = r.length, a, i = 1; i < n; i++)
    a = r[i], Uu(Bu(r[i]), t ? function(o) {
      e[o] = zu(a[o], t);
    } : function(o) {
      e[o] = a[o];
    });
  return e;
}
var qu = function(e) {
  if (e) {
    var r = arguments;
    if (e === !0) {
      if (r.length > 1)
        return e = Wu(e[1]) ? [] : {}, sn(e, r, !0);
    } else
      return on ? on.apply(Object, r) : sn(e, r);
  }
  return e;
}, Y = qu, Hu = re, Gu = x, Yu = C, Vu = L, Ju = Y, $e = function() {
};
function Ku() {
  Gu(arguments, function(e) {
    Yu(e, function(r, t) {
      $e[t] = Vu(r) ? function() {
        var n = r.apply($e.$context, arguments);
        return $e.$context = null, n;
      } : r;
    });
  });
}
function Xu(e) {
  return Ju(Hu, e);
}
$e.VERSION = "3.5.25";
$e.mixin = Ku;
$e.setup = Xu;
var Zu = $e;
function Qu(e, r, t) {
  for (var n = e.length - 1; n >= 0; n--)
    r.call(t, e[n], n, e);
}
var Ft = Qu, ju = Ft, el = te;
function rl(e, r, t) {
  ju(el(e), function(n) {
    r.call(t, e[n], n, e);
  });
}
var ai = rl;
function tl(e) {
  return e === null;
}
var ne = tl, nl = ne;
function al(e, r) {
  return function(t) {
    return nl(t) ? r : t[e];
  };
}
var ke = al, il = C, ol = L, sl = ke;
function ul(e, r, t) {
  var n = {};
  if (e)
    if (r)
      ol(r) || (r = sl(r)), il(e, function(a, i) {
        n[i] = r.call(t, a, i, e);
      });
    else
      return e;
  return n;
}
var ll = ul;
function cl(e) {
  return e ? e.constructor === Object : !1;
}
var Pe = cl, un = w, ln = Pe, fl = C;
function ii(e, r) {
  return ln(e) && ln(r) || un(e) && un(r) ? (fl(r, function(t, n) {
    e[n] = ii(e[n], t);
  }), e) : r;
}
var dl = function(e) {
  e || (e = {});
  for (var r = arguments, t = r.length, n, a = 1; a < t; a++)
    n = r[a], n && ii(e, n);
  return e;
}, vl = dl, hl = C;
function pl(e, r, t) {
  var n = [];
  if (e && arguments.length > 1) {
    if (e.map)
      return e.map(r, t);
    hl(e, function() {
      n.push(r.apply(t, arguments));
    });
  }
  return n;
}
var xe = pl, ml = G, gl = w;
function $l(e, r, t, n, a) {
  return function(i, o, s) {
    if (i && o) {
      if (e && i[e])
        return i[e](o, s);
      if (r && gl(i)) {
        for (var c = 0, u = i.length; c < u; c++)
          if (!!o.call(s, i[c], c, i) === n)
            return [!0, !1, c, i[c]][t];
      } else
        for (var l in i)
          if (ml(i, l) && !!o.call(s, i[l], l, i) === n)
            return [!0, !1, l, i[l]][t];
    }
    return a;
  };
}
var Pr = $l, yl = Pr, _l = yl("some", 1, 0, !0, !1), oi = _l, bl = Pr, Sl = bl("every", 1, 1, !1, !0), si = Sl, wl = G;
function El(e, r) {
  if (e) {
    if (e.includes)
      return e.includes(r);
    for (var t in e)
      if (wl(e, t) && r === e[t])
        return !0;
  }
  return !1;
}
var Ke = El, cn = w, fn = Ke;
function Ol(e, r) {
  var t, n = 0;
  if (cn(e) && cn(r)) {
    for (t = r.length; n < t; n++)
      if (!fn(e, r[n]))
        return !1;
    return !0;
  }
  return fn(e, r);
}
var ui = Ol, dn = C, Nl = Ke, Dl = L, Tl = ke;
function Cl(e, r, t) {
  var n = [];
  if (r) {
    Dl(r) || (r = Tl(r));
    var a, i = {};
    dn(e, function(o, s) {
      a = r.call(t, o, s, e), i[a] || (i[a] = 1, n.push(o));
    });
  } else
    dn(e, function(o) {
      Nl(n, o) || n.push(o);
    });
  return n;
}
var li = Cl, Rl = xe;
function Al(e) {
  return Rl(e, function(r) {
    return r;
  });
}
var Mt = Al, kl = li, Pl = Mt;
function xl() {
  for (var e = arguments, r = [], t = 0, n = e.length; t < n; t++)
    r = r.concat(Pl(e[t]));
  return kl(r);
}
var Fl = xl, Ml = "undefined", V = Ml, Il = V, Ll = Ae, Ul = Ll(Il), Q = Ul, Bl = ne, Wl = Q;
function zl(e) {
  return Bl(e) || Wl(e);
}
var pe = zl, ql = /(.+)?\[(\d+)\]$/, ci = ql;
function Hl(e) {
  return e ? e.splice && e.join ? e : ("" + e).replace(/(\[\d+\])\.?/g, "$1.").replace(/\.$/, "").split(".") : [];
}
var It = Hl, Gl = ci, Yl = It, Vl = G, Jl = Q, fi = pe;
function Kl(e, r, t) {
  if (fi(e))
    return t;
  var n = Zl(e, r);
  return Jl(n) ? t : n;
}
function Xl(e, r) {
  var t = r ? r.match(Gl) : "";
  return t ? t[1] ? e[t[1]] ? e[t[1]][t[2]] : void 0 : e[t[2]] : e[r];
}
function Zl(e, r) {
  if (e) {
    var t, n, a, i = 0;
    if (e[r] || Vl(e, r))
      return e[r];
    if (n = Yl(r), a = n.length, a) {
      for (t = e; i < a; i++)
        if (t = Xl(t, n[i]), fi(t))
          return i === a - 1 ? t : void 0;
    }
    return t;
  }
}
var Xe = Kl, vn = x, Ql = Mt, hn = xe, pn = w, jl = L, ec = Pe, mn = Q, rc = ne, tc = pe, nc = Xe, ac = ke, ic = "asc", oc = "desc";
function $t(e, r) {
  return mn(e) ? 1 : rc(e) ? mn(r) ? -1 : 1 : e && e.localeCompare ? e.localeCompare(r) : e > r ? 1 : -1;
}
function sc(e, r, t) {
  return function(n, a) {
    var i = n[e], o = a[e];
    return i === o ? t ? t(n, a) : 0 : r.order === oc ? $t(o, i) : $t(i, o);
  };
}
function uc(e, r, t, n) {
  var a = [];
  return t = pn(t) ? t : [t], vn(t, function(i, o) {
    if (i) {
      var s = i, c;
      pn(i) ? (s = i[0], c = i[1]) : ec(i) && (s = i.field, c = i.order), a.push({
        field: s,
        order: c || ic
      }), vn(r, jl(s) ? function(u, l) {
        u[o] = s.call(n, u.data, l, e);
      } : function(u) {
        u[o] = s ? nc(u.data, s) : u.data;
      });
    }
  }), a;
}
function lc(e, r, t) {
  if (e) {
    if (tc(r))
      return Ql(e).sort($t);
    for (var n, a = hn(e, function(s) {
      return { data: s };
    }), i = uc(e, a, r, t), o = i.length - 1; o >= 0; )
      n = sc(o, i[o], n), o--;
    return n && (a = a.sort(n)), hn(a, ac("data"));
  }
  return [];
}
var Lt = lc, cc = Lt, fc = cc, dc = fc;
function vc(e, r) {
  return e >= r ? e : (e = e >> 0) + Math.round(Math.random() * ((r || 9) - e));
}
var di = vc, hc = Pt, pc = hc("values", 0), Fe = pc, mc = di, gc = Fe;
function $c(e) {
  for (var r, t = [], n = gc(e), a = n.length - 1; a >= 0; a--)
    r = a > 0 ? mc(0, a) : 0, t.push(n[r]), n.splice(r, 1);
  return t;
}
var vi = $c, yc = vi;
function _c(e, r) {
  var t = yc(e);
  return arguments.length <= 1 ? t[0] : (r < t.length && (t.length = r || 0), t);
}
var bc = _c;
function Sc(e) {
  return function(r) {
    if (r) {
      var t = e(r && r.replace ? r.replace(/,/g, "") : r);
      if (!isNaN(t))
        return t;
    }
    return 0;
  };
}
var hi = Sc, wc = hi, Ec = wc(parseFloat), be = Ec, gn = be;
function Oc(e, r, t) {
  var n = [], a = arguments.length;
  if (e) {
    if (r = a >= 2 ? gn(r) : 0, t = a >= 3 ? gn(t) : e.length, e.slice)
      return e.slice(r, t);
    for (; r < t; r++)
      n.push(e[r]);
  }
  return n;
}
var Se = Oc, Nc = C;
function Dc(e, r, t) {
  var n = [];
  if (e && r) {
    if (e.filter)
      return e.filter(r, t);
    Nc(e, function(a, i) {
      r.call(t, a, i, e) && n.push(a);
    });
  }
  return n;
}
var Tc = Dc, Cc = Pr, Rc = Cc("", 0, 2, !0), Ac = Rc, kc = Pr, Pc = kc("find", 1, 3, !0), xc = Pc, Fc = w, Mc = Fe;
function Ic(e, r, t) {
  if (e) {
    Fc(e) || (e = Mc(e));
    for (var n = e.length - 1; n >= 0; n--)
      if (r.call(t, e[n], n, e))
        return e[n];
  }
}
var Lc = Ic, Uc = te;
function Bc(e, r, t) {
  if (e) {
    var n, a, i = 0, o = null, s = t, c = arguments.length > 2, u = Uc(e);
    if (e.length && e.reduce)
      return a = function() {
        return r.apply(o, arguments);
      }, c ? e.reduce(a, s) : e.reduce(a);
    for (c && (i = 1, s = e[u[0]]), n = u.length; i < n; i++)
      s = r.call(o, s, e[u[i]], i, e);
    return s;
  }
}
var Wc = Bc, zc = w;
function qc(e, r, t, n) {
  if (zc(e) && e.copyWithin)
    return e.copyWithin(r, t, n);
  var a, i, o = r >> 0, s = t >> 0, c = e.length, u = arguments.length > 3 ? n >> 0 : c;
  if (o < c && (o = o >= 0 ? o : c + o, o >= 0 && (s = s >= 0 ? s : c + s, u = u >= 0 ? u : c + u, s < u)))
    for (a = 0, i = e.slice(s, u); o < c && !(i.length <= a); o++)
      e[o] = i[a++];
  return e;
}
var Hc = qc, Gc = w;
function Yc(e, r) {
  var t, n = [], a = r >> 0 || 1;
  if (Gc(e))
    if (a >= 0 && e.length > a)
      for (t = 0; t < e.length; )
        n.push(e.slice(t, t + a)), t += a;
    else
      n = e.length ? [e] : e;
  return n;
}
var Vc = Yc, Jc = xe, Kc = ke;
function Xc(e, r) {
  return Jc(e, Kc(r));
}
var pi = Xc, Zc = L, $n = pe, Qc = Xe, jc = x;
function ef(e) {
  return function(r, t) {
    if (r && r.length) {
      var n, a;
      return jc(r, function(i, o) {
        t && (i = Zc(t) ? t(i, o, r) : Qc(i, t)), !$n(i) && ($n(n) || e(n, i)) && (a = o, n = i);
      }), r[a];
    }
    return n;
  };
}
var mi = ef, rf = mi, tf = rf(function(e, r) {
  return e < r;
}), gi = tf, nf = pi, af = gi;
function of(e) {
  var r, t, n, a = [];
  if (e && e.length)
    for (r = 0, t = af(e, function(i) {
      return i ? i.length : 0;
    }), n = t ? t.length : 0; r < n; r++)
      a.push(nf(e, r));
  return a;
}
var $i = of, sf = $i;
function uf() {
  return sf(arguments);
}
var lf = uf, cf = Fe, ff = C;
function df(e, r) {
  var t = {};
  return r = r || [], ff(cf(e), function(n, a) {
    t[n] = r[a];
  }), t;
}
var vf = df, yi = w, hf = x;
function _i(e, r) {
  var t = [];
  return hf(e, function(n) {
    t = t.concat(yi(n) ? r ? _i(n, r) : n : [n]);
  }), t;
}
function pf(e, r) {
  return yi(e) ? _i(e, r) : [];
}
var mf = pf, gf = xe, $f = w;
function yf(e, r) {
  for (var t = 0, n = r.length; e && t < n; )
    e = e[r[t++]];
  return n && e ? e : 0;
}
function _f(e, r) {
  for (var t, n = arguments, a = [], i = [], o = 2, s = n.length; o < s; o++)
    a.push(n[o]);
  if ($f(r)) {
    for (s = r.length - 1, o = 0; o < s; o++)
      i.push(r[o]);
    r = r[s];
  }
  return gf(e, function(c) {
    if (i.length && (c = yf(c, i)), t = c[r] || r, t && t.apply)
      return t.apply(c, a);
  });
}
var bf = _f;
function Sf(e, r) {
  try {
    delete e[r];
  } catch {
    e[r] = void 0;
  }
}
var bi = Sf, wf = w, Ef = Ft, Of = ai;
function Nf(e, r, t) {
  return e && (wf(e) ? Ef : Of)(e, r, t);
}
var Si = Nf, Df = Ae, Tf = Df("object"), xr = Tf, Cf = bi, Rf = Pe, Af = xr, kf = w, Pf = ne, xf = Y, Ff = Je;
function Mf(e, r, t) {
  if (e) {
    var n, a = arguments.length > 1 && (Pf(r) || !Af(r)), i = a ? t : r;
    if (Rf(e))
      Ff(e, a ? function(o, s) {
        e[s] = r;
      } : function(o, s) {
        Cf(e, s);
      }), i && xf(e, i);
    else if (kf(e)) {
      if (a)
        for (n = e.length; n > 0; )
          n--, e[n] = r;
      else
        e.length = 0;
      i && e.push.apply(e, i);
    }
  }
  return e;
}
var wi = Mf, If = bi, Lf = L, Uf = w, Bf = C, Wf = x, zf = Si, qf = wi, Hf = pe;
function Gf(e) {
  return function(r, t) {
    return t === e;
  };
}
function Yf(e, r, t) {
  if (e) {
    if (!Hf(r)) {
      var n = [], a = [];
      return Lf(r) || (r = Gf(r)), Bf(e, function(i, o, s) {
        r.call(t, i, o, s) && n.push(o);
      }), Uf(e) ? zf(n, function(i, o) {
        a.push(e[i]), e.splice(i, 1);
      }) : (a = {}, Wf(n, function(i) {
        a[i] = e[i], If(e, i);
      })), a;
    }
    return qf(e);
  }
  return e;
}
var Ei = Yf, Vf = re, Jf = Lt, Kf = xt, Xf = pe, yt = C, Zf = Ei, Qf = Y;
function jf(e, r) {
  yt(e, function(t) {
    t[r] && !t[r].length && Zf(t, r);
  });
}
function ed(e, r) {
  var t = Qf({}, Vf.treeOptions, r), n = t.strict, a = t.key, i = t.parentKey, o = t.children, s = t.mapChildren, c = t.sortKey, u = t.reverse, l = t.data, f = [], v = {}, p = {}, h, m, $;
  return c && (e = Jf(Kf(e), c), u && (e = e.reverse())), yt(e, function(y) {
    h = y[a], p[h] = !0;
  }), yt(e, function(y) {
    h = y[a], l ? (m = {}, m[l] = y) : m = y, $ = y[i], v[h] = v[h] || [], v[$] = v[$] || [], v[$].push(m), m[a] = h, m[i] = $, m[o] = v[h], s && (m[s] = v[h]), (!n || n && Xf($)) && (p[$] || f.push(m));
  }), n && jf(e, o), f;
}
var rd = ed, td = re, nd = C, ad = Y;
function Oi(e, r, t) {
  var n = t.children, a = t.data, i = t.clear;
  return nd(r, function(o) {
    var s = o[n];
    a && (o = o[a]), e.push(o), s && s.length && Oi(e, s, t), i && delete o[n];
  }), e;
}
function id(e, r) {
  return Oi([], e, ad({}, td.treeOptions, r));
}
var od = id;
function sd(e) {
  return function(r, t, n, a) {
    var i = n || {}, o = i.children || "children";
    return e(null, r, t, a, [], [], o, i);
  };
}
var Fr = sd, ud = Fr;
function Ni(e, r, t, n, a, i, o, s) {
  if (r) {
    var c, u, l, f, v, p;
    for (u = 0, l = r.length; u < l; u++) {
      if (c = r[u], f = a.concat(["" + u]), v = i.concat([c]), t.call(n, c, u, r, f, e, v))
        return { index: u, item: c, path: f, items: r, parent: e, nodes: v };
      if (o && c && (p = Ni(c, c[o], t, n, f.concat([o]), v, o), p))
        return p;
    }
  }
}
var ld = ud(Ni), cd = ld, fd = Fr, dd = C;
function Di(e, r, t, n, a, i, o, s) {
  var c, u;
  dd(r, function(l, f) {
    c = a.concat(["" + f]), u = i.concat([l]), t.call(n, l, f, r, c, e, u), l && o && (c.push(o), Di(l, l[o], t, n, c, u, o));
  });
}
var vd = fd(Di), Ti = vd, hd = Fr, pd = xe;
function Ci(e, r, t, n, a, i, o, s) {
  var c, u, l, f = s.mapChildren || o;
  return pd(r, function(v, p) {
    return c = a.concat(["" + p]), u = i.concat([v]), l = t.call(n, v, p, r, c, e, u), l && v && o && v[o] && (l[f] = Ci(v, v[o], t, n, c, u, o, s)), l;
  });
}
var md = hd(Ci), gd = md, $d = Ti;
function yd(e, r, t, n) {
  var a = [];
  return e && r && $d(e, function(i, o, s, c, u, l) {
    r.call(n, i, o, s, c, u, l) && a.push(i);
  }, t), a;
}
var _d = yd, bd = Fr, Sd = x, wd = Y;
function Ri(e, r, t, n, a, i, o, s, c) {
  var u, l, f, v, p, h = [], m = c.original, $ = c.data, y = c.mapChildren || s, O = c.isEvery;
  return Sd(t, function(g, _) {
    u = i.concat(["" + _]), l = o.concat([g]), v = e && !O || n.call(a, g, _, t, u, r, l), p = s && g[s], v || p ? (m ? f = g : (f = wd({}, g), $ && (f[$] = g)), f[y] = Ri(v, g, g[s], n, a, u, l, s, c), (v || f[y].length) && h.push(f)) : v && h.push(f);
  }), h;
}
var Ed = bd(function(e, r, t, n, a, i, o, s) {
  return Ri(0, e, r, t, n, a, i, o, s);
}), Od = Ed;
function Nd(e, r) {
  if (e.indexOf)
    return e.indexOf(r);
  for (var t = 0, n = e.length; t < n; t++)
    if (r === e[t])
      return t;
}
var Ai = Nd;
function Dd(e, r) {
  if (e.lastIndexOf)
    return e.lastIndexOf(r);
  for (var t = e.length - 1; t >= 0; t--)
    if (r === e[t])
      return t;
  return -1;
}
var ki = Dd, Td = Ae, Cd = Td("number"), j = Cd, Rd = j;
function Ad(e) {
  return Rd(e) && isNaN(e);
}
var kd = Ad, Pd = Ae, xd = Pd("string"), ae = xd, Fd = Ve, Md = Fd("Date"), me = Md, Id = parseInt, Ze = Id;
function Ld(e) {
  return Date.UTC(e.y, e.M || 0, e.d || 1, e.H || 0, e.m || 0, e.s || 0, e.S || 0);
}
var Ud = Ld;
function Bd(e) {
  return e.getTime();
}
var W = Bd, wr = Ze, yn = Ud, Wd = W, zd = ae, qd = me;
function Qe(e) {
  return "(\\d{" + e + "})";
}
function Hd(e) {
  return e < 10 ? e * 100 : e < 100 ? e * 10 : e;
}
function _n(e) {
  return isNaN(e) ? e : wr(e);
}
var we = Qe(2), Ne = Qe("1,2"), Pi = Qe("1,7"), xi = Qe("3,4"), Fi = ".{1}", Ue = Fi + Ne, Mi = "(([zZ])|([-+]\\d{2}:?\\d{2}))", bn = [xi, Ue, Ue, Ue, Ue, Ue, Fi + Pi, Mi], _t = [];
for (var Zr = bn.length - 1; Zr >= 0; Zr--) {
  for (var Sn = "", ye = 0; ye < Zr + 1; ye++)
    Sn += bn[ye];
  _t.push(new RegExp("^" + Sn + "$"));
}
function Gd(e) {
  for (var r, t = {}, n = 0, a = _t.length; n < a; n++)
    if (r = e.match(_t[n]), r) {
      t.y = r[1], t.M = r[2], t.d = r[3], t.H = r[4], t.m = r[5], t.s = r[6], t.S = r[7], t.Z = r[8];
      break;
    }
  return t;
}
var wn = [
  ["yyyy", xi],
  ["yy", we],
  ["MM", we],
  ["M", Ne],
  ["dd", we],
  ["d", Ne],
  ["HH", we],
  ["H", Ne],
  ["mm", we],
  ["m", Ne],
  ["ss", we],
  ["s", Ne],
  ["SSS", Qe(3)],
  ["S", Pi],
  ["Z", Mi]
], Ii = {}, Li = ["\\[([^\\]]+)\\]"];
for (var ye = 0; ye < wn.length; ye++) {
  var Qr = wn[ye];
  Ii[Qr[0]] = Qr[1] + "?", Li.push(Qr[0]);
}
var Yd = new RegExp(Li.join("|"), "g"), En = {};
function Vd(e, r) {
  var t = En[r];
  if (!t) {
    var n = [], a = r.replace(/([$(){}*+.?\\^|])/g, "\\$1").replace(Yd, function(l, f) {
      var v = l.charAt(0);
      return v === "[" ? f : (n.push(v), Ii[l]);
    });
    t = En[r] = {
      _i: n,
      _r: new RegExp(a)
    };
  }
  var i = {}, o = e.match(t._r);
  if (o) {
    for (var s = t._i, c = 1, u = o.length; c < u; c++)
      i[s[c - 1]] = o[c];
    return i;
  }
  return i;
}
function Jd(e) {
  if (/^[zZ]/.test(e.Z))
    return new Date(yn(e));
  var r = e.Z.match(/([-+])(\d{2}):?(\d{2})/);
  return r ? new Date(yn(e) - (r[1] === "-" ? -1 : 1) * wr(r[2]) * 36e5 + wr(r[3]) * 6e4) : /* @__PURE__ */ new Date("");
}
function Kd(e, r) {
  if (e) {
    var t = qd(e);
    if (t || !r && /^[0-9]{11,15}$/.test(e))
      return new Date(t ? Wd(e) : wr(e));
    if (zd(e)) {
      var n = r ? Vd(e, r) : Gd(e);
      if (n.y)
        return n.M && (n.M = _n(n.M) - 1), n.S && (n.S = Hd(_n(n.S.substring(0, 3)))), n.Z ? Jd(n) : new Date(n.y, n.M || 0, n.d || 1, n.H || 0, n.m || 0, n.s || 0, n.S || 0);
    }
  }
  return /* @__PURE__ */ new Date("");
}
var U = Kd;
function Xd() {
  return /* @__PURE__ */ new Date();
}
var Mr = Xd, Zd = me, Qd = U, jd = Mr;
function ev(e) {
  var r, t = e ? Qd(e) : jd();
  return Zd(t) ? (r = t.getFullYear(), r % 4 === 0 && (r % 100 !== 0 || r % 400 === 0)) : !1;
}
var Ui = ev, rv = w, tv = G;
function nv(e, r, t) {
  if (e) {
    if (rv(e))
      for (var n = 0, a = e.length; n < a && r.call(t, e[n], n, e) !== !1; n++)
        ;
    else
      for (var i in e)
        if (tv(e, i) && r.call(t, e[i], i, e) === !1)
          break;
  }
}
var av = nv, iv = w, ov = G;
function sv(e, r, t) {
  if (e) {
    var n, a;
    if (iv(e))
      for (n = e.length - 1; n >= 0 && r.call(t, e[n], n, e) !== !1; n--)
        ;
    else
      for (a = ov(e), n = a.length - 1; n >= 0 && r.call(t, e[a[n]], a[n], e) !== !1; n--)
        ;
  }
}
var uv = sv, lv = w, cv = ae, fv = G;
function dv(e, r) {
  return function(t, n) {
    if (t) {
      if (t[e])
        return t[e](n);
      if (cv(t) || lv(t))
        return r(t, n);
      for (var a in t)
        if (fv(t, a) && n === t[a])
          return a;
    }
    return -1;
  };
}
var Bi = dv, vv = Bi, hv = Ai, pv = vv("indexOf", hv), mv = pv, gv = Bi, $v = ki, yv = gv("lastIndexOf", $v), Wi = yv, _v = w, bv = ae, Sv = C;
function wv(e) {
  var r = 0;
  return bv(e) || _v(e) ? e.length : (Sv(e, function() {
    r++;
  }), r);
}
var zi = wv, Ev = j;
function Ov(e) {
  return Ev(e) && isFinite(e);
}
var Nv = Ov, Dv = w, Tv = ne, Cv = function(e) {
  return !Tv(e) && !isNaN(e) && !Dv(e) && e % 1 === 0;
}, qi = Cv, Rv = w, Av = qi, kv = ne;
function Pv(e) {
  return !kv(e) && !isNaN(e) && !Rv(e) && !Av(e);
}
var xv = Pv, Fv = Ae, Mv = Fv("boolean"), Hi = Mv, Iv = Ve, Lv = Iv("RegExp"), Ut = Lv, Uv = Ve, Bv = Uv("Error"), Gi = Bv;
function Wv(e) {
  return e ? e.constructor === TypeError : !1;
}
var zv = Wv;
function qv(e) {
  for (var r in e)
    return !1;
  return !0;
}
var Yi = qv, Hv = V, Gv = typeof Symbol !== Hv;
function Yv(e) {
  return Gv && Symbol.isSymbol ? Symbol.isSymbol(e) : typeof e == "symbol";
}
var Vi = Yv, Vv = Ve, Jv = Vv("Arguments"), Kv = Jv, Xv = ae, Zv = j;
function Qv(e) {
  return !!(e && Xv(e.nodeName) && Zv(e.nodeType));
}
var jv = Qv, eh = V, rh = typeof document === eh ? 0 : document, Bt = rh, th = Bt;
function nh(e) {
  return !!(e && th && e.nodeType === 9);
}
var ah = nh, ih = V, oh = typeof window === ih ? 0 : window, Ji = oh, sh = Ji;
function uh(e) {
  return !!(sh && (e && e === e.window));
}
var lh = uh, ch = V, fh = typeof FormData !== ch;
function dh(e) {
  return fh && e instanceof FormData;
}
var vh = dh, hh = V, ph = typeof Map !== hh;
function mh(e) {
  return ph && e instanceof Map;
}
var gh = mh, $h = V, yh = typeof WeakMap !== $h;
function _h(e) {
  return yh && e instanceof WeakMap;
}
var bh = _h, Sh = V, wh = typeof Set !== Sh;
function Eh(e) {
  return wh && e instanceof Set;
}
var Oh = Eh, Nh = V, Dh = typeof WeakSet !== Nh;
function Th(e) {
  return Dh && e instanceof WeakSet;
}
var Ch = Th, Rh = L, Ah = ae, kh = w, Ph = G;
function xh(e) {
  return function(r, t, n) {
    if (r && Rh(t)) {
      if (kh(r) || Ah(r))
        return e(r, t, n);
      for (var a in r)
        if (Ph(r, a) && t.call(n, r[a], a, r))
          return a;
    }
    return -1;
  };
}
var Ki = xh, Fh = Ki, Mh = Fh(function(e, r, t) {
  for (var n = 0, a = e.length; n < a; n++)
    if (r.call(t, e[n], n, e))
      return n;
  return -1;
}), Wt = Mh, On = j, Nn = w, Dn = ae, Ih = Ut, Lh = me, Uh = Hi, Bh = Q, Tn = te, Wh = si;
function Xi(e, r, t, n, a, i, o) {
  if (e === r)
    return !0;
  if (e && r && !On(e) && !On(r) && !Dn(e) && !Dn(r)) {
    if (Ih(e))
      return t("" + e, "" + r, a, i, o);
    if (Lh(e) || Uh(e))
      return t(+e, +r, a, i, o);
    var s, c, u, l = Nn(e), f = Nn(r);
    if (l || f ? l && f : e.constructor === r.constructor)
      return c = Tn(e), u = Tn(r), n && (s = n(e, r, a)), c.length === u.length ? Bh(s) ? Wh(c, function(v, p) {
        return v === u[p] && Xi(e[v], r[u[p]], t, n, l || f ? p : v, e, r);
      }) : !!s : !1;
  }
  return t(e, r, a, i, o);
}
var Zi = Xi;
function zh(e, r) {
  return e === r;
}
var Qi = zh, qh = Zi, Hh = Qi;
function Gh(e, r) {
  return qh(e, r, Hh);
}
var ji = Gh, Cn = te, Yh = Wt, Rn = ji, Vh = oi, Jh = ui;
function Kh(e, r) {
  var t = Cn(e), n = Cn(r);
  if (n.length) {
    if (Jh(t, n))
      return Vh(n, function(a) {
        return Yh(t, function(i) {
          return i === a && Rn(e[i], r[a]);
        }) > -1;
      });
  } else
    return !0;
  return Rn(e, r);
}
var Xh = Kh, An = Zi, kn = Qi, Zh = L, Qh = Q;
function jh(e, r, t) {
  return Zh(t) ? An(e, r, function(n, a, i, o, s) {
    var c = t(n, a, i, o, s);
    return Qh(c) ? kn(n, a) : !!c;
  }, t) : An(e, r, kn);
}
var ep = jh, rp = Vi, tp = me, np = w, ap = Ut, ip = Gi, op = ne;
function sp(e) {
  return op(e) ? "null" : rp(e) ? "symbol" : tp(e) ? "date" : np(e) ? "array" : ap(e) ? "regexp" : ip(e) ? "error" : typeof e;
}
var up = sp, lp = 0;
function cp(e) {
  return [e, ++lp].join("");
}
var fp = cp, dp = Ki, vp = dp(function(e, r, t) {
  for (var n = e.length - 1; n >= 0; n--)
    if (r.call(t, e[n], n, e))
      return n;
  return -1;
}), hp = vp, pp = Pe, mp = ae;
function gp(e) {
  if (pp(e))
    return e;
  if (mp(e))
    try {
      return JSON.parse(e);
    } catch {
    }
  return {};
}
var $p = gp, yp = pe;
function _p(e) {
  return yp(e) ? "" : JSON.stringify(e);
}
var bp = _p, Sp = Pt, wp = Sp("entries", 2), Ep = wp, Op = L, Np = w, Dp = C, Tp = Wt;
function Cp(e, r) {
  return function(t, n) {
    var a, i, o = {}, s = [], c = this, u = arguments, l = u.length;
    if (!Op(n)) {
      for (i = 1; i < l; i++)
        a = u[i], s.push.apply(s, Np(a) ? a : [a]);
      n = 0;
    }
    return Dp(t, function(f, v) {
      ((n ? n.call(c, f, v, t) : Tp(s, function(p) {
        return p === v;
      }) > -1) ? e : r) && (o[v] = f);
    }), o;
  };
}
var eo = Cp, Rp = eo, Ap = Rp(1, 0), kp = Ap, Pp = eo, xp = Pp(0, 1), Fp = xp, Mp = Fe;
function Ip(e) {
  return Mp(e)[0];
}
var Lp = Ip, Up = Fe;
function Bp(e) {
  var r = Up(e);
  return r[r.length - 1];
}
var Wp = Bp, zp = ci, qp = It, lr = G;
function Hp(e, r) {
  if (e) {
    if (lr(e, r))
      return !0;
    var t, n, a, i, o, s, c = qp(r), u = 0, l = c.length;
    for (o = e; u < l && (s = !1, t = c[u], i = t ? t.match(zp) : "", i ? (n = i[1], a = i[2], n ? o[n] && lr(o[n], a) && (s = !0, o = o[n][a]) : lr(o, a) && (s = !0, o = o[a])) : lr(o, t) && (s = !0, o = o[t]), s); u++)
      if (u === l - 1)
        return !0;
  }
  return !1;
}
var Gp = Hp, Pn = Ze, Yp = It, Vp = G, xn = /(.+)?\[(\d+)\]$/;
function Jp(e, r, t, n, a) {
  if (e[r])
    t && (e[r] = a);
  else {
    var i, o, s = r ? r.match(xn) : null;
    if (t)
      o = a;
    else {
      var c = n ? n.match(xn) : null;
      c && !c[1] ? o = new Array(Pn(c[2]) + 1) : o = {};
    }
    return s ? s[1] ? (i = Pn(s[2]), e[s[1]] ? t ? e[s[1]][i] = o : e[s[1]][i] ? o = e[s[1]][i] : e[s[1]][i] = o : (e[s[1]] = new Array(i + 1), e[s[1]][i] = o)) : e[s[2]] = o : e[r] = o, o;
  }
  return e[r];
}
function Kp(e, r, t) {
  if (e) {
    if ((e[r] || Vp(e, r)) && !Fn(r))
      e[r] = t;
    else
      for (var n = e, a = Yp(r), i = a.length, o = 0; o < i; o++)
        if (!Fn(a[o])) {
          var s = o === i - 1;
          n = Jp(n, a[o], s, s ? null : a[o + 1], t);
        }
  }
  return e;
}
function Fn(e) {
  return e === "__proto__" || e === "constructor" || e === "prototype";
}
var Xp = Kp, Zp = Yi, Qp = xr, jp = L, e0 = ke, r0 = C;
function t0(e) {
  return function() {
    return Zp(e);
  };
}
function n0(e, r, t) {
  var n, a = {};
  return e && (r && Qp(r) ? r = t0(r) : jp(r) || (r = e0(r)), r0(e, function(i, o) {
    n = r ? r.call(t, i, o, e) : i, a[n] ? a[n].push(i) : a[n] = [i];
  })), a;
}
var ro = n0, a0 = ro, i0 = Je;
function o0(e, r, t) {
  var n = a0(e, r, t || this);
  return i0(n, function(a, i) {
    n[i] = a.length;
  }), n;
}
var s0 = o0;
function u0(e, r, t) {
  var n, a, i = [], o = arguments;
  if (o.length < 2 && (r = o[0], e = 0), n = e >> 0, a = r >> 0, n < r)
    for (t = t >> 0 || 1; n < a; n += t)
      i.push(n);
  return i;
}
var l0 = u0, Mn = te, c0 = Se, f0 = Ke, d0 = x, v0 = Y;
function h0(e, r) {
  if (e && r) {
    var t = v0.apply(this, [{}].concat(c0(arguments, 1))), n = Mn(t);
    d0(Mn(e), function(a) {
      f0(n, a) && (e[a] = t[a]);
    });
  }
  return e;
}
var p0 = h0, m0 = mi, g0 = m0(function(e, r) {
  return e > r;
}), $0 = g0;
function y0(e) {
  return (e.split(".")[1] || "").length;
}
var Ir = y0, _0 = Ze;
function b0(e, r) {
  if (e.repeat)
    return e.repeat(r);
  var t = isNaN(r) ? [] : new Array(_0(r));
  return t.join(e) + (t.length > 0 ? e : "");
}
var je = b0;
function S0(e, r) {
  return e.substring(0, r) + "." + e.substring(r, e.length);
}
var to = S0, cr = je, jr = to;
function w0(e) {
  var r = "" + e, t = r.match(/^([-+]?)((\d+)|((\d+)?[.](\d+)?))e([-+]{1})([0-9]+)$/);
  if (t) {
    var n = e < 0, a = n ? "-" : "", i = t[3] || "", o = t[5] || "", s = t[6] || "", c = t[7], u = t[8], l = u - s.length, f = u - i.length, v = u - o.length;
    return c === "+" ? i ? a + i + cr("0", u) : l > 0 ? a + o + s + cr("0", l) : a + o + jr(s, u) : i ? f > 0 ? a + "0." + cr("0", Math.abs(f)) + i : a + jr(i, f) : v > 0 ? a + "0." + cr("0", Math.abs(v)) + o + s : a + jr(o, v) + s;
  }
  return r;
}
var ge = w0, In = Ir, Ln = ge;
function E0(e, r) {
  var t = Ln(e), n = Ln(r);
  return parseInt(t.replace(".", "")) * parseInt(n.replace(".", "")) / Math.pow(10, In(t) + In(n));
}
var no = E0, O0 = no, Un = be, N0 = ge;
function D0(e) {
  return function(r, t) {
    var n = Un(r), a = n;
    if (n) {
      t = t >> 0;
      var i = N0(n), o = i.split("."), s = o[0], c = o[1] || "", u = c.substring(0, t + 1), l = s + (u ? "." + u : "");
      if (t >= c.length)
        return Un(l);
      if (l = n, t > 0) {
        var f = Math.pow(10, t);
        a = Math[e](O0(l, f)) / f;
      } else
        a = Math[e](l);
    }
    return a;
  };
}
var zt = D0, T0 = zt, C0 = T0("round"), qt = C0, R0 = zt, A0 = R0("ceil"), ao = A0, k0 = zt, P0 = k0("floor"), io = P0, x0 = pe, F0 = j, M0 = ge;
function I0(e) {
  return F0(e) ? M0(e) : "" + (x0(e) ? "" : e);
}
var F = I0, L0 = qt, U0 = F, B0 = je, W0 = to;
function z0(e, r) {
  r = r >> 0;
  var t = U0(L0(e, r)), n = t.split("."), a = n[0], i = n[1] || "", o = r - i.length;
  return r ? o > 0 ? a + "." + i + B0("0", o) : a + W0(i, Math.abs(o)) : a;
}
var Ht = z0, q0 = re, H0 = qt, G0 = ao, Y0 = io, V0 = j, J0 = F, K0 = Ht, X0 = ge, Z0 = Y;
function Q0(e, r) {
  var t = Z0({}, q0.commafyOptions, r), n = t.digits, a = V0(e), i, o, s, c, u;
  return a ? (i = (t.ceil ? G0 : t.floor ? Y0 : H0)(e, n), o = X0(n ? K0(i, n) : i).split("."), c = o[0], u = o[1], s = c && i < 0, s && (c = c.substring(1, c.length))) : (i = J0(e).replace(/,/g, ""), o = i ? [i] : [], c = o[0]), o.length ? (s ? "-" : "") + c.replace(new RegExp("(?=(?!(\\b))(.{" + (t.spaceNumber || 3) + "})+$)", "g"), t.separator || ",") + (u ? "." + u : "") : i;
}
var j0 = Q0, e1 = Ze, r1 = hi, t1 = r1(e1), n1 = t1, a1 = no, Bn = be;
function i1(e, r) {
  var t = Bn(e), n = Bn(r);
  return a1(t, n);
}
var Gt = i1, Wn = Ir, zn = ge, qn = Gt;
function o1(e, r) {
  var t = zn(e), n = zn(r), a = Math.pow(10, Math.max(Wn(t), Wn(n)));
  return (qn(e, a) + qn(r, a)) / a;
}
var oo = o1, s1 = oo, Hn = be;
function u1(e, r) {
  return s1(Hn(e), Hn(r));
}
var l1 = u1, Gn = Ir, Yn = ge, Vn = be, c1 = Ht;
function f1(e, r) {
  var t = Vn(e), n = Vn(r), a = Yn(t), i = Yn(n), o = Gn(a), s = Gn(i), c = Math.pow(10, Math.max(o, s)), u = o >= s ? o : s;
  return parseFloat(c1((t * c - n * c) / c, u));
}
var d1 = f1, Jn = Ir, Kn = ge, v1 = Gt;
function h1(e, r) {
  var t = Kn(e), n = Kn(r), a = Jn(t), i = Jn(n), o = i - a, s = o < 0, c = Math.pow(10, s ? Math.abs(o) : o);
  return v1(t.replace(".", "") / n.replace(".", ""), s ? 1 / c : c);
}
var so = h1, p1 = so, Xn = be;
function m1(e, r) {
  return p1(Xn(e), Xn(r));
}
var g1 = m1, et = oo, $1 = L, y1 = C, _1 = Xe;
function b1(e, r, t) {
  var n = 0;
  return y1(e, r ? $1(r) ? function() {
    n = et(n, r.apply(t, arguments));
  } : function(a) {
    n = et(n, _1(a, r));
  } : function(a) {
    n = et(n, a);
  }), n;
}
var uo = b1, S1 = so, w1 = zi, E1 = uo;
function O1(e, r, t) {
  return S1(E1(e, r, t), w1(e));
}
var N1 = O1, D1 = "first", er = D1, T1 = "last", Lr = T1;
function C1(e) {
  return e.getFullYear();
}
var rr = C1, R1 = 864e5, tr = R1;
function A1(e) {
  return e.getMonth();
}
var Ur = A1, k1 = me, P1 = W;
function x1(e) {
  return k1(e) && !isNaN(P1(e));
}
var z = x1, Zn = er, F1 = Lr, M1 = tr, I1 = rr, Qn = W, jn = Ur, L1 = U, U1 = z, B1 = j;
function lo(e, r, t) {
  var n = r && !isNaN(r) ? r : 0;
  if (e = L1(e), U1(e)) {
    if (t === Zn)
      return new Date(I1(e), jn(e) + n, 1);
    if (t === F1)
      return new Date(Qn(lo(e, n + 1, Zn)) - 1);
    if (B1(t) && e.setDate(t), n) {
      var a = e.getDate();
      if (e.setMonth(jn(e) + n), a !== e.getDate())
        return e.setDate(1), new Date(Qn(e) - M1);
    }
  }
  return e;
}
var nr = lo, W1 = er, ea = Lr, ra = rr, z1 = nr, q1 = U, H1 = z;
function G1(e, r, t) {
  var n;
  if (e = q1(e), H1(e) && (r && (n = r && !isNaN(r) ? r : 0, e.setFullYear(ra(e) + n)), t || !isNaN(t))) {
    if (t === W1)
      return new Date(ra(e), 0, 1);
    if (t === ea)
      return e.setMonth(11), z1(e, 0, ea);
    e.setMonth(t);
  }
  return e;
}
var Br = G1, Y1 = nr, V1 = U, J1 = z;
function K1(e) {
  var r = e.getMonth();
  return r < 3 ? 1 : r < 6 ? 2 : r < 9 ? 3 : 4;
}
function X1(e, r, t) {
  var n, a = r && !isNaN(r) ? r * 3 : 0;
  return e = V1(e), J1(e) ? (n = (K1(e) - 1) * 3, e.setMonth(n), Y1(e, a, t)) : e;
}
var Z1 = X1, ta = er, Q1 = Lr, j1 = Ze, em = rr, rm = Ur, tm = W, nm = U, am = z;
function co(e, r, t) {
  if (e = nm(e), am(e) && !isNaN(r)) {
    if (e.setDate(e.getDate() + j1(r)), t === ta)
      return new Date(em(e), rm(e), e.getDate());
    if (t === Q1)
      return new Date(tm(co(e, 1, ta)) - 1);
  }
  return e;
}
var fo = co;
function im(e) {
  return e.toUpperCase();
}
var vo = im, om = tr, sm = om * 7, ho = sm, um = re, rt = tr, lm = ho, cm = W, fm = U, dm = z, na = j;
function vm(e, r, t, n) {
  if (e = fm(e), dm(e)) {
    var a = na(t), i = na(n), o = cm(e);
    if (a || i) {
      var s = i ? n : um.firstDayOfWeek, c = e.getDay(), u = a ? t : c;
      if (c !== u) {
        var l = 0;
        s > c ? l = -(7 - s + c) : s < c && (l = s - c), u > s ? o += ((u === 0 ? 7 : u) - s + l) * rt : u < s ? o += (7 - s + u + l) * rt : o += l * rt;
      }
    }
    return r && !isNaN(r) && (o += r * lm), new Date(o);
  }
  return e;
}
var po = vm, hm = re, pm = ho, mm = j, gm = z, $m = po, aa = W;
function ym(e) {
  return function(r, t) {
    var n = mm(t) ? t : hm.firstDayOfWeek, a = $m(r, 0, n, n);
    if (gm(a)) {
      var i = new Date(a.getFullYear(), a.getMonth(), a.getDate()), o = e(a), s = o.getDay();
      return s > n && o.setDate(7 - s + n + 1), s < n && o.setDate(n - s + 1), Math.floor((aa(i) - aa(o)) / pm + 1);
    }
    return NaN;
  };
}
var mo = ym, _m = mo, bm = _m(function(e) {
  return new Date(e.getFullYear(), 0, 1);
}), go = bm, Sm = rr, wm = Ur;
function Em(e) {
  return new Date(Sm(e), wm(e), e.getDate());
}
var Om = Em, Nm = W, Dm = Om;
function Tm(e) {
  return Nm(Dm(e));
}
var Cm = Tm, Rm = tr, Am = er, ia = Cm, km = Br, Pm = U, xm = z;
function Fm(e) {
  return e = Pm(e), xm(e) ? Math.floor((ia(e) - ia(km(e, 0, Am))) / Rm) + 1 : NaN;
}
var $o = Fm, Mm = F, Im = Q, Lm = je;
function Um(e, r, t) {
  var n = Mm(e);
  return r = r >> 0, t = Im(t) ? " " : "" + t, n.padStart ? n.padStart(r, t) : r > n.length ? (r -= n.length, r > t.length && (t += Lm(t, r / t.length)), t.slice(0, r) + n) : n;
}
var yo = Um, Be = re, Bm = vo, Wm = rr, oa = Ur, zm = U, qm = go, Hm = $o, Gm = Y, Ym = z, Vm = L, K = yo;
function ie(e, r, t, n) {
  var a = r[t];
  return a ? Vm(a) ? a(n, t, e) : a[n] : n;
}
var Jm = /\[([^\]]+)]|y{2,4}|M{1,2}|d{1,2}|H{1,2}|h{1,2}|m{1,2}|s{1,2}|S{1,3}|Z{1,2}|W{1,2}|D{1,3}|[aAeEq]/g;
function Km(e, r, t) {
  if (e) {
    if (e = zm(e), Ym(e)) {
      var n = r || Be.parseDateFormat || Be.formatString, a = e.getHours(), i = a < 12 ? "am" : "pm", o = Gm({}, Be.parseDateRules || Be.formatStringMatchs, t ? t.formats : null), s = function(g, _) {
        return ("" + Wm(e)).substr(4 - _);
      }, c = function(g, _) {
        return K(oa(e) + 1, _, "0");
      }, u = function(g, _) {
        return K(e.getDate(), _, "0");
      }, l = function(g, _) {
        return K(a, _, "0");
      }, f = function(g, _) {
        return K(a <= 12 ? a : a - 12, _, "0");
      }, v = function(g, _) {
        return K(e.getMinutes(), _, "0");
      }, p = function(g, _) {
        return K(e.getSeconds(), _, "0");
      }, h = function(g, _) {
        return K(e.getMilliseconds(), _, "0");
      }, m = function(g, _) {
        var M = e.getTimezoneOffset() / 60 * -1;
        return ie(e, o, g, (M >= 0 ? "+" : "-") + K(M, 2, "0") + (_ === 1 ? ":" : "") + "00");
      }, $ = function(g, _) {
        return K(ie(e, o, g, qm(e, (t ? t.firstDay : null) || Be.firstDayOfWeek)), _, "0");
      }, y = function(g, _) {
        return K(ie(e, o, g, Hm(e)), _, "0");
      }, O = {
        yyyy: s,
        yy: s,
        MM: c,
        M: c,
        dd: u,
        d: u,
        HH: l,
        H: l,
        hh: f,
        h: f,
        mm: v,
        m: v,
        ss: p,
        s: p,
        SSS: h,
        S: h,
        ZZ: m,
        Z: m,
        WW: $,
        W: $,
        DDD: y,
        D: y,
        a: function(g) {
          return ie(e, o, g, i);
        },
        A: function(g) {
          return ie(e, o, g, Bm(i));
        },
        e: function(g) {
          return ie(e, o, g, e.getDay());
        },
        E: function(g) {
          return ie(e, o, g, e.getDay());
        },
        q: function(g) {
          return ie(e, o, g, Math.floor((oa(e) + 3) / 3));
        }
      };
      return n.replace(Jm, function(g, _) {
        return _ || (O[g] ? O[g](g, g.length) : g);
      });
    }
    return "Invalid Date";
  }
  return "";
}
var _o = Km, Xm = W, Zm = Mr, Qm = Date.now || function() {
  return Xm(Zm());
}, bo = Qm, jm = W, eg = bo, rg = U, tg = me, ng = function(e, r) {
  if (e) {
    var t = rg(e, r);
    return tg(t) ? jm(t) : t;
  }
  return eg();
}, ag = ng, sa = _o;
function ig(e, r, t) {
  return e && r ? (e = sa(e, t), e !== "Invalid Date" && e === sa(r, t)) : !1;
}
var og = ig, sg = mo, ug = sg(function(e) {
  return new Date(e.getFullYear(), e.getMonth(), 1);
}), lg = ug, cg = Br, fg = U, dg = z, vg = Ui;
function hg(e, r) {
  return e = fg(e), dg(e) ? vg(cg(e, r)) ? 366 : 365 : NaN;
}
var pg = hg, mg = tr, gg = er, $g = Lr, ua = W, la = nr, yg = U, _g = z;
function bg(e, r) {
  return e = yg(e), _g(e) ? Math.floor((ua(la(e, r, $g)) - ua(la(e, r, gg))) / mg) + 1 : NaN;
}
var Sg = bg, ca = W, wg = Mr, fa = U, da = z, va = [
  ["yyyy", 31536e6],
  ["MM", 2592e6],
  ["dd", 864e5],
  ["HH", 36e5],
  ["mm", 6e4],
  ["ss", 1e3],
  ["S", 0]
];
function Eg(e, r) {
  var t, n, a, i, o, s, c = { done: !1, time: 0 };
  if (e = fa(e), r = r ? fa(r) : wg(), da(e) && da(r) && (t = ca(e), n = ca(r), t < n))
    for (i = c.time = n - t, c.done = !0, s = 0, o = va.length; s < o; s++)
      a = va[s], i >= a[1] ? s === o - 1 ? c[a[0]] = i || 0 : (c[a[0]] = Math.floor(i / a[1]), i -= c[a[0]] * a[1]) : c[a[0]] = 0;
  return c;
}
var Og = Eg, Ng = F, Dg = Q, Tg = je;
function Cg(e, r, t) {
  var n = Ng(e);
  return r = r >> 0, t = Dg(t) ? " " : "" + t, n.padEnd ? n.padEnd(r, t) : r > n.length ? (r -= n.length, r > t.length && (t += Tg(t, r / t.length)), n + t.slice(0, r)) : n;
}
var Rg = Cg, Ag = F, kg = je;
function Pg(e, r) {
  return kg(Ag(e), r);
}
var xg = Pg, Fg = F;
function Mg(e) {
  return e && e.trimRight ? e.trimRight() : Fg(e).replace(/[\s\uFEFF\xA0]+$/g, "");
}
var So = Mg, Ig = F;
function Lg(e) {
  return e && e.trimLeft ? e.trimLeft() : Ig(e).replace(/^[\s\uFEFF\xA0]+/g, "");
}
var wo = Lg, Ug = So, Bg = wo;
function Wg(e) {
  return e && e.trim ? e.trim() : Ug(Bg(e));
}
var Eo = Wg, zg = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': "&quot;",
  "'": "&#x27;",
  "`": "&#x60;"
}, Oo = zg, qg = F, Hg = te;
function Gg(e) {
  var r = new RegExp("(?:" + Hg(e).join("|") + ")", "g");
  return function(t) {
    return qg(t).replace(r, function(n) {
      return e[n];
    });
  };
}
var No = Gg, Yg = Oo, Vg = No, Jg = Vg(Yg), Kg = Jg, ha = Oo, Xg = No, Zg = C, Do = {};
Zg(ha, function(e, r) {
  Do[ha[r]] = r;
});
var Qg = Xg(Do), jg = Qg;
function e$(e, r, t) {
  return e.substring(r, t);
}
var To = e$;
function r$(e) {
  return e.toLowerCase();
}
var Co = r$, t$ = F, oe = To, We = vo, n$ = Co, tt = {};
function a$(e) {
  if (e = t$(e), tt[e])
    return tt[e];
  var r = e.length, t = e.replace(/([-]+)/g, function(n, a, i) {
    return i && i + a.length < r ? "-" : "";
  });
  return r = t.length, t = t.replace(/([A-Z]+)/g, function(n, a, i) {
    var o = a.length;
    return a = n$(a), i ? o > 2 && i + o < r ? We(oe(a, 0, 1)) + oe(a, 1, o - 1) + We(oe(a, o - 1, o)) : We(oe(a, 0, 1)) + oe(a, 1, o) : o > 1 && i + o < r ? oe(a, 0, o - 1) + We(oe(a, o - 1, o)) : a;
  }).replace(/(-[a-zA-Z])/g, function(n, a) {
    return We(oe(a, 1, a.length));
  }), tt[e] = t, t;
}
var i$ = a$, o$ = F, Ee = To, se = Co, nt = {};
function s$(e) {
  if (e = o$(e), nt[e])
    return nt[e];
  if (/^[A-Z]+$/.test(e))
    return se(e);
  var r = e.replace(/^([a-z])([A-Z]+)([a-z]+)$/, function(t, n, a, i) {
    var o = a.length;
    return o > 1 ? n + "-" + se(Ee(a, 0, o - 1)) + "-" + se(Ee(a, o - 1, o)) + i : se(n + "-" + a + i);
  }).replace(/^([A-Z]+)([a-z]+)?$/, function(t, n, a) {
    var i = n.length;
    return se(Ee(n, 0, i - 1) + "-" + Ee(n, i - 1, i) + (a || ""));
  }).replace(/([a-z]?)([A-Z]+)([a-z]?)/g, function(t, n, a, i, o) {
    var s = a.length;
    return s > 1 && (n && (n += "-"), i) ? (n || "") + se(Ee(a, 0, s - 1)) + "-" + se(Ee(a, s - 1, s)) + i : (n || "") + (o ? "-" : "") + se(a) + (i || "");
  });
  return r = r.replace(/([-]+)/g, function(t, n, a) {
    return a && a + n.length < r.length ? "-" : "";
  }), nt[e] = r, r;
}
var u$ = s$, l$ = F;
function c$(e, r, t) {
  var n = l$(e);
  return (arguments.length === 1 ? n : n.substring(t)).indexOf(r) === 0;
}
var f$ = c$, d$ = F;
function v$(e, r, t) {
  var n = d$(e), a = arguments.length;
  return a > 1 && (a > 2 ? n.substring(0, t).indexOf(r) === t - 1 : n.indexOf(r) === n.length - 1);
}
var h$ = v$, p$ = re, m$ = F, g$ = Eo, $$ = Xe;
function y$(e, r, t) {
  return m$(e).replace((t || p$).tmplRE || /\{{2}([.\w[\]\s]+)\}{2}/g, function(n, a) {
    return $$(r, g$(a));
  });
}
var Ro = y$, _$ = Ro;
function b$(e, r) {
  return _$(e, r, { tmplRE: /\{([.\w[\]\s]+)\}/g });
}
var S$ = b$;
function w$() {
}
var E$ = w$, pa = Se;
function O$(e, r) {
  var t = pa(arguments, 2);
  return function() {
    return e.apply(r, pa(arguments).concat(t));
  };
}
var N$ = O$, ma = Se;
function D$(e, r) {
  var t = !1, n = null, a = ma(arguments, 2);
  return function() {
    return t || (n = e.apply(r, ma(arguments).concat(a)), t = !0), n;
  };
}
var T$ = D$, C$ = Se;
function R$(e, r, t) {
  var n = 0, a = [];
  return function() {
    var i = arguments;
    n++, n <= e && a.push(i[0]), n >= e && r.apply(t, [a].concat(C$(i)));
  };
}
var A$ = R$, k$ = Se;
function P$(e, r, t) {
  var n = 0, a = [];
  return t = t || this, function() {
    var i = arguments;
    n++, n < e && (a.push(i[0]), r.apply(t, [a].concat(k$(i))));
  };
}
var x$ = P$;
function F$(e, r, t) {
  var n = null, a = null, i = t || {}, o = !1, s = null, c = "leading" in i ? i.leading : !0, u = "trailing" in i ? i.trailing : !1, l = function() {
    n = null, a = null;
  }, f = function() {
    o = !0, e.apply(a, n), s = setTimeout(v, r), l();
  }, v = function() {
    s = null, !o && u === !0 && f();
  }, p = function() {
    var m = s !== null;
    return m && clearTimeout(s), l(), s = null, o = !1, m;
  }, h = function() {
    n = arguments, a = this, o = !1, s === null && (c === !0 ? f() : u === !0 && (s = setTimeout(v, r)));
  };
  return h.cancel = p, h;
}
var M$ = F$;
function I$(e, r, t) {
  var n = null, a = null, i = t || {}, o = !1, s = null, c = typeof t == "boolean", u = "leading" in i ? i.leading : c, l = "trailing" in i ? i.trailing : !c, f = function() {
    n = null, a = null;
  }, v = function() {
    o = !0, e.apply(a, n), f();
  }, p = function() {
    u === !0 && (s = null), !o && l === !0 && v();
  }, h = function() {
    var $ = s !== null;
    return $ && clearTimeout(s), f(), s = null, o = !1, $;
  }, m = function() {
    o = !1, n = arguments, a = this, s === null ? u === !0 && v() : clearTimeout(s), s = setTimeout(p, r);
  };
  return m.cancel = h, m;
}
var L$ = I$, U$ = Se;
function B$(e, r) {
  var t = U$(arguments, 2), n = this;
  return setTimeout(function() {
    e.apply(n, t);
  }, r);
}
var W$ = B$, z$ = decodeURIComponent, Ao = z$, ga = Ao, q$ = x, H$ = ae;
function G$(e) {
  var r, t = {};
  return e && H$(e) && q$(e.split("&"), function(n) {
    r = n.split("="), t[ga(r[0])] = ga(r[1] || "");
  }), t;
}
var ko = G$, Y$ = encodeURIComponent, Po = Y$, Er = Po, xo = C, Fo = w, Mo = ne, V$ = Q, Io = Pe;
function Lo(e, r, t) {
  var n, a = [];
  return xo(e, function(i, o) {
    n = Fo(i), Io(i) || n ? a = a.concat(Lo(i, r + "[" + o + "]", n)) : a.push(Er(r + "[" + (t ? "" : o) + "]") + "=" + Er(Mo(i) ? "" : i));
  }), a;
}
function J$(e) {
  var r, t = [];
  return xo(e, function(n, a) {
    V$(n) || (r = Fo(n), Io(n) || r ? t = t.concat(Lo(n, a, r)) : t.push(Er(a) + "=" + Er(Mo(n) ? "" : n)));
  }), t.join("&").replace(/%20/g, "+");
}
var K$ = J$, X$ = V, Z$ = typeof location === X$ ? 0 : location, Wr = Z$, fr = Wr;
function Q$() {
  return fr ? fr.origin || fr.protocol + "//" + fr.host : "";
}
var Uo = Q$, $a = Wr, j$ = ko, ey = Uo;
function ya(e) {
  return j$(e.split("?")[1] || "");
}
function ry(e) {
  var r, t, n, a, i = "" + e;
  return i.indexOf("//") === 0 ? i = ($a ? $a.protocol : "") + i : i.indexOf("/") === 0 && (i = ey() + i), n = i.replace(/#.*/, "").match(/(\?.*)/), a = {
    href: i,
    hash: "",
    host: "",
    hostname: "",
    protocol: "",
    port: "",
    search: n && n[1] && n[1].length > 1 ? n[1] : ""
  }, a.path = i.replace(/^([a-z0-9.+-]*:)\/\//, function(o, s) {
    return a.protocol = s, "";
  }).replace(/^([a-z0-9.+-]*)(:\d+)?\/?/, function(o, s, c) {
    return t = c || "", a.port = t.replace(":", ""), a.hostname = s, a.host = s + t, "/";
  }).replace(/(#.*)/, function(o, s) {
    return a.hash = s.length > 1 ? s : "", "";
  }), r = a.hash.match(/#((.*)\?|(.*))/), a.pathname = a.path.replace(/(\?|#.*).*/, ""), a.origin = a.protocol + "//" + a.host, a.hashKey = r && (r[2] || r[1]) || "", a.hashQuery = ya(a.hash), a.searchQuery = ya(a.search), a;
}
var Bo = ry, _a = Wr, ty = Uo, ny = Wi;
function ay() {
  if (_a) {
    var e = _a.pathname, r = ny(e, "/") + 1;
    return ty() + (r === e.length ? e : e.substring(0, r));
  }
  return "";
}
var iy = ay, ba = Wr, oy = Bo;
function sy() {
  return ba ? oy(ba.href) : {};
}
var uy = sy, Wo = re, at = Bt, Sa = Ao, wa = Po, ly = w, Ea = xr, zo = me, cy = Q, fy = Ke, dy = te, Or = Y, it = x, vy = Mr, dr = W, hy = Br, py = nr, my = fo;
function Oa(e, r) {
  var t = parseFloat(r), n = vy(), a = dr(n);
  switch (e) {
    case "y":
      return dr(hy(n, t));
    case "M":
      return dr(py(n, t));
    case "d":
      return dr(my(n, t));
    case "h":
    case "H":
      return a + t * 60 * 60 * 1e3;
    case "m":
      return a + t * 60 * 1e3;
    case "s":
      return a + t * 1e3;
  }
  return a;
}
function ot(e) {
  return (zo(e) ? e : new Date(e)).toUTCString();
}
function he(e, r, t) {
  if (at) {
    var n, a, i, o, s, c, u = [], l = arguments;
    return ly(e) ? u = e : l.length > 1 ? u = [Or({ name: e, value: r }, t)] : Ea(e) && (u = [e]), u.length > 0 ? (it(u, function(f) {
      n = Or({}, Wo.cookies, f), i = [], n.name && (a = n.expires, i.push(wa(n.name) + "=" + wa(Ea(n.value) ? JSON.stringify(n.value) : n.value)), a && (isNaN(a) ? a = a.replace(/^([0-9]+)(y|M|d|H|h|m|s)$/, function(v, p, h) {
        return ot(Oa(h, p));
      }) : /^[0-9]{11,13}$/.test(a) || zo(a) ? a = ot(a) : a = ot(Oa("d", a)), n.expires = a), it(["expires", "path", "domain", "secure"], function(v) {
        cy(n[v]) || i.push(n[v] && v === "secure" ? v : v + "=" + n[v]);
      })), at.cookie = i.join("; ");
    }), !0) : (o = {}, s = at.cookie, s && it(s.split("; "), function(f) {
      c = f.indexOf("="), o[Sa(f.substring(0, c))] = Sa(f.substring(c + 1) || "");
    }), l.length === 1 ? o[e] : o);
  }
  return !1;
}
function gy(e) {
  return fy(qo(), e);
}
function Na(e) {
  return he(e);
}
function Da(e, r, t) {
  return he(e, r, t), he;
}
function Ta(e, r) {
  he(e, "", Or({ expires: -1 }, Wo.cookies, r));
}
function qo() {
  return dy(he());
}
function $y() {
  return he();
}
Or(he, {
  has: gy,
  set: Da,
  setItem: Da,
  get: Na,
  getItem: Na,
  remove: Ta,
  removeItem: Ta,
  keys: qo,
  getJSON: $y
});
var yy = he, _y = V, st = Bt, ut = Ji, by = Y, Sy = x;
function Ca(e) {
  try {
    var r = "__xe_t";
    return e.setItem(r, 1), e.removeItem(r), !0;
  } catch {
    return !1;
  }
}
function vr(e) {
  return navigator.userAgent.indexOf(e) > -1;
}
function wy() {
  var e, r, t, n = !1, a = !1, i = !1, o = {
    isNode: !1,
    isMobile: n,
    isPC: !1,
    isDoc: !!st
  };
  if (!ut && typeof process !== _y)
    o.isNode = !0;
  else {
    t = vr("Edge"), r = vr("Chrome"), n = /(Android|webOS|iPhone|iPad|iPod|SymbianOS|BlackBerry|Windows Phone)/.test(navigator.userAgent), o.isDoc && (e = st.body || st.documentElement, Sy(["webkit", "khtml", "moz", "ms", "o"], function(s) {
      o["-" + s] = !!e[s + "MatchesSelector"];
    }));
    try {
      a = Ca(ut.localStorage);
    } catch {
    }
    try {
      i = Ca(ut.sessionStorage);
    } catch {
    }
    by(o, {
      edge: t,
      firefox: vr("Firefox"),
      msie: !t && o["-ms"],
      safari: !r && !t && vr("Safari"),
      isMobile: n,
      isPC: !n,
      isLocalStorage: a,
      isSessionStorage: i
    });
  }
  return o;
}
var Ey = wy, Ho = Zu, Ra = Y, Oy = Je, Ny = ai, Dy = ll, Ty = vl, Cy = xe, Ry = oi, Ay = si, ky = ui, Py = x, xy = Ft, Fy = li, My = Fl, Iy = Mt, Ly = dc, Uy = Lt, By = vi, Wy = bc, zy = Se, qy = Tc, Hy = Ac, Gy = Ke, Yy = xc, Vy = Lc, Jy = Wc, Ky = Hc, Xy = Vc, Zy = lf, Qy = $i, jy = vf, e2 = mf, r2 = pi, t2 = bf, n2 = rd, a2 = od, i2 = cd, o2 = Ti, s2 = gd, u2 = _d, l2 = Od, c2 = Ai, f2 = ki, d2 = G, v2 = w, h2 = ne, p2 = kd, m2 = Q, g2 = L, $2 = xr, y2 = ae, _2 = Pe, b2 = Ui, S2 = me, w2 = pe, E2 = C, O2 = av, N2 = uv, D2 = mv, T2 = Wi, C2 = te, R2 = Fe, A2 = xt, k2 = zi, P2 = Si, x2 = Ei, F2 = wi, M2 = Nv, I2 = xv, L2 = qi, U2 = Hi, B2 = j, W2 = Ut, z2 = Gi, q2 = zv, H2 = Yi, G2 = Vi, Y2 = Kv, V2 = jv, J2 = ah, K2 = lh, X2 = vh, Z2 = gh, Q2 = bh, j2 = Oh, e_ = Ch, r_ = Xh, t_ = ji, n_ = ep, a_ = up, i_ = fp, o_ = Wt, s_ = hp, u_ = $p, l_ = bp, c_ = Ep, f_ = kp, d_ = Fp, v_ = Lp, h_ = Wp, p_ = Gp, m_ = Xe, g_ = Xp, $_ = ro, y_ = s0, __ = l0, b_ = p0, S_ = di, w_ = gi, E_ = $0, O_ = j0, N_ = qt, D_ = ao, T_ = io, C_ = Ht, R_ = n1, A_ = be, k_ = ge, P_ = l1, x_ = d1, F_ = Gt, M_ = g1, I_ = uo, L_ = N1, U_ = Br, B_ = Z1, W_ = nr, z_ = fo, q_ = U, H_ = _o, G_ = bo, Y_ = ag, V_ = z, J_ = og, K_ = po, X_ = $o, Z_ = go, Q_ = lg, j_ = pg, eb = Sg, rb = Og, tb = Rg, nb = yo, ab = xg, ib = Eo, ob = So, sb = wo, ub = Kg, lb = jg, cb = i$, fb = u$, db = f$, vb = h$, hb = Ro, pb = S$, Aa = F, mb = E$, gb = ke, $b = N$, yb = T$, _b = A$, bb = x$, Sb = M$, wb = L$, Eb = W$, Ob = ko, Nb = K$, Db = Bo, Tb = iy, Cb = uy, Rb = yy, Ab = Ey;
Ra(Ho, {
  // object
  assign: Ra,
  objectEach: Oy,
  lastObjectEach: Ny,
  objectMap: Dy,
  merge: Ty,
  // array
  uniq: Fy,
  union: My,
  sortBy: Ly,
  orderBy: Uy,
  shuffle: By,
  sample: Wy,
  some: Ry,
  every: Ay,
  slice: zy,
  filter: qy,
  find: Yy,
  findLast: Vy,
  findKey: Hy,
  includes: Gy,
  arrayIndexOf: c2,
  arrayLastIndexOf: f2,
  map: Cy,
  reduce: Jy,
  copyWithin: Ky,
  chunk: Xy,
  zip: Zy,
  unzip: Qy,
  zipObject: jy,
  flatten: e2,
  toArray: Iy,
  includeArrays: ky,
  pluck: r2,
  invoke: t2,
  arrayEach: Py,
  lastArrayEach: xy,
  toArrayTree: n2,
  toTreeArray: a2,
  findTree: i2,
  eachTree: o2,
  mapTree: s2,
  filterTree: u2,
  searchTree: l2,
  // base
  hasOwnProp: d2,
  eqNull: w2,
  isNaN: p2,
  isFinite: M2,
  isUndefined: m2,
  isArray: v2,
  isFloat: I2,
  isInteger: L2,
  isFunction: g2,
  isBoolean: U2,
  isString: y2,
  isNumber: B2,
  isRegExp: W2,
  isObject: $2,
  isPlainObject: _2,
  isDate: S2,
  isError: z2,
  isTypeError: q2,
  isEmpty: H2,
  isNull: h2,
  isSymbol: G2,
  isArguments: Y2,
  isElement: V2,
  isDocument: J2,
  isWindow: K2,
  isFormData: X2,
  isMap: Z2,
  isWeakMap: Q2,
  isSet: j2,
  isWeakSet: e_,
  isLeapYear: b2,
  isMatch: r_,
  isEqual: t_,
  isEqualWith: n_,
  getType: a_,
  uniqueId: i_,
  getSize: k2,
  indexOf: D2,
  lastIndexOf: T2,
  findIndexOf: o_,
  findLastIndexOf: s_,
  toStringJSON: u_,
  toJSONString: l_,
  keys: C2,
  values: R2,
  entries: c_,
  pick: f_,
  omit: d_,
  first: v_,
  last: h_,
  each: E2,
  forOf: O2,
  lastForOf: N2,
  lastEach: P2,
  has: p_,
  get: m_,
  set: g_,
  groupBy: $_,
  countBy: y_,
  clone: A2,
  clear: F2,
  remove: x2,
  range: __,
  destructuring: b_,
  // number
  random: S_,
  min: E_,
  max: w_,
  commafy: O_,
  round: N_,
  ceil: D_,
  floor: T_,
  toFixed: C_,
  toNumber: A_,
  toNumberString: k_,
  toInteger: R_,
  add: P_,
  subtract: x_,
  multiply: F_,
  divide: M_,
  sum: I_,
  mean: L_,
  // date
  now: G_,
  timestamp: Y_,
  isValidDate: V_,
  isDateSame: J_,
  toStringDate: q_,
  toDateString: H_,
  getWhatYear: U_,
  getWhatQuarter: B_,
  getWhatMonth: W_,
  getWhatWeek: K_,
  getWhatDay: z_,
  getYearDay: X_,
  getYearWeek: Z_,
  getMonthWeek: Q_,
  getDayOfYear: j_,
  getDayOfMonth: eb,
  getDateDiff: rb,
  // string
  trim: ib,
  trimLeft: sb,
  trimRight: ob,
  escape: ub,
  unescape: lb,
  camelCase: cb,
  kebabCase: fb,
  repeat: ab,
  padStart: nb,
  padEnd: tb,
  startsWith: db,
  endsWith: vb,
  template: hb,
  toFormatString: pb,
  toString: Aa,
  toValueString: Aa,
  // function
  noop: mb,
  property: gb,
  bind: $b,
  once: yb,
  after: _b,
  before: bb,
  throttle: Sb,
  debounce: wb,
  delay: Eb,
  // url
  unserialize: Ob,
  serialize: Nb,
  parseUrl: Db,
  // web
  getBaseURL: Tb,
  locat: Cb,
  browse: Ab,
  cookie: Rb
});
var kb = Ho;
const Yt = /* @__PURE__ */ ri(kb);
function Go(e, r) {
  return function() {
    return e.apply(r, arguments);
  };
}
const { toString: Pb } = Object.prototype, { getPrototypeOf: Vt } = Object, zr = /* @__PURE__ */ ((e) => (r) => {
  const t = Pb.call(r);
  return e[t] || (e[t] = t.slice(8, -1).toLowerCase());
})(/* @__PURE__ */ Object.create(null)), J = (e) => (e = e.toLowerCase(), (r) => zr(r) === e), qr = (e) => (r) => typeof r === e, { isArray: Me } = Array, He = qr("undefined");
function xb(e) {
  return e !== null && !He(e) && e.constructor !== null && !He(e.constructor) && B(e.constructor.isBuffer) && e.constructor.isBuffer(e);
}
const Yo = J("ArrayBuffer");
function Fb(e) {
  let r;
  return typeof ArrayBuffer < "u" && ArrayBuffer.isView ? r = ArrayBuffer.isView(e) : r = e && e.buffer && Yo(e.buffer), r;
}
const Mb = qr("string"), B = qr("function"), Vo = qr("number"), Hr = (e) => e !== null && typeof e == "object", Ib = (e) => e === !0 || e === !1, hr = (e) => {
  if (zr(e) !== "object")
    return !1;
  const r = Vt(e);
  return (r === null || r === Object.prototype || Object.getPrototypeOf(r) === null) && !(Symbol.toStringTag in e) && !(Symbol.iterator in e);
}, Lb = J("Date"), Ub = J("File"), Bb = J("Blob"), Wb = J("FileList"), zb = (e) => Hr(e) && B(e.pipe), qb = (e) => {
  let r;
  return e && (typeof FormData == "function" && e instanceof FormData || B(e.append) && ((r = zr(e)) === "formdata" || // detect form-data instance
  r === "object" && B(e.toString) && e.toString() === "[object FormData]"));
}, Hb = J("URLSearchParams"), [Gb, Yb, Vb, Jb] = ["ReadableStream", "Request", "Response", "Headers"].map(J), Kb = (e) => e.trim ? e.trim() : e.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
function ar(e, r, { allOwnKeys: t = !1 } = {}) {
  if (e === null || typeof e > "u")
    return;
  let n, a;
  if (typeof e != "object" && (e = [e]), Me(e))
    for (n = 0, a = e.length; n < a; n++)
      r.call(null, e[n], n, e);
  else {
    const i = t ? Object.getOwnPropertyNames(e) : Object.keys(e), o = i.length;
    let s;
    for (n = 0; n < o; n++)
      s = i[n], r.call(null, e[s], s, e);
  }
}
function Jo(e, r) {
  r = r.toLowerCase();
  const t = Object.keys(e);
  let n = t.length, a;
  for (; n-- > 0; )
    if (a = t[n], r === a.toLowerCase())
      return a;
  return null;
}
const Ko = typeof globalThis < "u" ? globalThis : typeof self < "u" ? self : typeof window < "u" ? window : global, Xo = (e) => !He(e) && e !== Ko;
function bt() {
  const { caseless: e } = Xo(this) && this || {}, r = {}, t = (n, a) => {
    const i = e && Jo(r, a) || a;
    hr(r[i]) && hr(n) ? r[i] = bt(r[i], n) : hr(n) ? r[i] = bt({}, n) : Me(n) ? r[i] = n.slice() : r[i] = n;
  };
  for (let n = 0, a = arguments.length; n < a; n++)
    arguments[n] && ar(arguments[n], t);
  return r;
}
const Xb = (e, r, t, { allOwnKeys: n } = {}) => (ar(r, (a, i) => {
  t && B(a) ? e[i] = Go(a, t) : e[i] = a;
}, { allOwnKeys: n }), e), Zb = (e) => (e.charCodeAt(0) === 65279 && (e = e.slice(1)), e), Qb = (e, r, t, n) => {
  e.prototype = Object.create(r.prototype, n), e.prototype.constructor = e, Object.defineProperty(e, "super", {
    value: r.prototype
  }), t && Object.assign(e.prototype, t);
}, jb = (e, r, t, n) => {
  let a, i, o;
  const s = {};
  if (r = r || {}, e == null)
    return r;
  do {
    for (a = Object.getOwnPropertyNames(e), i = a.length; i-- > 0; )
      o = a[i], (!n || n(o, e, r)) && !s[o] && (r[o] = e[o], s[o] = !0);
    e = t !== !1 && Vt(e);
  } while (e && (!t || t(e, r)) && e !== Object.prototype);
  return r;
}, eS = (e, r, t) => {
  e = String(e), (t === void 0 || t > e.length) && (t = e.length), t -= r.length;
  const n = e.indexOf(r, t);
  return n !== -1 && n === t;
}, rS = (e) => {
  if (!e)
    return null;
  if (Me(e))
    return e;
  let r = e.length;
  if (!Vo(r))
    return null;
  const t = new Array(r);
  for (; r-- > 0; )
    t[r] = e[r];
  return t;
}, tS = /* @__PURE__ */ ((e) => (r) => e && r instanceof e)(typeof Uint8Array < "u" && Vt(Uint8Array)), nS = (e, r) => {
  const n = (e && e[Symbol.iterator]).call(e);
  let a;
  for (; (a = n.next()) && !a.done; ) {
    const i = a.value;
    r.call(e, i[0], i[1]);
  }
}, aS = (e, r) => {
  let t;
  const n = [];
  for (; (t = e.exec(r)) !== null; )
    n.push(t);
  return n;
}, iS = J("HTMLFormElement"), oS = (e) => e.toLowerCase().replace(
  /[-_\s]([a-z\d])(\w*)/g,
  function(t, n, a) {
    return n.toUpperCase() + a;
  }
), ka = (({ hasOwnProperty: e }) => (r, t) => e.call(r, t))(Object.prototype), sS = J("RegExp"), Zo = (e, r) => {
  const t = Object.getOwnPropertyDescriptors(e), n = {};
  ar(t, (a, i) => {
    let o;
    (o = r(a, i, e)) !== !1 && (n[i] = o || a);
  }), Object.defineProperties(e, n);
}, uS = (e) => {
  Zo(e, (r, t) => {
    if (B(e) && ["arguments", "caller", "callee"].indexOf(t) !== -1)
      return !1;
    const n = e[t];
    if (B(n)) {
      if (r.enumerable = !1, "writable" in r) {
        r.writable = !1;
        return;
      }
      r.set || (r.set = () => {
        throw Error("Can not rewrite read-only method '" + t + "'");
      });
    }
  });
}, lS = (e, r) => {
  const t = {}, n = (a) => {
    a.forEach((i) => {
      t[i] = !0;
    });
  };
  return Me(e) ? n(e) : n(String(e).split(r)), t;
}, cS = () => {
}, fS = (e, r) => e != null && Number.isFinite(e = +e) ? e : r, lt = "abcdefghijklmnopqrstuvwxyz", Pa = "0123456789", Qo = {
  DIGIT: Pa,
  ALPHA: lt,
  ALPHA_DIGIT: lt + lt.toUpperCase() + Pa
}, dS = (e = 16, r = Qo.ALPHA_DIGIT) => {
  let t = "";
  const { length: n } = r;
  for (; e--; )
    t += r[Math.random() * n | 0];
  return t;
};
function vS(e) {
  return !!(e && B(e.append) && e[Symbol.toStringTag] === "FormData" && e[Symbol.iterator]);
}
const hS = (e) => {
  const r = new Array(10), t = (n, a) => {
    if (Hr(n)) {
      if (r.indexOf(n) >= 0)
        return;
      if (!("toJSON" in n)) {
        r[a] = n;
        const i = Me(n) ? [] : {};
        return ar(n, (o, s) => {
          const c = t(o, a + 1);
          !He(c) && (i[s] = c);
        }), r[a] = void 0, i;
      }
    }
    return n;
  };
  return t(e, 0);
}, pS = J("AsyncFunction"), mS = (e) => e && (Hr(e) || B(e)) && B(e.then) && B(e.catch), d = {
  isArray: Me,
  isArrayBuffer: Yo,
  isBuffer: xb,
  isFormData: qb,
  isArrayBufferView: Fb,
  isString: Mb,
  isNumber: Vo,
  isBoolean: Ib,
  isObject: Hr,
  isPlainObject: hr,
  isReadableStream: Gb,
  isRequest: Yb,
  isResponse: Vb,
  isHeaders: Jb,
  isUndefined: He,
  isDate: Lb,
  isFile: Ub,
  isBlob: Bb,
  isRegExp: sS,
  isFunction: B,
  isStream: zb,
  isURLSearchParams: Hb,
  isTypedArray: tS,
  isFileList: Wb,
  forEach: ar,
  merge: bt,
  extend: Xb,
  trim: Kb,
  stripBOM: Zb,
  inherits: Qb,
  toFlatObject: jb,
  kindOf: zr,
  kindOfTest: J,
  endsWith: eS,
  toArray: rS,
  forEachEntry: nS,
  matchAll: aS,
  isHTMLForm: iS,
  hasOwnProperty: ka,
  hasOwnProp: ka,
  // an alias to avoid ESLint no-prototype-builtins detection
  reduceDescriptors: Zo,
  freezeMethods: uS,
  toObjectSet: lS,
  toCamelCase: oS,
  noop: cS,
  toFiniteNumber: fS,
  findKey: Jo,
  global: Ko,
  isContextDefined: Xo,
  ALPHABET: Qo,
  generateString: dS,
  isSpecCompliantForm: vS,
  toJSONObject: hS,
  isAsyncFn: pS,
  isThenable: mS
};
function b(e, r, t, n, a) {
  Error.call(this), Error.captureStackTrace ? Error.captureStackTrace(this, this.constructor) : this.stack = new Error().stack, this.message = e, this.name = "AxiosError", r && (this.code = r), t && (this.config = t), n && (this.request = n), a && (this.response = a);
}
d.inherits(b, Error, {
  toJSON: function() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: d.toJSONObject(this.config),
      code: this.code,
      status: this.response && this.response.status ? this.response.status : null
    };
  }
});
const jo = b.prototype, es = {};
[
  "ERR_BAD_OPTION_VALUE",
  "ERR_BAD_OPTION",
  "ECONNABORTED",
  "ETIMEDOUT",
  "ERR_NETWORK",
  "ERR_FR_TOO_MANY_REDIRECTS",
  "ERR_DEPRECATED",
  "ERR_BAD_RESPONSE",
  "ERR_BAD_REQUEST",
  "ERR_CANCELED",
  "ERR_NOT_SUPPORT",
  "ERR_INVALID_URL"
  // eslint-disable-next-line func-names
].forEach((e) => {
  es[e] = { value: e };
});
Object.defineProperties(b, es);
Object.defineProperty(jo, "isAxiosError", { value: !0 });
b.from = (e, r, t, n, a, i) => {
  const o = Object.create(jo);
  return d.toFlatObject(e, o, function(c) {
    return c !== Error.prototype;
  }, (s) => s !== "isAxiosError"), b.call(o, e.message, r, t, n, a), o.cause = e, o.name = e.name, i && Object.assign(o, i), o;
};
const gS = null;
function St(e) {
  return d.isPlainObject(e) || d.isArray(e);
}
function rs(e) {
  return d.endsWith(e, "[]") ? e.slice(0, -2) : e;
}
function xa(e, r, t) {
  return e ? e.concat(r).map(function(a, i) {
    return a = rs(a), !t && i ? "[" + a + "]" : a;
  }).join(t ? "." : "") : r;
}
function $S(e) {
  return d.isArray(e) && !e.some(St);
}
const yS = d.toFlatObject(d, {}, null, function(r) {
  return /^is[A-Z]/.test(r);
});
function Gr(e, r, t) {
  if (!d.isObject(e))
    throw new TypeError("target must be an object");
  r = r || new FormData(), t = d.toFlatObject(t, {
    metaTokens: !0,
    dots: !1,
    indexes: !1
  }, !1, function(m, $) {
    return !d.isUndefined($[m]);
  });
  const n = t.metaTokens, a = t.visitor || l, i = t.dots, o = t.indexes, c = (t.Blob || typeof Blob < "u" && Blob) && d.isSpecCompliantForm(r);
  if (!d.isFunction(a))
    throw new TypeError("visitor must be a function");
  function u(h) {
    if (h === null)
      return "";
    if (d.isDate(h))
      return h.toISOString();
    if (!c && d.isBlob(h))
      throw new b("Blob is not supported. Use a Buffer instead.");
    return d.isArrayBuffer(h) || d.isTypedArray(h) ? c && typeof Blob == "function" ? new Blob([h]) : Buffer.from(h) : h;
  }
  function l(h, m, $) {
    let y = h;
    if (h && !$ && typeof h == "object") {
      if (d.endsWith(m, "{}"))
        m = n ? m : m.slice(0, -2), h = JSON.stringify(h);
      else if (d.isArray(h) && $S(h) || (d.isFileList(h) || d.endsWith(m, "[]")) && (y = d.toArray(h)))
        return m = rs(m), y.forEach(function(g, _) {
          !(d.isUndefined(g) || g === null) && r.append(
            // eslint-disable-next-line no-nested-ternary
            o === !0 ? xa([m], _, i) : o === null ? m : m + "[]",
            u(g)
          );
        }), !1;
    }
    return St(h) ? !0 : (r.append(xa($, m, i), u(h)), !1);
  }
  const f = [], v = Object.assign(yS, {
    defaultVisitor: l,
    convertValue: u,
    isVisitable: St
  });
  function p(h, m) {
    if (!d.isUndefined(h)) {
      if (f.indexOf(h) !== -1)
        throw Error("Circular reference detected in " + m.join("."));
      f.push(h), d.forEach(h, function(y, O) {
        (!(d.isUndefined(y) || y === null) && a.call(
          r,
          y,
          d.isString(O) ? O.trim() : O,
          m,
          v
        )) === !0 && p(y, m ? m.concat(O) : [O]);
      }), f.pop();
    }
  }
  if (!d.isObject(e))
    throw new TypeError("data must be an object");
  return p(e), r;
}
function Fa(e) {
  const r = {
    "!": "%21",
    "'": "%27",
    "(": "%28",
    ")": "%29",
    "~": "%7E",
    "%20": "+",
    "%00": "\0"
  };
  return encodeURIComponent(e).replace(/[!'()~]|%20|%00/g, function(n) {
    return r[n];
  });
}
function Jt(e, r) {
  this._pairs = [], e && Gr(e, this, r);
}
const ts = Jt.prototype;
ts.append = function(r, t) {
  this._pairs.push([r, t]);
};
ts.toString = function(r) {
  const t = r ? function(n) {
    return r.call(this, n, Fa);
  } : Fa;
  return this._pairs.map(function(a) {
    return t(a[0]) + "=" + t(a[1]);
  }, "").join("&");
};
function _S(e) {
  return encodeURIComponent(e).replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]");
}
function ns(e, r, t) {
  if (!r)
    return e;
  const n = t && t.encode || _S, a = t && t.serialize;
  let i;
  if (a ? i = a(r, t) : i = d.isURLSearchParams(r) ? r.toString() : new Jt(r, t).toString(n), i) {
    const o = e.indexOf("#");
    o !== -1 && (e = e.slice(0, o)), e += (e.indexOf("?") === -1 ? "?" : "&") + i;
  }
  return e;
}
class Ma {
  constructor() {
    this.handlers = [];
  }
  /**
   * Add a new interceptor to the stack
   *
   * @param {Function} fulfilled The function to handle `then` for a `Promise`
   * @param {Function} rejected The function to handle `reject` for a `Promise`
   *
   * @return {Number} An ID used to remove interceptor later
   */
  use(r, t, n) {
    return this.handlers.push({
      fulfilled: r,
      rejected: t,
      synchronous: n ? n.synchronous : !1,
      runWhen: n ? n.runWhen : null
    }), this.handlers.length - 1;
  }
  /**
   * Remove an interceptor from the stack
   *
   * @param {Number} id The ID that was returned by `use`
   *
   * @returns {Boolean} `true` if the interceptor was removed, `false` otherwise
   */
  eject(r) {
    this.handlers[r] && (this.handlers[r] = null);
  }
  /**
   * Clear all interceptors from the stack
   *
   * @returns {void}
   */
  clear() {
    this.handlers && (this.handlers = []);
  }
  /**
   * Iterate over all the registered interceptors
   *
   * This method is particularly useful for skipping over any
   * interceptors that may have become `null` calling `eject`.
   *
   * @param {Function} fn The function to call for each interceptor
   *
   * @returns {void}
   */
  forEach(r) {
    d.forEach(this.handlers, function(n) {
      n !== null && r(n);
    });
  }
}
const as = {
  silentJSONParsing: !0,
  forcedJSONParsing: !0,
  clarifyTimeoutError: !1
}, bS = typeof URLSearchParams < "u" ? URLSearchParams : Jt, SS = typeof FormData < "u" ? FormData : null, wS = typeof Blob < "u" ? Blob : null, ES = {
  isBrowser: !0,
  classes: {
    URLSearchParams: bS,
    FormData: SS,
    Blob: wS
  },
  protocols: ["http", "https", "file", "blob", "url", "data"]
}, Kt = typeof window < "u" && typeof document < "u", OS = ((e) => Kt && ["ReactNative", "NativeScript", "NS"].indexOf(e) < 0)(typeof navigator < "u" && navigator.product), NS = typeof WorkerGlobalScope < "u" && // eslint-disable-next-line no-undef
self instanceof WorkerGlobalScope && typeof self.importScripts == "function", DS = Kt && window.location.href || "http://localhost", TS = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  hasBrowserEnv: Kt,
  hasStandardBrowserEnv: OS,
  hasStandardBrowserWebWorkerEnv: NS,
  origin: DS
}, Symbol.toStringTag, { value: "Module" })), q = {
  ...TS,
  ...ES
};
function CS(e, r) {
  return Gr(e, new q.classes.URLSearchParams(), Object.assign({
    visitor: function(t, n, a, i) {
      return q.isNode && d.isBuffer(t) ? (this.append(n, t.toString("base64")), !1) : i.defaultVisitor.apply(this, arguments);
    }
  }, r));
}
function RS(e) {
  return d.matchAll(/\w+|\[(\w*)]/g, e).map((r) => r[0] === "[]" ? "" : r[1] || r[0]);
}
function AS(e) {
  const r = {}, t = Object.keys(e);
  let n;
  const a = t.length;
  let i;
  for (n = 0; n < a; n++)
    i = t[n], r[i] = e[i];
  return r;
}
function is(e) {
  function r(t, n, a, i) {
    let o = t[i++];
    if (o === "__proto__")
      return !0;
    const s = Number.isFinite(+o), c = i >= t.length;
    return o = !o && d.isArray(a) ? a.length : o, c ? (d.hasOwnProp(a, o) ? a[o] = [a[o], n] : a[o] = n, !s) : ((!a[o] || !d.isObject(a[o])) && (a[o] = []), r(t, n, a[o], i) && d.isArray(a[o]) && (a[o] = AS(a[o])), !s);
  }
  if (d.isFormData(e) && d.isFunction(e.entries)) {
    const t = {};
    return d.forEachEntry(e, (n, a) => {
      r(RS(n), a, t, 0);
    }), t;
  }
  return null;
}
function kS(e, r, t) {
  if (d.isString(e))
    try {
      return (r || JSON.parse)(e), d.trim(e);
    } catch (n) {
      if (n.name !== "SyntaxError")
        throw n;
    }
  return (t || JSON.stringify)(e);
}
const Xt = {
  transitional: as,
  adapter: ["xhr", "http", "fetch"],
  transformRequest: [function(r, t) {
    const n = t.getContentType() || "", a = n.indexOf("application/json") > -1, i = d.isObject(r);
    if (i && d.isHTMLForm(r) && (r = new FormData(r)), d.isFormData(r))
      return a ? JSON.stringify(is(r)) : r;
    if (d.isArrayBuffer(r) || d.isBuffer(r) || d.isStream(r) || d.isFile(r) || d.isBlob(r) || d.isReadableStream(r))
      return r;
    if (d.isArrayBufferView(r))
      return r.buffer;
    if (d.isURLSearchParams(r))
      return t.setContentType("application/x-www-form-urlencoded;charset=utf-8", !1), r.toString();
    let s;
    if (i) {
      if (n.indexOf("application/x-www-form-urlencoded") > -1)
        return CS(r, this.formSerializer).toString();
      if ((s = d.isFileList(r)) || n.indexOf("multipart/form-data") > -1) {
        const c = this.env && this.env.FormData;
        return Gr(
          s ? { "files[]": r } : r,
          c && new c(),
          this.formSerializer
        );
      }
    }
    return i || a ? (t.setContentType("application/json", !1), kS(r)) : r;
  }],
  transformResponse: [function(r) {
    const t = this.transitional || Xt.transitional, n = t && t.forcedJSONParsing, a = this.responseType === "json";
    if (d.isResponse(r) || d.isReadableStream(r))
      return r;
    if (r && d.isString(r) && (n && !this.responseType || a)) {
      const o = !(t && t.silentJSONParsing) && a;
      try {
        return JSON.parse(r);
      } catch (s) {
        if (o)
          throw s.name === "SyntaxError" ? b.from(s, b.ERR_BAD_RESPONSE, this, null, this.response) : s;
      }
    }
    return r;
  }],
  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,
  xsrfCookieName: "XSRF-TOKEN",
  xsrfHeaderName: "X-XSRF-TOKEN",
  maxContentLength: -1,
  maxBodyLength: -1,
  env: {
    FormData: q.classes.FormData,
    Blob: q.classes.Blob
  },
  validateStatus: function(r) {
    return r >= 200 && r < 300;
  },
  headers: {
    common: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": void 0
    }
  }
};
d.forEach(["delete", "get", "head", "post", "put", "patch"], (e) => {
  Xt.headers[e] = {};
});
const Zt = Xt, PS = d.toObjectSet([
  "age",
  "authorization",
  "content-length",
  "content-type",
  "etag",
  "expires",
  "from",
  "host",
  "if-modified-since",
  "if-unmodified-since",
  "last-modified",
  "location",
  "max-forwards",
  "proxy-authorization",
  "referer",
  "retry-after",
  "user-agent"
]), xS = (e) => {
  const r = {};
  let t, n, a;
  return e && e.split(`
`).forEach(function(o) {
    a = o.indexOf(":"), t = o.substring(0, a).trim().toLowerCase(), n = o.substring(a + 1).trim(), !(!t || r[t] && PS[t]) && (t === "set-cookie" ? r[t] ? r[t].push(n) : r[t] = [n] : r[t] = r[t] ? r[t] + ", " + n : n);
  }), r;
}, Ia = Symbol("internals");
function ze(e) {
  return e && String(e).trim().toLowerCase();
}
function pr(e) {
  return e === !1 || e == null ? e : d.isArray(e) ? e.map(pr) : String(e);
}
function FS(e) {
  const r = /* @__PURE__ */ Object.create(null), t = /([^\s,;=]+)\s*(?:=\s*([^,;]+))?/g;
  let n;
  for (; n = t.exec(e); )
    r[n[1]] = n[2];
  return r;
}
const MS = (e) => /^[-_a-zA-Z0-9^`|~,!#$%&'*+.]+$/.test(e.trim());
function ct(e, r, t, n, a) {
  if (d.isFunction(n))
    return n.call(this, r, t);
  if (a && (r = t), !!d.isString(r)) {
    if (d.isString(n))
      return r.indexOf(n) !== -1;
    if (d.isRegExp(n))
      return n.test(r);
  }
}
function IS(e) {
  return e.trim().toLowerCase().replace(/([a-z\d])(\w*)/g, (r, t, n) => t.toUpperCase() + n);
}
function LS(e, r) {
  const t = d.toCamelCase(" " + r);
  ["get", "set", "has"].forEach((n) => {
    Object.defineProperty(e, n + t, {
      value: function(a, i, o) {
        return this[n].call(this, r, a, i, o);
      },
      configurable: !0
    });
  });
}
class Yr {
  constructor(r) {
    r && this.set(r);
  }
  set(r, t, n) {
    const a = this;
    function i(s, c, u) {
      const l = ze(c);
      if (!l)
        throw new Error("header name must be a non-empty string");
      const f = d.findKey(a, l);
      (!f || a[f] === void 0 || u === !0 || u === void 0 && a[f] !== !1) && (a[f || c] = pr(s));
    }
    const o = (s, c) => d.forEach(s, (u, l) => i(u, l, c));
    if (d.isPlainObject(r) || r instanceof this.constructor)
      o(r, t);
    else if (d.isString(r) && (r = r.trim()) && !MS(r))
      o(xS(r), t);
    else if (d.isHeaders(r))
      for (const [s, c] of r.entries())
        i(c, s, n);
    else
      r != null && i(t, r, n);
    return this;
  }
  get(r, t) {
    if (r = ze(r), r) {
      const n = d.findKey(this, r);
      if (n) {
        const a = this[n];
        if (!t)
          return a;
        if (t === !0)
          return FS(a);
        if (d.isFunction(t))
          return t.call(this, a, n);
        if (d.isRegExp(t))
          return t.exec(a);
        throw new TypeError("parser must be boolean|regexp|function");
      }
    }
  }
  has(r, t) {
    if (r = ze(r), r) {
      const n = d.findKey(this, r);
      return !!(n && this[n] !== void 0 && (!t || ct(this, this[n], n, t)));
    }
    return !1;
  }
  delete(r, t) {
    const n = this;
    let a = !1;
    function i(o) {
      if (o = ze(o), o) {
        const s = d.findKey(n, o);
        s && (!t || ct(n, n[s], s, t)) && (delete n[s], a = !0);
      }
    }
    return d.isArray(r) ? r.forEach(i) : i(r), a;
  }
  clear(r) {
    const t = Object.keys(this);
    let n = t.length, a = !1;
    for (; n--; ) {
      const i = t[n];
      (!r || ct(this, this[i], i, r, !0)) && (delete this[i], a = !0);
    }
    return a;
  }
  normalize(r) {
    const t = this, n = {};
    return d.forEach(this, (a, i) => {
      const o = d.findKey(n, i);
      if (o) {
        t[o] = pr(a), delete t[i];
        return;
      }
      const s = r ? IS(i) : String(i).trim();
      s !== i && delete t[i], t[s] = pr(a), n[s] = !0;
    }), this;
  }
  concat(...r) {
    return this.constructor.concat(this, ...r);
  }
  toJSON(r) {
    const t = /* @__PURE__ */ Object.create(null);
    return d.forEach(this, (n, a) => {
      n != null && n !== !1 && (t[a] = r && d.isArray(n) ? n.join(", ") : n);
    }), t;
  }
  [Symbol.iterator]() {
    return Object.entries(this.toJSON())[Symbol.iterator]();
  }
  toString() {
    return Object.entries(this.toJSON()).map(([r, t]) => r + ": " + t).join(`
`);
  }
  get [Symbol.toStringTag]() {
    return "AxiosHeaders";
  }
  static from(r) {
    return r instanceof this ? r : new this(r);
  }
  static concat(r, ...t) {
    const n = new this(r);
    return t.forEach((a) => n.set(a)), n;
  }
  static accessor(r) {
    const n = (this[Ia] = this[Ia] = {
      accessors: {}
    }).accessors, a = this.prototype;
    function i(o) {
      const s = ze(o);
      n[s] || (LS(a, o), n[s] = !0);
    }
    return d.isArray(r) ? r.forEach(i) : i(r), this;
  }
}
Yr.accessor(["Content-Type", "Content-Length", "Accept", "Accept-Encoding", "User-Agent", "Authorization"]);
d.reduceDescriptors(Yr.prototype, ({ value: e }, r) => {
  let t = r[0].toUpperCase() + r.slice(1);
  return {
    get: () => e,
    set(n) {
      this[t] = n;
    }
  };
});
d.freezeMethods(Yr);
const H = Yr;
function ft(e, r) {
  const t = this || Zt, n = r || t, a = H.from(n.headers);
  let i = n.data;
  return d.forEach(e, function(s) {
    i = s.call(t, i, a.normalize(), r ? r.status : void 0);
  }), a.normalize(), i;
}
function os(e) {
  return !!(e && e.__CANCEL__);
}
function Ie(e, r, t) {
  b.call(this, e ?? "canceled", b.ERR_CANCELED, r, t), this.name = "CanceledError";
}
d.inherits(Ie, b, {
  __CANCEL__: !0
});
function ss(e, r, t) {
  const n = t.config.validateStatus;
  !t.status || !n || n(t.status) ? e(t) : r(new b(
    "Request failed with status code " + t.status,
    [b.ERR_BAD_REQUEST, b.ERR_BAD_RESPONSE][Math.floor(t.status / 100) - 4],
    t.config,
    t.request,
    t
  ));
}
function US(e) {
  const r = /^([-+\w]{1,25})(:?\/\/|:)/.exec(e);
  return r && r[1] || "";
}
function BS(e, r) {
  e = e || 10;
  const t = new Array(e), n = new Array(e);
  let a = 0, i = 0, o;
  return r = r !== void 0 ? r : 1e3, function(c) {
    const u = Date.now(), l = n[i];
    o || (o = u), t[a] = c, n[a] = u;
    let f = i, v = 0;
    for (; f !== a; )
      v += t[f++], f = f % e;
    if (a = (a + 1) % e, a === i && (i = (i + 1) % e), u - o < r)
      return;
    const p = l && u - l;
    return p ? Math.round(v * 1e3 / p) : void 0;
  };
}
function WS(e, r) {
  let t = 0;
  const n = 1e3 / r;
  let a = null;
  return function() {
    const o = this === !0, s = Date.now();
    if (o || s - t > n)
      return a && (clearTimeout(a), a = null), t = s, e.apply(null, arguments);
    a || (a = setTimeout(() => (a = null, t = Date.now(), e.apply(null, arguments)), n - (s - t)));
  };
}
const Nr = (e, r, t = 3) => {
  let n = 0;
  const a = BS(50, 250);
  return WS((i) => {
    const o = i.loaded, s = i.lengthComputable ? i.total : void 0, c = o - n, u = a(c), l = o <= s;
    n = o;
    const f = {
      loaded: o,
      total: s,
      progress: s ? o / s : void 0,
      bytes: c,
      rate: u || void 0,
      estimated: u && s && l ? (s - o) / u : void 0,
      event: i,
      lengthComputable: s != null
    };
    f[r ? "download" : "upload"] = !0, e(f);
  }, t);
}, zS = q.hasStandardBrowserEnv ? (
  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  function() {
    const r = /(msie|trident)/i.test(navigator.userAgent), t = document.createElement("a");
    let n;
    function a(i) {
      let o = i;
      return r && (t.setAttribute("href", o), o = t.href), t.setAttribute("href", o), {
        href: t.href,
        protocol: t.protocol ? t.protocol.replace(/:$/, "") : "",
        host: t.host,
        search: t.search ? t.search.replace(/^\?/, "") : "",
        hash: t.hash ? t.hash.replace(/^#/, "") : "",
        hostname: t.hostname,
        port: t.port,
        pathname: t.pathname.charAt(0) === "/" ? t.pathname : "/" + t.pathname
      };
    }
    return n = a(window.location.href), function(o) {
      const s = d.isString(o) ? a(o) : o;
      return s.protocol === n.protocol && s.host === n.host;
    };
  }()
) : (
  // Non standard browser envs (web workers, react-native) lack needed support.
  /* @__PURE__ */ function() {
    return function() {
      return !0;
    };
  }()
), qS = q.hasStandardBrowserEnv ? (
  // Standard browser envs support document.cookie
  {
    write(e, r, t, n, a, i) {
      const o = [e + "=" + encodeURIComponent(r)];
      d.isNumber(t) && o.push("expires=" + new Date(t).toGMTString()), d.isString(n) && o.push("path=" + n), d.isString(a) && o.push("domain=" + a), i === !0 && o.push("secure"), document.cookie = o.join("; ");
    },
    read(e) {
      const r = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
      return r ? decodeURIComponent(r[3]) : null;
    },
    remove(e) {
      this.write(e, "", Date.now() - 864e5);
    }
  }
) : (
  // Non-standard browser env (web workers, react-native) lack needed support.
  {
    write() {
    },
    read() {
      return null;
    },
    remove() {
    }
  }
);
function HS(e) {
  return /^([a-z][a-z\d+\-.]*:)?\/\//i.test(e);
}
function GS(e, r) {
  return r ? e.replace(/\/?\/$/, "") + "/" + r.replace(/^\/+/, "") : e;
}
function us(e, r) {
  return e && !HS(r) ? GS(e, r) : r;
}
const La = (e) => e instanceof H ? { ...e } : e;
function _e(e, r) {
  r = r || {};
  const t = {};
  function n(u, l, f) {
    return d.isPlainObject(u) && d.isPlainObject(l) ? d.merge.call({ caseless: f }, u, l) : d.isPlainObject(l) ? d.merge({}, l) : d.isArray(l) ? l.slice() : l;
  }
  function a(u, l, f) {
    if (d.isUndefined(l)) {
      if (!d.isUndefined(u))
        return n(void 0, u, f);
    } else
      return n(u, l, f);
  }
  function i(u, l) {
    if (!d.isUndefined(l))
      return n(void 0, l);
  }
  function o(u, l) {
    if (d.isUndefined(l)) {
      if (!d.isUndefined(u))
        return n(void 0, u);
    } else
      return n(void 0, l);
  }
  function s(u, l, f) {
    if (f in r)
      return n(u, l);
    if (f in e)
      return n(void 0, u);
  }
  const c = {
    url: i,
    method: i,
    data: i,
    baseURL: o,
    transformRequest: o,
    transformResponse: o,
    paramsSerializer: o,
    timeout: o,
    timeoutMessage: o,
    withCredentials: o,
    withXSRFToken: o,
    adapter: o,
    responseType: o,
    xsrfCookieName: o,
    xsrfHeaderName: o,
    onUploadProgress: o,
    onDownloadProgress: o,
    decompress: o,
    maxContentLength: o,
    maxBodyLength: o,
    beforeRedirect: o,
    transport: o,
    httpAgent: o,
    httpsAgent: o,
    cancelToken: o,
    socketPath: o,
    responseEncoding: o,
    validateStatus: s,
    headers: (u, l) => a(La(u), La(l), !0)
  };
  return d.forEach(Object.keys(Object.assign({}, e, r)), function(l) {
    const f = c[l] || a, v = f(e[l], r[l], l);
    d.isUndefined(v) && f !== s || (t[l] = v);
  }), t;
}
const ls = (e) => {
  const r = _e({}, e);
  let { data: t, withXSRFToken: n, xsrfHeaderName: a, xsrfCookieName: i, headers: o, auth: s } = r;
  r.headers = o = H.from(o), r.url = ns(us(r.baseURL, r.url), e.params, e.paramsSerializer), s && o.set(
    "Authorization",
    "Basic " + btoa((s.username || "") + ":" + (s.password ? unescape(encodeURIComponent(s.password)) : ""))
  );
  let c;
  if (d.isFormData(t)) {
    if (q.hasStandardBrowserEnv || q.hasStandardBrowserWebWorkerEnv)
      o.setContentType(void 0);
    else if ((c = o.getContentType()) !== !1) {
      const [u, ...l] = c ? c.split(";").map((f) => f.trim()).filter(Boolean) : [];
      o.setContentType([u || "multipart/form-data", ...l].join("; "));
    }
  }
  if (q.hasStandardBrowserEnv && (n && d.isFunction(n) && (n = n(r)), n || n !== !1 && zS(r.url))) {
    const u = a && i && qS.read(i);
    u && o.set(a, u);
  }
  return r;
}, YS = typeof XMLHttpRequest < "u", VS = YS && function(e) {
  return new Promise(function(t, n) {
    const a = ls(e);
    let i = a.data;
    const o = H.from(a.headers).normalize();
    let { responseType: s } = a, c;
    function u() {
      a.cancelToken && a.cancelToken.unsubscribe(c), a.signal && a.signal.removeEventListener("abort", c);
    }
    let l = new XMLHttpRequest();
    l.open(a.method.toUpperCase(), a.url, !0), l.timeout = a.timeout;
    function f() {
      if (!l)
        return;
      const p = H.from(
        "getAllResponseHeaders" in l && l.getAllResponseHeaders()
      ), m = {
        data: !s || s === "text" || s === "json" ? l.responseText : l.response,
        status: l.status,
        statusText: l.statusText,
        headers: p,
        config: e,
        request: l
      };
      ss(function(y) {
        t(y), u();
      }, function(y) {
        n(y), u();
      }, m), l = null;
    }
    "onloadend" in l ? l.onloadend = f : l.onreadystatechange = function() {
      !l || l.readyState !== 4 || l.status === 0 && !(l.responseURL && l.responseURL.indexOf("file:") === 0) || setTimeout(f);
    }, l.onabort = function() {
      l && (n(new b("Request aborted", b.ECONNABORTED, a, l)), l = null);
    }, l.onerror = function() {
      n(new b("Network Error", b.ERR_NETWORK, a, l)), l = null;
    }, l.ontimeout = function() {
      let h = a.timeout ? "timeout of " + a.timeout + "ms exceeded" : "timeout exceeded";
      const m = a.transitional || as;
      a.timeoutErrorMessage && (h = a.timeoutErrorMessage), n(new b(
        h,
        m.clarifyTimeoutError ? b.ETIMEDOUT : b.ECONNABORTED,
        a,
        l
      )), l = null;
    }, i === void 0 && o.setContentType(null), "setRequestHeader" in l && d.forEach(o.toJSON(), function(h, m) {
      l.setRequestHeader(m, h);
    }), d.isUndefined(a.withCredentials) || (l.withCredentials = !!a.withCredentials), s && s !== "json" && (l.responseType = a.responseType), typeof a.onDownloadProgress == "function" && l.addEventListener("progress", Nr(a.onDownloadProgress, !0)), typeof a.onUploadProgress == "function" && l.upload && l.upload.addEventListener("progress", Nr(a.onUploadProgress)), (a.cancelToken || a.signal) && (c = (p) => {
      l && (n(!p || p.type ? new Ie(null, e, l) : p), l.abort(), l = null);
    }, a.cancelToken && a.cancelToken.subscribe(c), a.signal && (a.signal.aborted ? c() : a.signal.addEventListener("abort", c)));
    const v = US(a.url);
    if (v && q.protocols.indexOf(v) === -1) {
      n(new b("Unsupported protocol " + v + ":", b.ERR_BAD_REQUEST, e));
      return;
    }
    l.send(i || null);
  });
}, JS = (e, r) => {
  let t = new AbortController(), n;
  const a = function(c) {
    if (!n) {
      n = !0, o();
      const u = c instanceof Error ? c : this.reason;
      t.abort(u instanceof b ? u : new Ie(u instanceof Error ? u.message : u));
    }
  };
  let i = r && setTimeout(() => {
    a(new b(`timeout ${r} of ms exceeded`, b.ETIMEDOUT));
  }, r);
  const o = () => {
    e && (i && clearTimeout(i), i = null, e.forEach((c) => {
      c && (c.removeEventListener ? c.removeEventListener("abort", a) : c.unsubscribe(a));
    }), e = null);
  };
  e.forEach((c) => c && c.addEventListener && c.addEventListener("abort", a));
  const { signal: s } = t;
  return s.unsubscribe = o, [s, () => {
    i && clearTimeout(i), i = null;
  }];
}, KS = function* (e, r) {
  let t = e.byteLength;
  if (!r || t < r) {
    yield e;
    return;
  }
  let n = 0, a;
  for (; n < t; )
    a = n + r, yield e.slice(n, a), n = a;
}, XS = async function* (e, r, t) {
  for await (const n of e)
    yield* KS(ArrayBuffer.isView(n) ? n : await t(String(n)), r);
}, Ua = (e, r, t, n, a) => {
  const i = XS(e, r, a);
  let o = 0;
  return new ReadableStream({
    type: "bytes",
    async pull(s) {
      const { done: c, value: u } = await i.next();
      if (c) {
        s.close(), n();
        return;
      }
      let l = u.byteLength;
      t && t(o += l), s.enqueue(new Uint8Array(u));
    },
    cancel(s) {
      return n(s), i.return();
    }
  }, {
    highWaterMark: 2
  });
}, Ba = (e, r) => {
  const t = e != null;
  return (n) => setTimeout(() => r({
    lengthComputable: t,
    total: e,
    loaded: n
  }));
}, Vr = typeof fetch == "function" && typeof Request == "function" && typeof Response == "function", cs = Vr && typeof ReadableStream == "function", wt = Vr && (typeof TextEncoder == "function" ? /* @__PURE__ */ ((e) => (r) => e.encode(r))(new TextEncoder()) : async (e) => new Uint8Array(await new Response(e).arrayBuffer())), ZS = cs && (() => {
  let e = !1;
  const r = new Request(q.origin, {
    body: new ReadableStream(),
    method: "POST",
    get duplex() {
      return e = !0, "half";
    }
  }).headers.has("Content-Type");
  return e && !r;
})(), Wa = 64 * 1024, Et = cs && !!(() => {
  try {
    return d.isReadableStream(new Response("").body);
  } catch {
  }
})(), Dr = {
  stream: Et && ((e) => e.body)
};
Vr && ((e) => {
  ["text", "arrayBuffer", "blob", "formData", "stream"].forEach((r) => {
    !Dr[r] && (Dr[r] = d.isFunction(e[r]) ? (t) => t[r]() : (t, n) => {
      throw new b(`Response type '${r}' is not supported`, b.ERR_NOT_SUPPORT, n);
    });
  });
})(new Response());
const QS = async (e) => {
  if (e == null)
    return 0;
  if (d.isBlob(e))
    return e.size;
  if (d.isSpecCompliantForm(e))
    return (await new Request(e).arrayBuffer()).byteLength;
  if (d.isArrayBufferView(e))
    return e.byteLength;
  if (d.isURLSearchParams(e) && (e = e + ""), d.isString(e))
    return (await wt(e)).byteLength;
}, jS = async (e, r) => {
  const t = d.toFiniteNumber(e.getContentLength());
  return t ?? QS(r);
}, e5 = Vr && (async (e) => {
  let {
    url: r,
    method: t,
    data: n,
    signal: a,
    cancelToken: i,
    timeout: o,
    onDownloadProgress: s,
    onUploadProgress: c,
    responseType: u,
    headers: l,
    withCredentials: f = "same-origin",
    fetchOptions: v
  } = ls(e);
  u = u ? (u + "").toLowerCase() : "text";
  let [p, h] = a || i || o ? JS([a, i], o) : [], m, $;
  const y = () => {
    !m && setTimeout(() => {
      p && p.unsubscribe();
    }), m = !0;
  };
  let O;
  try {
    if (c && ZS && t !== "get" && t !== "head" && (O = await jS(l, n)) !== 0) {
      let k = new Request(r, {
        method: "POST",
        body: n,
        duplex: "half"
      }), ee;
      d.isFormData(n) && (ee = k.headers.get("content-type")) && l.setContentType(ee), k.body && (n = Ua(k.body, Wa, Ba(
        O,
        Nr(c)
      ), null, wt));
    }
    d.isString(f) || (f = f ? "cors" : "omit"), $ = new Request(r, {
      ...v,
      signal: p,
      method: t.toUpperCase(),
      headers: l.normalize().toJSON(),
      body: n,
      duplex: "half",
      withCredentials: f
    });
    let g = await fetch($);
    const _ = Et && (u === "stream" || u === "response");
    if (Et && (s || _)) {
      const k = {};
      ["status", "statusText", "headers"].forEach((Le) => {
        k[Le] = g[Le];
      });
      const ee = d.toFiniteNumber(g.headers.get("content-length"));
      g = new Response(
        Ua(g.body, Wa, s && Ba(
          ee,
          Nr(s, !0)
        ), _ && y, wt),
        k
      );
    }
    u = u || "text";
    let M = await Dr[d.findKey(Dr, u) || "text"](g, e);
    return !_ && y(), h && h(), await new Promise((k, ee) => {
      ss(k, ee, {
        data: M,
        headers: H.from(g.headers),
        status: g.status,
        statusText: g.statusText,
        config: e,
        request: $
      });
    });
  } catch (g) {
    throw y(), g && g.name === "TypeError" && /fetch/i.test(g.message) ? Object.assign(
      new b("Network Error", b.ERR_NETWORK, e, $),
      {
        cause: g.cause || g
      }
    ) : b.from(g, g && g.code, e, $);
  }
}), Ot = {
  http: gS,
  xhr: VS,
  fetch: e5
};
d.forEach(Ot, (e, r) => {
  if (e) {
    try {
      Object.defineProperty(e, "name", { value: r });
    } catch {
    }
    Object.defineProperty(e, "adapterName", { value: r });
  }
});
const za = (e) => `- ${e}`, r5 = (e) => d.isFunction(e) || e === null || e === !1, fs = {
  getAdapter: (e) => {
    e = d.isArray(e) ? e : [e];
    const { length: r } = e;
    let t, n;
    const a = {};
    for (let i = 0; i < r; i++) {
      t = e[i];
      let o;
      if (n = t, !r5(t) && (n = Ot[(o = String(t)).toLowerCase()], n === void 0))
        throw new b(`Unknown adapter '${o}'`);
      if (n)
        break;
      a[o || "#" + i] = n;
    }
    if (!n) {
      const i = Object.entries(a).map(
        ([s, c]) => `adapter ${s} ` + (c === !1 ? "is not supported by the environment" : "is not available in the build")
      );
      let o = r ? i.length > 1 ? `since :
` + i.map(za).join(`
`) : " " + za(i[0]) : "as no adapter specified";
      throw new b(
        "There is no suitable adapter to dispatch the request " + o,
        "ERR_NOT_SUPPORT"
      );
    }
    return n;
  },
  adapters: Ot
};
function dt(e) {
  if (e.cancelToken && e.cancelToken.throwIfRequested(), e.signal && e.signal.aborted)
    throw new Ie(null, e);
}
function qa(e) {
  return dt(e), e.headers = H.from(e.headers), e.data = ft.call(
    e,
    e.transformRequest
  ), ["post", "put", "patch"].indexOf(e.method) !== -1 && e.headers.setContentType("application/x-www-form-urlencoded", !1), fs.getAdapter(e.adapter || Zt.adapter)(e).then(function(n) {
    return dt(e), n.data = ft.call(
      e,
      e.transformResponse,
      n
    ), n.headers = H.from(n.headers), n;
  }, function(n) {
    return os(n) || (dt(e), n && n.response && (n.response.data = ft.call(
      e,
      e.transformResponse,
      n.response
    ), n.response.headers = H.from(n.response.headers))), Promise.reject(n);
  });
}
const ds = "1.7.2", Qt = {};
["object", "boolean", "number", "function", "string", "symbol"].forEach((e, r) => {
  Qt[e] = function(n) {
    return typeof n === e || "a" + (r < 1 ? "n " : " ") + e;
  };
});
const Ha = {};
Qt.transitional = function(r, t, n) {
  function a(i, o) {
    return "[Axios v" + ds + "] Transitional option '" + i + "'" + o + (n ? ". " + n : "");
  }
  return (i, o, s) => {
    if (r === !1)
      throw new b(
        a(o, " has been removed" + (t ? " in " + t : "")),
        b.ERR_DEPRECATED
      );
    return t && !Ha[o] && (Ha[o] = !0, console.warn(
      a(
        o,
        " has been deprecated since v" + t + " and will be removed in the near future"
      )
    )), r ? r(i, o, s) : !0;
  };
};
function t5(e, r, t) {
  if (typeof e != "object")
    throw new b("options must be an object", b.ERR_BAD_OPTION_VALUE);
  const n = Object.keys(e);
  let a = n.length;
  for (; a-- > 0; ) {
    const i = n[a], o = r[i];
    if (o) {
      const s = e[i], c = s === void 0 || o(s, i, e);
      if (c !== !0)
        throw new b("option " + i + " must be " + c, b.ERR_BAD_OPTION_VALUE);
      continue;
    }
    if (t !== !0)
      throw new b("Unknown option " + i, b.ERR_BAD_OPTION);
  }
}
const Nt = {
  assertOptions: t5,
  validators: Qt
}, ue = Nt.validators;
class Tr {
  constructor(r) {
    this.defaults = r, this.interceptors = {
      request: new Ma(),
      response: new Ma()
    };
  }
  /**
   * Dispatch a request
   *
   * @param {String|Object} configOrUrl The config specific for this request (merged with this.defaults)
   * @param {?Object} config
   *
   * @returns {Promise} The Promise to be fulfilled
   */
  async request(r, t) {
    try {
      return await this._request(r, t);
    } catch (n) {
      if (n instanceof Error) {
        let a;
        Error.captureStackTrace ? Error.captureStackTrace(a = {}) : a = new Error();
        const i = a.stack ? a.stack.replace(/^.+\n/, "") : "";
        try {
          n.stack ? i && !String(n.stack).endsWith(i.replace(/^.+\n.+\n/, "")) && (n.stack += `
` + i) : n.stack = i;
        } catch {
        }
      }
      throw n;
    }
  }
  _request(r, t) {
    typeof r == "string" ? (t = t || {}, t.url = r) : t = r || {}, t = _e(this.defaults, t);
    const { transitional: n, paramsSerializer: a, headers: i } = t;
    n !== void 0 && Nt.assertOptions(n, {
      silentJSONParsing: ue.transitional(ue.boolean),
      forcedJSONParsing: ue.transitional(ue.boolean),
      clarifyTimeoutError: ue.transitional(ue.boolean)
    }, !1), a != null && (d.isFunction(a) ? t.paramsSerializer = {
      serialize: a
    } : Nt.assertOptions(a, {
      encode: ue.function,
      serialize: ue.function
    }, !0)), t.method = (t.method || this.defaults.method || "get").toLowerCase();
    let o = i && d.merge(
      i.common,
      i[t.method]
    );
    i && d.forEach(
      ["delete", "get", "head", "post", "put", "patch", "common"],
      (h) => {
        delete i[h];
      }
    ), t.headers = H.concat(o, i);
    const s = [];
    let c = !0;
    this.interceptors.request.forEach(function(m) {
      typeof m.runWhen == "function" && m.runWhen(t) === !1 || (c = c && m.synchronous, s.unshift(m.fulfilled, m.rejected));
    });
    const u = [];
    this.interceptors.response.forEach(function(m) {
      u.push(m.fulfilled, m.rejected);
    });
    let l, f = 0, v;
    if (!c) {
      const h = [qa.bind(this), void 0];
      for (h.unshift.apply(h, s), h.push.apply(h, u), v = h.length, l = Promise.resolve(t); f < v; )
        l = l.then(h[f++], h[f++]);
      return l;
    }
    v = s.length;
    let p = t;
    for (f = 0; f < v; ) {
      const h = s[f++], m = s[f++];
      try {
        p = h(p);
      } catch ($) {
        m.call(this, $);
        break;
      }
    }
    try {
      l = qa.call(this, p);
    } catch (h) {
      return Promise.reject(h);
    }
    for (f = 0, v = u.length; f < v; )
      l = l.then(u[f++], u[f++]);
    return l;
  }
  getUri(r) {
    r = _e(this.defaults, r);
    const t = us(r.baseURL, r.url);
    return ns(t, r.params, r.paramsSerializer);
  }
}
d.forEach(["delete", "get", "head", "options"], function(r) {
  Tr.prototype[r] = function(t, n) {
    return this.request(_e(n || {}, {
      method: r,
      url: t,
      data: (n || {}).data
    }));
  };
});
d.forEach(["post", "put", "patch"], function(r) {
  function t(n) {
    return function(i, o, s) {
      return this.request(_e(s || {}, {
        method: r,
        headers: n ? {
          "Content-Type": "multipart/form-data"
        } : {},
        url: i,
        data: o
      }));
    };
  }
  Tr.prototype[r] = t(), Tr.prototype[r + "Form"] = t(!0);
});
const mr = Tr;
class jt {
  constructor(r) {
    if (typeof r != "function")
      throw new TypeError("executor must be a function.");
    let t;
    this.promise = new Promise(function(i) {
      t = i;
    });
    const n = this;
    this.promise.then((a) => {
      if (!n._listeners)
        return;
      let i = n._listeners.length;
      for (; i-- > 0; )
        n._listeners[i](a);
      n._listeners = null;
    }), this.promise.then = (a) => {
      let i;
      const o = new Promise((s) => {
        n.subscribe(s), i = s;
      }).then(a);
      return o.cancel = function() {
        n.unsubscribe(i);
      }, o;
    }, r(function(i, o, s) {
      n.reason || (n.reason = new Ie(i, o, s), t(n.reason));
    });
  }
  /**
   * Throws a `CanceledError` if cancellation has been requested.
   */
  throwIfRequested() {
    if (this.reason)
      throw this.reason;
  }
  /**
   * Subscribe to the cancel signal
   */
  subscribe(r) {
    if (this.reason) {
      r(this.reason);
      return;
    }
    this._listeners ? this._listeners.push(r) : this._listeners = [r];
  }
  /**
   * Unsubscribe from the cancel signal
   */
  unsubscribe(r) {
    if (!this._listeners)
      return;
    const t = this._listeners.indexOf(r);
    t !== -1 && this._listeners.splice(t, 1);
  }
  /**
   * Returns an object that contains a new `CancelToken` and a function that, when called,
   * cancels the `CancelToken`.
   */
  static source() {
    let r;
    return {
      token: new jt(function(a) {
        r = a;
      }),
      cancel: r
    };
  }
}
const n5 = jt;
function a5(e) {
  return function(t) {
    return e.apply(null, t);
  };
}
function i5(e) {
  return d.isObject(e) && e.isAxiosError === !0;
}
const Dt = {
  Continue: 100,
  SwitchingProtocols: 101,
  Processing: 102,
  EarlyHints: 103,
  Ok: 200,
  Created: 201,
  Accepted: 202,
  NonAuthoritativeInformation: 203,
  NoContent: 204,
  ResetContent: 205,
  PartialContent: 206,
  MultiStatus: 207,
  AlreadyReported: 208,
  ImUsed: 226,
  MultipleChoices: 300,
  MovedPermanently: 301,
  Found: 302,
  SeeOther: 303,
  NotModified: 304,
  UseProxy: 305,
  Unused: 306,
  TemporaryRedirect: 307,
  PermanentRedirect: 308,
  BadRequest: 400,
  Unauthorized: 401,
  PaymentRequired: 402,
  Forbidden: 403,
  NotFound: 404,
  MethodNotAllowed: 405,
  NotAcceptable: 406,
  ProxyAuthenticationRequired: 407,
  RequestTimeout: 408,
  Conflict: 409,
  Gone: 410,
  LengthRequired: 411,
  PreconditionFailed: 412,
  PayloadTooLarge: 413,
  UriTooLong: 414,
  UnsupportedMediaType: 415,
  RangeNotSatisfiable: 416,
  ExpectationFailed: 417,
  ImATeapot: 418,
  MisdirectedRequest: 421,
  UnprocessableEntity: 422,
  Locked: 423,
  FailedDependency: 424,
  TooEarly: 425,
  UpgradeRequired: 426,
  PreconditionRequired: 428,
  TooManyRequests: 429,
  RequestHeaderFieldsTooLarge: 431,
  UnavailableForLegalReasons: 451,
  InternalServerError: 500,
  NotImplemented: 501,
  BadGateway: 502,
  ServiceUnavailable: 503,
  GatewayTimeout: 504,
  HttpVersionNotSupported: 505,
  VariantAlsoNegotiates: 506,
  InsufficientStorage: 507,
  LoopDetected: 508,
  NotExtended: 510,
  NetworkAuthenticationRequired: 511
};
Object.entries(Dt).forEach(([e, r]) => {
  Dt[r] = e;
});
const o5 = Dt;
function vs(e) {
  const r = new mr(e), t = Go(mr.prototype.request, r);
  return d.extend(t, mr.prototype, r, { allOwnKeys: !0 }), d.extend(t, r, null, { allOwnKeys: !0 }), t.create = function(a) {
    return vs(_e(e, a));
  }, t;
}
const T = vs(Zt);
T.Axios = mr;
T.CanceledError = Ie;
T.CancelToken = n5;
T.isCancel = os;
T.VERSION = ds;
T.toFormData = Gr;
T.AxiosError = b;
T.Cancel = T.CanceledError;
T.all = function(r) {
  return Promise.all(r);
};
T.spread = a5;
T.isAxiosError = i5;
T.mergeConfig = _e;
T.AxiosHeaders = H;
T.formToJSON = (e) => is(d.isHTMLForm(e) ? new FormData(e) : e);
T.getAdapter = fs.getAdapter;
T.HttpStatusCode = o5;
T.default = T;
let vt = 0;
const s5 = (e) => {
  vt > 0 || (console.error(e), Tt("error", e.message), vt++, setTimeout(() => {
    vt = 0;
  }, 1e3));
}, Tt = (e, r) => {
  Ce.notifications && Ce.notifications[e] && Ce.notifications[e](r);
}, Ga = T.create(), Ce = {
  host: {},
  axios: Ga,
  token: "",
  notifications: null,
  setup(e = {}) {
    e.host && (this.host = e.host), e.notifications && (this.notifications = e.notifications), e.token && (this.token = e.token), e.headers && Object.keys(e.headers).forEach((r) => {
      Ga.defaults.headers[r] = e.headers[r];
    });
  },
  buildUrl(e) {
    const r = e.split(":");
    return r.length == 2 && this.host.hasOwnProperty([r[0]]) ? this.host[r[0]] + r[1] : e;
  },
  request(e = {}) {
    return e.url = this.buildUrl(e.url), new Promise((r, t) => this.axios(e).then((n) => {
      const a = n.data;
      a.status !== void 0 ? (a.status <= 200 ? r(a) : t(a), a.message && Tt(a.status <= 100 ? "success" : "alert", a.message)) : r(a);
    }).catch((n) => {
      t(n), s5(n);
    }));
  },
  get(e, r = {}) {
    return r.method = "get", r.url = e, this.request(r);
  },
  delete(e, r = {}) {
    return r.method = "delete", r.url = e, this.request(r);
  },
  post(e, r = {}) {
    return r.method = "post", r.url = e, r.data = r.data || {}, this.request(r);
  },
  put(e, r = {}) {
    return r.method = "put", r.url = e, r.data = r.data || {}, this.request(r);
  },
  upload(e, r = {}) {
    return r.method = "post", r.url = e, r.headers = {
      "Content-Type": "multipart/form-data"
    }, this.request(r);
  },
  open(e, r, t) {
    /\?/.test(e) ? e += `&token=${this.token}` : e += `?token=${this.token}`, window.open(e, r, t);
  },
  download(e, r = {}, t) {
    return r.url = e, r.method = r.method || "get", r.responseType = "blob", this.request(r).then((n) => {
      if (n.type == "application/json")
        n.text().then((a) => {
          const i = JSON.parse(a);
          i.message && Tt("alert", i.message);
        });
      else {
        let a = window.URL.createObjectURL(n), i = document.createElement("a");
        i.href = a, i.download = t || r.fileName || "file", i.target = "_blank", document.body.appendChild(i), i.click(), i.remove(), window.URL.revokeObjectURL(a);
      }
    });
  }
}, u5 = (e) => {
  e.config.globalProperties.$http = Ce, e.config.globalProperties.$session = gt, e.config.globalProperties.$storage = Sr, e.config.globalProperties.$utils = Yt;
}, l5 = {
  install: u5
}, c5 = { class: "bs-resizer-bar" }, f5 = {
  __name: "Resizer",
  props: {
    type: {
      type: String,
      default: "right",
      validator(e) {
        return ["left", "right", "top", "bottom"].includes(e);
      }
    }
  },
  emits: ["move"],
  setup(e, { expose: r, emit: t }) {
    const n = e;
    let a = null, i = 0, o = 0;
    const s = Ar(null);
    At(() => {
      var p;
      a = (p = s.value) == null ? void 0 : p.parentElement, a && (a.classList.add("resize-" + n.type), i = a.clientWidth, o = a.clientHeight);
    });
    const c = t, u = Rr("layout", null), l = (p) => {
      p != null && p.parent ? l(p.parent) : p == null || p.doLayout();
    }, f = (p) => {
      p.preventDefault();
      const h = document.onmousemove, m = document.onmouseup, $ = a.clientWidth, y = a.clientHeight, O = p.clientX, g = p.clientY;
      document.onmousemove = (_) => {
        _.preventDefault(), n.type == "left" && c("move", $ - _.clientX + O, 0), n.type == "right" && c("move", $ + _.clientX - O, 0), n.type == "top" && c("move", 0, y - _.clientY + g), n.type == "bottom" && c("move", 0, y + _.clientY - g);
      }, document.onmouseup = () => {
        document.onmousemove = h, document.onmouseup = m, setTimeout(() => {
          l(u);
        }, 100);
      };
    }, v = () => {
      (n.type == "left" || n.type == "right") && c("move", i, 0), (n.type == "top" || n.type == "bottom") && c("move", 0, o), setTimeout(() => {
        l(u);
      }, 100);
    };
    return r({
      reset: v
    }), (p, h) => (S(), D("div", {
      ref_key: "root",
      ref: s,
      class: Ge(["bs-resizer", `is-${n.type}`]),
      onMousedown: f,
      onDblclick: h[0] || (h[0] = (m) => v())
    }, [
      Z("div", c5, [
        (S(), D(de, null, Re(8, (m) => Z("i", { key: m })), 64))
      ])
    ], 34));
  }
}, d5 = {
  __name: "Layout",
  props: {
    type: {
      type: String,
      default: "col"
    },
    width: [String, Number],
    height: [String, Number],
    flex: String,
    resize: String
  },
  setup(e, { expose: r }) {
    const t = e, n = t.width !== void 0 || t.height !== void 0, a = [
      "bs-layout",
      t.type == "row" ? "flex-row" : "flex-col",
      t.flex == null ? n ? "flex-none" : "flex-1" : "flex-" + t.flex
    ], i = qe({
      width: t.width === void 0 ? null : isNaN(Number(t.width)) ? t.width : t.width + "px",
      height: t.height === void 0 ? null : isNaN(Number(t.height)) ? t.height : t.height + "px"
    }), o = (p, h) => {
      p > 0 && (i.width = p + "px"), h > 0 && (i.height = h + "px");
    }, s = (p) => {
      p && l.push(p);
    }, c = () => {
      l.forEach((p) => {
        p.doLayout();
      });
    }, u = Rr("layout", null), l = [], f = {
      parent: u,
      addChild: s,
      doLayout: c
    };
    u && u.addChild(f);
    const v = Yt.debounce(c, 100);
    return At(() => {
      f.parent || window.addEventListener("resize", v);
    }), Ss(() => {
      f.parent || window.removeEventListener("resize", v);
    }), Ja("layout", f), r({
      doLayout: c
    }), (p, h) => (S(), D("div", {
      ref: "root",
      class: Ge(a),
      style: Rt(i)
    }, [
      t.resize && P(n) ? (S(), I(f5, {
        key: 0,
        type: t.resize,
        onMove: o
      }, null, 8, ["type"])) : ve("", !0),
      N(p.$slots, "default")
    ], 4));
  }
}, v5 = {
  __name: "Content",
  props: {
    width: [String, Number],
    height: [String, Number],
    flex: String
  },
  emits: ["layout"],
  setup(e, { expose: r, emit: t }) {
    const n = e, a = n.width !== void 0 || n.height !== void 0, i = [
      "bs-content",
      n.flex == null ? a ? "flex-none" : "flex-1" : "flex-" + n.flex
    ], o = qe({
      width: n.width === void 0 ? null : isNaN(Number(n.width)) ? n.width : n.width + "px",
      height: n.height === void 0 ? null : isNaN(Number(n.height)) ? n.height : n.height + "px"
    }), s = qe({
      width: 0,
      height: 0,
      clientWidth: 0,
      clientHeight: 0
    }), c = t, u = Ar(null), l = () => {
      if (!u.value)
        return;
      const v = u.value, p = window.getComputedStyle(v);
      let h = Number(p.paddingLeft.replace("px", "")), m = Number(p.paddingRight.replace("px", "")), $ = Number(p.paddingTop.replace("px", "")), y = Number(p.paddingBottom.replace("px", ""));
      isNaN(h) && (h = 0), isNaN(m) && (m = 0), isNaN($) && ($ = 0), isNaN(y) && (y = 0), s.clientWidth = v.clientWidth, s.clientHeight = v.clientHeight, s.width = v.clientWidth - h - m, s.height = v.clientHeight - $ - y, c("layout", s);
    }, f = Rr("layout", null);
    return f && f.addChild({
      doLayout: l
    }), At(() => {
      l();
    }), r({
      size: s,
      doLayout: l
    }), (v, p) => (S(), D("div", {
      ref_key: "root",
      ref: u,
      class: Ge(i),
      style: Rt(o)
    }, [
      N(v.$slots, "default", _r(br(s)))
    ], 4));
  }
}, h5 = {}, p5 = { class: "bs-toolbar" };
function m5(e, r) {
  return S(), D("div", p5, [
    N(e.$slots, "default")
  ]);
}
const g5 = /* @__PURE__ */ Ye(h5, [["render", m5]]), $5 = {}, y5 = { class: "bs-title" }, _5 = {
  key: 0,
  class: "bs-title-icon"
}, b5 = { class: "bs-title-text" };
function S5(e, r) {
  return S(), D("div", y5, [
    e.$slots.icon ? (S(), D("div", _5, [
      N(e.$slots, "icon")
    ])) : ve("", !0),
    Z("div", b5, [
      N(e.$slots, "default")
    ])
  ]);
}
const w5 = /* @__PURE__ */ Ye($5, [["render", S5]]), Ya = {
  AppHeader: Qs,
  AppSider: lu,
  AppMenu: du,
  AppContainer: tu,
  AppContent: ou,
  AppNavbar: vu,
  BsLayout: d5,
  BsContent: v5,
  BsToolbar: g5,
  BsTitle: w5
}, E5 = function(e, r = {}) {
  e.use(Gs, r == null ? void 0 : r.icon), Object.keys(Ya).forEach((t) => {
    e.component(t, Ya[t]);
  });
}, O5 = {
  install: E5
}, N5 = {
  __name: "Modal",
  props: {
    fullscreen: Boolean,
    draggable: {
      type: Boolean,
      default: !0
    }
  },
  setup(e) {
    const r = e, t = ws(), n = Object.keys(Es()).filter((u) => u != "default"), a = Ar(null), i = {
      modelEl: null,
      headerEl: null,
      left: 0,
      top: 0
    };
    function o() {
      var u, l;
      r.draggable && !r.fullscreen && (i.modelEl = (u = a.value) == null ? void 0 : u.parentElement.parentElement.parentElement, i.modelEl && (i.headerEl = i.modelEl.querySelector(".ant-modal-header")), i.headerEl && (i.top = i.modelEl.offsetTop, i.headerEl.addEventListener("mousedown", s))), (l = a.value) == null || l.remove();
    }
    function s(u) {
      u.preventDefault();
      const l = document.onmousemove, f = document.onmouseup, v = i.modelEl, p = (document.body.clientWidth - v.clientWidth) / 2, h = document.body.clientHeight - v.clientHeight, m = u.clientX - i.left, $ = u.clientY - i.top;
      document.onmousemove = (y) => {
        y.preventDefault(), i.left = Math.min(Math.max(y.clientX - m, -p), p), i.top = Math.min(Math.max(y.clientY - $, 0), h), v.style.left = i.left + "px", v.style.top = i.top + "px";
      }, document.onmouseup = () => {
        document.onmousemove = l, document.onmouseup = f;
      };
    }
    let c = !0;
    return yr(() => t.open, (u) => {
      u && c && (c = !1, Os(() => {
        o();
      }));
    }), Ns(() => {
      i.headerEl && i.headerEl.removeEventListener("mousedown", s);
    }), (u, l) => (S(), I(P(kt), {
      width: r.fullscreen ? "100%" : P(t).width,
      class: Ge({ "is-fullscreen": e.fullscreen, "is-draggable": e.draggable })
    }, Ka({
      default: A(() => [
        Z("div", {
          ref_key: "body",
          ref: a,
          style: { display: "none" }
        }, null, 512),
        N(u.$slots, "default")
      ]),
      _: 2
    }, [
      Re(P(n), (f) => ({
        name: f,
        fn: A((v) => [
          N(u.$slots, f, _r(br(v || {})))
        ])
      }))
    ]), 1032, ["width", "class"]));
  }
};
var hs = {}, ps = {}, en = { exports: {} }, E = String, ms = function() {
  return { isColorSupported: !1, reset: E, bold: E, dim: E, italic: E, underline: E, inverse: E, hidden: E, strikethrough: E, black: E, red: E, green: E, yellow: E, blue: E, magenta: E, cyan: E, white: E, gray: E, bgBlack: E, bgRed: E, bgGreen: E, bgYellow: E, bgBlue: E, bgMagenta: E, bgCyan: E, bgWhite: E };
};
en.exports = ms();
en.exports.createColors = ms;
var D5 = en.exports;
(function(e) {
  var r = {};
  Object.defineProperty(e, "__esModule", {
    value: !0
  });
  function t(u, l) {
    for (var f in l)
      Object.defineProperty(u, f, {
        enumerable: !0,
        get: l[f]
      });
  }
  t(e, {
    dim: function() {
      return s;
    },
    default: function() {
      return c;
    }
  });
  const n = /* @__PURE__ */ a(D5);
  function a(u) {
    return u && u.__esModule ? u : {
      default: u
    };
  }
  let i = /* @__PURE__ */ new Set();
  function o(u, l, f) {
    typeof process < "u" && r.JEST_WORKER_ID || f && i.has(f) || (f && i.add(f), console.warn(""), l.forEach((v) => console.warn(u, "-", v)));
  }
  function s(u) {
    return n.default.dim(u);
  }
  const c = {
    info(u, l) {
      o(n.default.bold(n.default.cyan("info")), ...Array.isArray(u) ? [
        u
      ] : [
        l,
        u
      ]);
    },
    warn(u, l) {
      o(n.default.bold(n.default.yellow("warn")), ...Array.isArray(u) ? [
        u
      ] : [
        l,
        u
      ]);
    },
    risk(u, l) {
      o(n.default.bold(n.default.magenta("risk")), ...Array.isArray(u) ? [
        u
      ] : [
        l,
        u
      ]);
    }
  };
})(ps);
(function(e) {
  Object.defineProperty(e, "__esModule", {
    value: !0
  }), Object.defineProperty(e, "default", {
    enumerable: !0,
    get: function() {
      return a;
    }
  });
  const r = /* @__PURE__ */ t(ps);
  function t(i) {
    return i && i.__esModule ? i : {
      default: i
    };
  }
  function n({ version: i, from: o, to: s }) {
    r.default.warn(`${o}-color-renamed`, [
      `As of Tailwind CSS ${i}, \`${o}\` has been renamed to \`${s}\`.`,
      "Update your configuration file to silence this warning."
    ]);
  }
  const a = {
    inherit: "inherit",
    current: "currentColor",
    transparent: "transparent",
    black: "#000",
    white: "#fff",
    slate: {
      50: "#f8fafc",
      100: "#f1f5f9",
      200: "#e2e8f0",
      300: "#cbd5e1",
      400: "#94a3b8",
      500: "#64748b",
      600: "#475569",
      700: "#334155",
      800: "#1e293b",
      900: "#0f172a",
      950: "#020617"
    },
    gray: {
      50: "#f9fafb",
      100: "#f3f4f6",
      200: "#e5e7eb",
      300: "#d1d5db",
      400: "#9ca3af",
      500: "#6b7280",
      600: "#4b5563",
      700: "#374151",
      800: "#1f2937",
      900: "#111827",
      950: "#030712"
    },
    zinc: {
      50: "#fafafa",
      100: "#f4f4f5",
      200: "#e4e4e7",
      300: "#d4d4d8",
      400: "#a1a1aa",
      500: "#71717a",
      600: "#52525b",
      700: "#3f3f46",
      800: "#27272a",
      900: "#18181b",
      950: "#09090b"
    },
    neutral: {
      50: "#fafafa",
      100: "#f5f5f5",
      200: "#e5e5e5",
      300: "#d4d4d4",
      400: "#a3a3a3",
      500: "#737373",
      600: "#525252",
      700: "#404040",
      800: "#262626",
      900: "#171717",
      950: "#0a0a0a"
    },
    stone: {
      50: "#fafaf9",
      100: "#f5f5f4",
      200: "#e7e5e4",
      300: "#d6d3d1",
      400: "#a8a29e",
      500: "#78716c",
      600: "#57534e",
      700: "#44403c",
      800: "#292524",
      900: "#1c1917",
      950: "#0c0a09"
    },
    red: {
      50: "#fef2f2",
      100: "#fee2e2",
      200: "#fecaca",
      300: "#fca5a5",
      400: "#f87171",
      500: "#ef4444",
      600: "#dc2626",
      700: "#b91c1c",
      800: "#991b1b",
      900: "#7f1d1d",
      950: "#450a0a"
    },
    orange: {
      50: "#fff7ed",
      100: "#ffedd5",
      200: "#fed7aa",
      300: "#fdba74",
      400: "#fb923c",
      500: "#f97316",
      600: "#ea580c",
      700: "#c2410c",
      800: "#9a3412",
      900: "#7c2d12",
      950: "#431407"
    },
    amber: {
      50: "#fffbeb",
      100: "#fef3c7",
      200: "#fde68a",
      300: "#fcd34d",
      400: "#fbbf24",
      500: "#f59e0b",
      600: "#d97706",
      700: "#b45309",
      800: "#92400e",
      900: "#78350f",
      950: "#451a03"
    },
    yellow: {
      50: "#fefce8",
      100: "#fef9c3",
      200: "#fef08a",
      300: "#fde047",
      400: "#facc15",
      500: "#eab308",
      600: "#ca8a04",
      700: "#a16207",
      800: "#854d0e",
      900: "#713f12",
      950: "#422006"
    },
    lime: {
      50: "#f7fee7",
      100: "#ecfccb",
      200: "#d9f99d",
      300: "#bef264",
      400: "#a3e635",
      500: "#84cc16",
      600: "#65a30d",
      700: "#4d7c0f",
      800: "#3f6212",
      900: "#365314",
      950: "#1a2e05"
    },
    green: {
      50: "#f0fdf4",
      100: "#dcfce7",
      200: "#bbf7d0",
      300: "#86efac",
      400: "#4ade80",
      500: "#22c55e",
      600: "#16a34a",
      700: "#15803d",
      800: "#166534",
      900: "#14532d",
      950: "#052e16"
    },
    emerald: {
      50: "#ecfdf5",
      100: "#d1fae5",
      200: "#a7f3d0",
      300: "#6ee7b7",
      400: "#34d399",
      500: "#10b981",
      600: "#059669",
      700: "#047857",
      800: "#065f46",
      900: "#064e3b",
      950: "#022c22"
    },
    teal: {
      50: "#f0fdfa",
      100: "#ccfbf1",
      200: "#99f6e4",
      300: "#5eead4",
      400: "#2dd4bf",
      500: "#14b8a6",
      600: "#0d9488",
      700: "#0f766e",
      800: "#115e59",
      900: "#134e4a",
      950: "#042f2e"
    },
    cyan: {
      50: "#ecfeff",
      100: "#cffafe",
      200: "#a5f3fc",
      300: "#67e8f9",
      400: "#22d3ee",
      500: "#06b6d4",
      600: "#0891b2",
      700: "#0e7490",
      800: "#155e75",
      900: "#164e63",
      950: "#083344"
    },
    sky: {
      50: "#f0f9ff",
      100: "#e0f2fe",
      200: "#bae6fd",
      300: "#7dd3fc",
      400: "#38bdf8",
      500: "#0ea5e9",
      600: "#0284c7",
      700: "#0369a1",
      800: "#075985",
      900: "#0c4a6e",
      950: "#082f49"
    },
    blue: {
      50: "#eff6ff",
      100: "#dbeafe",
      200: "#bfdbfe",
      300: "#93c5fd",
      400: "#60a5fa",
      500: "#3b82f6",
      600: "#2563eb",
      700: "#1d4ed8",
      800: "#1e40af",
      900: "#1e3a8a",
      950: "#172554"
    },
    indigo: {
      50: "#eef2ff",
      100: "#e0e7ff",
      200: "#c7d2fe",
      300: "#a5b4fc",
      400: "#818cf8",
      500: "#6366f1",
      600: "#4f46e5",
      700: "#4338ca",
      800: "#3730a3",
      900: "#312e81",
      950: "#1e1b4b"
    },
    violet: {
      50: "#f5f3ff",
      100: "#ede9fe",
      200: "#ddd6fe",
      300: "#c4b5fd",
      400: "#a78bfa",
      500: "#8b5cf6",
      600: "#7c3aed",
      700: "#6d28d9",
      800: "#5b21b6",
      900: "#4c1d95",
      950: "#2e1065"
    },
    purple: {
      50: "#faf5ff",
      100: "#f3e8ff",
      200: "#e9d5ff",
      300: "#d8b4fe",
      400: "#c084fc",
      500: "#a855f7",
      600: "#9333ea",
      700: "#7e22ce",
      800: "#6b21a8",
      900: "#581c87",
      950: "#3b0764"
    },
    fuchsia: {
      50: "#fdf4ff",
      100: "#fae8ff",
      200: "#f5d0fe",
      300: "#f0abfc",
      400: "#e879f9",
      500: "#d946ef",
      600: "#c026d3",
      700: "#a21caf",
      800: "#86198f",
      900: "#701a75",
      950: "#4a044e"
    },
    pink: {
      50: "#fdf2f8",
      100: "#fce7f3",
      200: "#fbcfe8",
      300: "#f9a8d4",
      400: "#f472b6",
      500: "#ec4899",
      600: "#db2777",
      700: "#be185d",
      800: "#9d174d",
      900: "#831843",
      950: "#500724"
    },
    rose: {
      50: "#fff1f2",
      100: "#ffe4e6",
      200: "#fecdd3",
      300: "#fda4af",
      400: "#fb7185",
      500: "#f43f5e",
      600: "#e11d48",
      700: "#be123c",
      800: "#9f1239",
      900: "#881337",
      950: "#4c0519"
    },
    get lightBlue() {
      return n({
        version: "v2.2",
        from: "lightBlue",
        to: "sky"
      }), this.sky;
    },
    get warmGray() {
      return n({
        version: "v3.0",
        from: "warmGray",
        to: "stone"
      }), this.stone;
    },
    get trueGray() {
      return n({
        version: "v3.0",
        from: "trueGray",
        to: "neutral"
      }), this.neutral;
    },
    get coolGray() {
      return n({
        version: "v3.0",
        from: "coolGray",
        to: "gray"
      }), this.gray;
    },
    get blueGray() {
      return n({
        version: "v3.0",
        from: "blueGray",
        to: "slate"
      }), this.slate;
    }
  };
})(hs);
let ht = hs;
var T5 = (ht.__esModule ? ht : { default: ht }).default;
const De = /* @__PURE__ */ ri(T5);
var Ct = {}, gs = {};
(function(e) {
  Object.defineProperty(e, "__esModule", {
    value: !0
  }), Object.defineProperty(e, "default", {
    enumerable: !0,
    get: function() {
      return r;
    }
  });
  const r = {
    aliceblue: [
      240,
      248,
      255
    ],
    antiquewhite: [
      250,
      235,
      215
    ],
    aqua: [
      0,
      255,
      255
    ],
    aquamarine: [
      127,
      255,
      212
    ],
    azure: [
      240,
      255,
      255
    ],
    beige: [
      245,
      245,
      220
    ],
    bisque: [
      255,
      228,
      196
    ],
    black: [
      0,
      0,
      0
    ],
    blanchedalmond: [
      255,
      235,
      205
    ],
    blue: [
      0,
      0,
      255
    ],
    blueviolet: [
      138,
      43,
      226
    ],
    brown: [
      165,
      42,
      42
    ],
    burlywood: [
      222,
      184,
      135
    ],
    cadetblue: [
      95,
      158,
      160
    ],
    chartreuse: [
      127,
      255,
      0
    ],
    chocolate: [
      210,
      105,
      30
    ],
    coral: [
      255,
      127,
      80
    ],
    cornflowerblue: [
      100,
      149,
      237
    ],
    cornsilk: [
      255,
      248,
      220
    ],
    crimson: [
      220,
      20,
      60
    ],
    cyan: [
      0,
      255,
      255
    ],
    darkblue: [
      0,
      0,
      139
    ],
    darkcyan: [
      0,
      139,
      139
    ],
    darkgoldenrod: [
      184,
      134,
      11
    ],
    darkgray: [
      169,
      169,
      169
    ],
    darkgreen: [
      0,
      100,
      0
    ],
    darkgrey: [
      169,
      169,
      169
    ],
    darkkhaki: [
      189,
      183,
      107
    ],
    darkmagenta: [
      139,
      0,
      139
    ],
    darkolivegreen: [
      85,
      107,
      47
    ],
    darkorange: [
      255,
      140,
      0
    ],
    darkorchid: [
      153,
      50,
      204
    ],
    darkred: [
      139,
      0,
      0
    ],
    darksalmon: [
      233,
      150,
      122
    ],
    darkseagreen: [
      143,
      188,
      143
    ],
    darkslateblue: [
      72,
      61,
      139
    ],
    darkslategray: [
      47,
      79,
      79
    ],
    darkslategrey: [
      47,
      79,
      79
    ],
    darkturquoise: [
      0,
      206,
      209
    ],
    darkviolet: [
      148,
      0,
      211
    ],
    deeppink: [
      255,
      20,
      147
    ],
    deepskyblue: [
      0,
      191,
      255
    ],
    dimgray: [
      105,
      105,
      105
    ],
    dimgrey: [
      105,
      105,
      105
    ],
    dodgerblue: [
      30,
      144,
      255
    ],
    firebrick: [
      178,
      34,
      34
    ],
    floralwhite: [
      255,
      250,
      240
    ],
    forestgreen: [
      34,
      139,
      34
    ],
    fuchsia: [
      255,
      0,
      255
    ],
    gainsboro: [
      220,
      220,
      220
    ],
    ghostwhite: [
      248,
      248,
      255
    ],
    gold: [
      255,
      215,
      0
    ],
    goldenrod: [
      218,
      165,
      32
    ],
    gray: [
      128,
      128,
      128
    ],
    green: [
      0,
      128,
      0
    ],
    greenyellow: [
      173,
      255,
      47
    ],
    grey: [
      128,
      128,
      128
    ],
    honeydew: [
      240,
      255,
      240
    ],
    hotpink: [
      255,
      105,
      180
    ],
    indianred: [
      205,
      92,
      92
    ],
    indigo: [
      75,
      0,
      130
    ],
    ivory: [
      255,
      255,
      240
    ],
    khaki: [
      240,
      230,
      140
    ],
    lavender: [
      230,
      230,
      250
    ],
    lavenderblush: [
      255,
      240,
      245
    ],
    lawngreen: [
      124,
      252,
      0
    ],
    lemonchiffon: [
      255,
      250,
      205
    ],
    lightblue: [
      173,
      216,
      230
    ],
    lightcoral: [
      240,
      128,
      128
    ],
    lightcyan: [
      224,
      255,
      255
    ],
    lightgoldenrodyellow: [
      250,
      250,
      210
    ],
    lightgray: [
      211,
      211,
      211
    ],
    lightgreen: [
      144,
      238,
      144
    ],
    lightgrey: [
      211,
      211,
      211
    ],
    lightpink: [
      255,
      182,
      193
    ],
    lightsalmon: [
      255,
      160,
      122
    ],
    lightseagreen: [
      32,
      178,
      170
    ],
    lightskyblue: [
      135,
      206,
      250
    ],
    lightslategray: [
      119,
      136,
      153
    ],
    lightslategrey: [
      119,
      136,
      153
    ],
    lightsteelblue: [
      176,
      196,
      222
    ],
    lightyellow: [
      255,
      255,
      224
    ],
    lime: [
      0,
      255,
      0
    ],
    limegreen: [
      50,
      205,
      50
    ],
    linen: [
      250,
      240,
      230
    ],
    magenta: [
      255,
      0,
      255
    ],
    maroon: [
      128,
      0,
      0
    ],
    mediumaquamarine: [
      102,
      205,
      170
    ],
    mediumblue: [
      0,
      0,
      205
    ],
    mediumorchid: [
      186,
      85,
      211
    ],
    mediumpurple: [
      147,
      112,
      219
    ],
    mediumseagreen: [
      60,
      179,
      113
    ],
    mediumslateblue: [
      123,
      104,
      238
    ],
    mediumspringgreen: [
      0,
      250,
      154
    ],
    mediumturquoise: [
      72,
      209,
      204
    ],
    mediumvioletred: [
      199,
      21,
      133
    ],
    midnightblue: [
      25,
      25,
      112
    ],
    mintcream: [
      245,
      255,
      250
    ],
    mistyrose: [
      255,
      228,
      225
    ],
    moccasin: [
      255,
      228,
      181
    ],
    navajowhite: [
      255,
      222,
      173
    ],
    navy: [
      0,
      0,
      128
    ],
    oldlace: [
      253,
      245,
      230
    ],
    olive: [
      128,
      128,
      0
    ],
    olivedrab: [
      107,
      142,
      35
    ],
    orange: [
      255,
      165,
      0
    ],
    orangered: [
      255,
      69,
      0
    ],
    orchid: [
      218,
      112,
      214
    ],
    palegoldenrod: [
      238,
      232,
      170
    ],
    palegreen: [
      152,
      251,
      152
    ],
    paleturquoise: [
      175,
      238,
      238
    ],
    palevioletred: [
      219,
      112,
      147
    ],
    papayawhip: [
      255,
      239,
      213
    ],
    peachpuff: [
      255,
      218,
      185
    ],
    peru: [
      205,
      133,
      63
    ],
    pink: [
      255,
      192,
      203
    ],
    plum: [
      221,
      160,
      221
    ],
    powderblue: [
      176,
      224,
      230
    ],
    purple: [
      128,
      0,
      128
    ],
    rebeccapurple: [
      102,
      51,
      153
    ],
    red: [
      255,
      0,
      0
    ],
    rosybrown: [
      188,
      143,
      143
    ],
    royalblue: [
      65,
      105,
      225
    ],
    saddlebrown: [
      139,
      69,
      19
    ],
    salmon: [
      250,
      128,
      114
    ],
    sandybrown: [
      244,
      164,
      96
    ],
    seagreen: [
      46,
      139,
      87
    ],
    seashell: [
      255,
      245,
      238
    ],
    sienna: [
      160,
      82,
      45
    ],
    silver: [
      192,
      192,
      192
    ],
    skyblue: [
      135,
      206,
      235
    ],
    slateblue: [
      106,
      90,
      205
    ],
    slategray: [
      112,
      128,
      144
    ],
    slategrey: [
      112,
      128,
      144
    ],
    snow: [
      255,
      250,
      250
    ],
    springgreen: [
      0,
      255,
      127
    ],
    steelblue: [
      70,
      130,
      180
    ],
    tan: [
      210,
      180,
      140
    ],
    teal: [
      0,
      128,
      128
    ],
    thistle: [
      216,
      191,
      216
    ],
    tomato: [
      255,
      99,
      71
    ],
    turquoise: [
      64,
      224,
      208
    ],
    violet: [
      238,
      130,
      238
    ],
    wheat: [
      245,
      222,
      179
    ],
    white: [
      255,
      255,
      255
    ],
    whitesmoke: [
      245,
      245,
      245
    ],
    yellow: [
      255,
      255,
      0
    ],
    yellowgreen: [
      154,
      205,
      50
    ]
  };
})(gs);
(function(e) {
  Object.defineProperty(e, "__esModule", {
    value: !0
  });
  function r(h, m) {
    for (var $ in m)
      Object.defineProperty(h, $, {
        enumerable: !0,
        get: m[$]
      });
  }
  r(e, {
    parseColor: function() {
      return v;
    },
    formatColor: function() {
      return p;
    }
  });
  const t = /* @__PURE__ */ n(gs);
  function n(h) {
    return h && h.__esModule ? h : {
      default: h
    };
  }
  let a = /^#([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})?$/i, i = /^#([a-f\d])([a-f\d])([a-f\d])([a-f\d])?$/i, o = /(?:\d+|\d*\.\d+)%?/, s = /(?:\s*,\s*|\s+)/, c = /\s*[,/]\s*/, u = /var\(--(?:[^ )]*?)(?:,(?:[^ )]*?|var\(--[^ )]*?\)))?\)/, l = new RegExp(`^(rgba?)\\(\\s*(${o.source}|${u.source})(?:${s.source}(${o.source}|${u.source}))?(?:${s.source}(${o.source}|${u.source}))?(?:${c.source}(${o.source}|${u.source}))?\\s*\\)$`), f = new RegExp(`^(hsla?)\\(\\s*((?:${o.source})(?:deg|rad|grad|turn)?|${u.source})(?:${s.source}(${o.source}|${u.source}))?(?:${s.source}(${o.source}|${u.source}))?(?:${c.source}(${o.source}|${u.source}))?\\s*\\)$`);
  function v(h, { loose: m = !1 } = {}) {
    var $, y;
    if (typeof h != "string")
      return null;
    if (h = h.trim(), h === "transparent")
      return {
        mode: "rgb",
        color: [
          "0",
          "0",
          "0"
        ],
        alpha: "0"
      };
    if (h in t.default)
      return {
        mode: "rgb",
        color: t.default[h].map((k) => k.toString())
      };
    let O = h.replace(i, (k, ee, Le, rn, Jr) => [
      "#",
      ee,
      ee,
      Le,
      Le,
      rn,
      rn,
      Jr ? Jr + Jr : ""
    ].join("")).match(a);
    if (O !== null)
      return {
        mode: "rgb",
        color: [
          parseInt(O[1], 16),
          parseInt(O[2], 16),
          parseInt(O[3], 16)
        ].map((k) => k.toString()),
        alpha: O[4] ? (parseInt(O[4], 16) / 255).toString() : void 0
      };
    var g;
    let _ = (g = h.match(l)) !== null && g !== void 0 ? g : h.match(f);
    if (_ === null)
      return null;
    let M = [
      _[2],
      _[3],
      _[4]
    ].filter(Boolean).map((k) => k.toString());
    return M.length === 2 && M[0].startsWith("var(") ? {
      mode: _[1],
      color: [
        M[0]
      ],
      alpha: M[1]
    } : !m && M.length !== 3 || M.length < 3 && !M.some((k) => /^var\(.*?\)$/.test(k)) ? null : {
      mode: _[1],
      color: M,
      alpha: ($ = _[5]) === null || $ === void 0 || (y = $.toString) === null || y === void 0 ? void 0 : y.call($)
    };
  }
  function p({ mode: h, color: m, alpha: $ }) {
    let y = $ !== void 0;
    return h === "rgba" || h === "hsla" ? `${h}(${m.join(", ")}${y ? `, ${$}` : ""})` : `${h}(${m.join(" ")}${y ? ` / ${$}` : ""})`;
  }
})(Ct);
const X = De.slate;
function Te(e, r = 1) {
  const t = Ct.parseColor(e);
  return t.alpha = r, Ct.formatColor(t);
}
const Va = De.sky[500], R = {
  dark: X[900],
  light: X[100],
  base: X[600],
  primary: Va,
  success: De.green[500],
  warning: De.orange[500],
  error: De.rose[500],
  info: X[500],
  background: X[50],
  border: X[300],
  "border-dark": X[600],
  split: Te(X[300], 0.6),
  fill: Te(X[300], 0.2),
  selected: Te(Va, 0.1),
  shadow: Te(De.black, 0.02),
  gray: X
}, C5 = {
  xss: "2px",
  xs: "4px",
  sm: "8px",
  md: "12px",
  lg: "16px",
  xl: "32px",
  "2xl": "48px"
}, R5 = {
  base: "12px",
  family: "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol',  'Noto Color Emoji'"
}, Cr = {
  colorText: R.base,
  colorPrimary: R.primary,
  colorError: R.error,
  colorInfo: R.info,
  colorSuccess: R.success,
  colorWarning: R.warning,
  colorBorder: R.border,
  colorBorderSecondary: R.split,
  colorSplit: R.split,
  colorLink: R.primary,
  colorFillAlter: R.fill,
  colorLinkHover: Te(R.primary, 0.6),
  borderRadius: 2,
  fontSizeSM: Number(R5.base.replace("px", "")),
  fontFamily: "inherit"
}, A5 = {
  Tooltip: {
    paddingXS: Number(C5.sm.replace("px", ""))
  }
}, le = qe({
  mode: "light",
  token: Cr,
  default: Cr,
  components: A5,
  algorithm: [Za.compactAlgorithm]
});
function k5(e) {
  e == "dark" && (le.mode = "dark", le.token = Object.assign({}, Cr, {
    colorText: R.light,
    colorBgBase: R.dark,
    colorBorder: R["border-dark"],
    colorBorderSecondary: R["border-dark"],
    colorSplit: R["border-dark"],
    colorFillAlter: Te(R.light, 0.05)
  }), le.algorithm.push(Za.darkAlgorithm), Sr.set("theme", "dark"), document.documentElement.classList.add("dark")), e == "light" && (le.mode = "light", le.token = Cr, le.algorithm.splice(-1, 1), Sr.remove("theme"), document.documentElement.classList.remove("dark"));
}
yr(() => le.mode, (e) => {
  k5(e);
});
le.mode = Sr.get("theme") || "light";
kt.props.maskClosable.default = !1;
Ts.props.allowClear.default = !0;
const $s = {
  type: String,
  default: (e) => e.picker == "year" ? "YYYY" : e.picker == "month" ? "YYYY-MM" : e.showTime ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD"
};
Cs.props.valueFormat = $s;
Rs.props.valueFormat = $s;
As.props.valueFormat = {
  type: String,
  default: "HH:mm:ss"
};
kr.props.defaultPageSize.default = 50;
kr.props.pageSizeOptions = {
  type: Array,
  default: () => ["10", "30", "50", "100"]
};
kr.props.showSizeChanger.default = !0;
kr.props.showTotal = {
  type: Function,
  default: (e) => `共 ${e} 条记录`
};
ks.props.labelCol.default = () => ({ style: { width: "100px" } });
function P5(e) {
  e._context.components.AModal = N5, e.config.globalProperties.$message = Ps, e.config.globalProperties.$notification = xs, e.config.globalProperties.$modal = kt;
}
const x5 = {
  install: P5
}, pt = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  Colgroup: Ms,
  Column: Qa,
  Edit: Is,
  Filter: Ls,
  Grid: Us,
  Keyboard: Bs,
  List: Ws,
  Pulldown: zs,
  Table: qs,
  VXETable: ja,
  Validator: Hs
}, Symbol.toStringTag, { value: "Module" })), F5 = {
  __name: "ColumnAction",
  props: {
    width: {
      type: String,
      default: "120"
    },
    type: {
      type: String,
      default: "link"
    },
    size: {
      type: String,
      default: "small"
    },
    align: {
      type: String,
      default: "right"
    },
    fixed: {
      type: String,
      default: "right"
    },
    actions: {
      type: Array,
      default: () => ["edit", "remove"]
    },
    disabled: Function,
    show: Function
  },
  setup(e) {
    const r = e, t = {
      edit: {
        key: "edit",
        text: Kr("vxe.actions.edit") || "编辑"
      },
      remove: {
        key: "remove",
        text: Kr("vxe.actions.delete") || "删除",
        confirm: Kr("vxe.confirm.delete") || "确定要删除吗？"
      }
    }, n = [];
    (r.actions || []).forEach((s) => {
      typeof s == "string" && t[s] && n.push(t[s]), typeof s == "object" && s.key && n.push(s);
    });
    const i = (s, c) => typeof s.show == "function" ? s.show(c) : typeof r.show == "function" ? r.show(s, c) : !0, o = (s, c) => typeof s.disabled == "function" ? s.disabled(c) : typeof r.disabled == "function" ? r.disabled(s, c) : !1;
    return (s, c) => (S(), I(P(Qa), {
      width: e.width,
      resizable: !1,
      fixed: r.fixed,
      "show-overflow": !1,
      align: r.align,
      "class-name": "action",
      "header-class-name": "action"
    }, {
      header: A((u) => [
        N(s.$slots, "header", _r(br(u)))
      ]),
      default: A((u) => [
        (S(), D(de, null, Re(n, (l) => (S(), D(de, {
          key: l.key
        }, [
          l.confirm && i(l, u) ? (S(), I(P(Fs), {
            key: 0,
            placement: "bottomRight",
            title: l.confirm,
            disabled: o(l, u),
            onConfirm: (f) => s.$emit(l.key, u)
          }, {
            default: A(() => [
              $r(P(an), {
                type: e.type,
                size: e.size,
                disabled: o(l, u),
                onClick: c[0] || (c[0] = tn(() => {
                }, ["stop"]))
              }, {
                default: A(() => [
                  ce(fe(l.text), 1)
                ]),
                _: 2
              }, 1032, ["type", "size", "disabled"])
            ]),
            _: 2
          }, 1032, ["title", "disabled", "onConfirm"])) : ve("", !0),
          !l.confirm && i(l, u) ? (S(), I(P(an), {
            key: 1,
            type: e.type,
            size: e.size,
            disabled: o(l, u),
            onClick: tn((f) => s.$emit(l.key, u), ["stop"])
          }, {
            default: A(() => [
              ce(fe(l.text), 1)
            ]),
            _: 2
          }, 1032, ["type", "size", "disabled", "onClick"])) : ve("", !0)
        ], 64))), 64)),
        N(s.$slots, "default", _r(br(u)))
      ]),
      _: 3
    }, 8, ["width", "fixed", "align"]));
  }
}, M5 = {
  table: {
    size: "mini",
    border: "inner",
    stripe: !1,
    sortConfig: { trigger: "cell" },
    showOverflow: "title",
    rowConfig: {
      isCurrent: !0,
      isHover: !0
    },
    columnConfig: {
      resizable: !0,
      minWidth: "80"
    }
  }
};
function I5(e, r) {
  ja.setConfig(Yt.merge(M5, r)), Object.keys(pt).forEach((t) => {
    pt[t].install && e.use(pt[t]);
  }), e.component("vxe-column-action", F5);
}
const L5 = {
  install: I5
};
function U5(e) {
  return { all: e = e || /* @__PURE__ */ new Map(), on: function(r, t) {
    var n = e.get(r);
    n ? n.push(t) : e.set(r, [t]);
  }, off: function(r, t) {
    var n = e.get(r);
    n && (t ? n.splice(n.indexOf(t) >>> 0, 1) : e.set(r, []));
  }, emit: function(r, t) {
    var n = e.get(r);
    n && n.slice().map(function(a) {
      a(t);
    }), (n = e.get("*")) && n.slice().map(function(a) {
      a(r, t);
    });
  } };
}
function B5(e, r) {
  e.use(x5), e.use(L5, r == null ? void 0 : r.vxe), e.use(O5, r), e.use(l5);
  const t = e.config.globalProperties;
  Ce.setup({
    notifications: {
      alert: (a) => {
        t.$modal.warning({
          content: a
        });
      },
      error: (a) => {
        t.$message.error(a, 10);
      },
      success: (a) => {
        t.$message.success(a);
      }
    }
  }), t.$events = U5();
  const n = window.CONFIG;
  n && (t.$config = n, Ce.setup({
    host: n.host
  }));
}
const G5 = {
  install: B5
};
export {
  tu as AppContainer,
  ou as AppContent,
  Qs as AppHeader,
  du as AppMenu,
  vu as AppNavbar,
  lu as AppSider,
  v5 as BsContent,
  d5 as BsLayout,
  w5 as BsTitle,
  g5 as BsToolbar,
  sr as Icon,
  G5 as default,
  Ce as http,
  gt as session,
  Sr as storage,
  le as theme,
  Yt as utils
};
